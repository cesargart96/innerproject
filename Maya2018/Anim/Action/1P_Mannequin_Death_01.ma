//Maya ASCII 2018ff09 scene
//Name: 1P_Mannequin_Death_01.ma
//Last modified: Sun, Sep 29, 2019 11:52:38 AM
//Codeset: 1252
file -rdi 1 -ns "Rig_UE4ManController" -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
file -rdi 2 -ns "SK_Mannequin" -rfn "Rig_UE4ManController:SK_MannequinRN" -op
		 "v=0;" -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/SkinnedMesh/SK_Mannequin.ma";
file -r -ns "Rig_UE4ManController" -dr 1 -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
requires maya "2018ff09";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2018.0.0";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201811122215-49253d42f6";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "9B71BA30-4D91-37E0-F8A1-76860572EF0C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -429.12830124025243 97.659624855289977 51.800103798157778 ;
	setAttr ".r" -type "double3" -2.7383527295990771 -79.800000000012048 1.1225397921523728e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "040CCD28-47B7-36F4-7075-158AB9BE94F6";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 587.78008298776899;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.061806879937648773 52.703701019287109 -4.5796432495117188 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "22E1EC1C-4B99-370C-4ED5-64A46726A3C8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "3C451472-4A71-EA39-E829-3EA2ED993072";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "FAA32395-4DDE-EAF3-353A-EF83E3FBBA46";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "899DFA0A-411A-D494-52C5-E29F29354815";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "4BC5C9E7-4524-0750-E579-1283483E856A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E25881E2-466F-FDA8-671F-CC887393B509";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode dagContainer -n "animBot";
	rename -uid "A4FECEBD-4BFD-FC3B-C1D0-34829DE42924";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -s false -ci true -sn "animBot_Anim_Recovery_Scene_ID" -ln "animBot_Anim_Recovery_Scene_ID" 
		-at "message";
	addAttr -s false -ci true -sn "animBot_Select_Sets" -ln "animBot_Select_Sets" -at "message";
	addAttr -s false -ci true -sn "__Purple__" -ln "__Purple__" -at "message";
	addAttr -s false -ci true -sn "__Light_Yellow__" -ln "__Light_Yellow__" -at "message";
	addAttr -s false -ci true -sn "__Light_Green__" -ln "__Light_Green__" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".iconSimpleName" -type "string" "animBot";
createNode dagContainer -n "animBot_Anim_Recovery_Scene_ID" -p "animBot";
	rename -uid "48859900-462F-A61E-3BCA-BC99D53BDBE6";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "sceneID" -ln "sceneID" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "anim_recovery";
	setAttr ".sceneID" -type "string" "1569311827.351000";
createNode dagContainer -n "animBot_Select_Sets" -p "animBot";
	rename -uid "3333EBB3-4AB9-EA84-B9D2-C78C9B3450C3";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "select_sets";
createNode dagContainer -n "__Purple__" -p "animBot_Select_Sets";
	rename -uid "59830D2C-4F4E-6A6D-4641-839492637B80";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "colorIndex" -ln "colorIndex" -at "long";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".uocol" yes;
	setAttr ".oclr" -type "float3" 0.68235296 0.60392159 0.86666667 ;
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "color_25";
	setAttr ".colorIndex" 25;
createNode transform -n "All" -p "__Purple__";
	rename -uid "160ED3D7-4D2F-DEE7-CDCD-EC937D726B85";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "contents" -ln "contents" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr -l on ".contents" -type "string" (
		"|Rig_UE4ManController:FP_Cam |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim");
createNode dagContainer -n "__Light_Yellow__" -p "animBot_Select_Sets";
	rename -uid "9C4DBE86-4E6A-4A0B-0DB9-AEACE8F67767";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "colorIndex" -ln "colorIndex" -at "long";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".uocol" yes;
	setAttr ".oclr" -type "float3" 0.94901961 0.90196079 0.42352942 ;
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "color_12";
	setAttr ".colorIndex" 12;
createNode transform -n "Spine" -p "__Light_Yellow__";
	rename -uid "24F80241-4840-FCC7-02E3-9C8F6E5F7E02";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "contents" -ln "contents" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr -l on ".contents" -type "string" (
		"|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim");
createNode dagContainer -n "__Light_Green__" -p "animBot_Select_Sets";
	rename -uid "2D18856B-4B29-FBC5-448C-74A111C151D7";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "colorIndex" -ln "colorIndex" -at "long";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".uocol" yes;
	setAttr ".oclr" -type "float3" 0.51764709 0.94901961 0.58823532 ;
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "color_15";
	setAttr ".colorIndex" 15;
createNode transform -n "Neck" -p "__Light_Green__";
	rename -uid "C6FBC7B8-427C-A641-561C-5E9970E316D8";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "contents" -ln "contents" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr -l on ".contents" -type "string" (
		"|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim");
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A0B85128-4B5F-0630-AC58-D0A12823EC6B";
	setAttr -s 90 ".lnk";
	setAttr -s 90 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "E1315388-4530-F441-B571-0E88CD09A6B6";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "E1F852AE-443D-F145-2901-079CA8008CAF";
createNode displayLayerManager -n "layerManager";
	rename -uid "38107528-4231-61C2-C05D-D4BB2CF5813B";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  1 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "24CE7960-4C6C-ECA7-197A-23BAA0C634A7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "7CA7621A-4AC8-E54A-D5B0-50B9DECE00C8";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C4DFE456-470E-4AC8-6521-E889CF0E7EB6";
	setAttr ".g" yes;
createNode hyperLayout -n "hyperLayout1";
	rename -uid "7A75F400-4B93-2B36-D3EC-0D99FB2FE2B8";
	setAttr ".ihi" 0;
	setAttr -s 2 ".hyp";
createNode reference -n "Rig_UE4ManControllerRN";
	rename -uid "CBC4EE03-4672-3F8F-F837-8DA731DA7206";
	setAttr ".fn[0]" -type "string" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
	setAttr -s 440 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Rig_UE4ManControllerRN"
		"Rig_UE4ManControllerRN" 0
		"Rig_UE4ManController:SK_MannequinRN" 0
		"Rig_UE4ManControllerRN" 41
		1 |Rig_UE4ManController:FP_Cam "blendParent1" "blendParent1" " -ci 1 -k 1 -dv 1 -smn 0 -smx 1 -at \"double\""
		
		2 "|Rig_UE4ManController:FP_Cam" "translate" " -type \"double3\" 0.17934966416846798 61.19643343515717504 50.55551468572223683"
		
		2 "|Rig_UE4ManController:FP_Cam" "translateX" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "translateY" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "translateZ" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "rotate" " -type \"double3\" -63.71628117381744971 -179.53833702594207011 0.55606240505765037"
		
		2 "|Rig_UE4ManController:FP_Cam" "rotateX" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "rotateY" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "rotateZ" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "blendParent1" " -k 1"
		2 "Rig_UE4ManController:Skel" "visibility" " 0"
		2 "Rig_UE4ManController:Skel" "displayOrder" " 3"
		2 "Rig_UE4ManController:MannequinMesh" "visibility" " 1"
		2 "Rig_UE4ManController:IKBones" "visibility" " 0"
		2 "Rig_UE4ManController:IKBones" "displayOrder" " 4"
		2 "Rig_UE4ManController:proxy_geo_layer" "displayOrder" " 5"
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateX" 
		"|Rig_UE4ManController:FP_Cam.translateX" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateY" 
		"|Rig_UE4ManController:FP_Cam.translateY" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateZ" 
		"|Rig_UE4ManController:FP_Cam.translateZ" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateX" 
		"|Rig_UE4ManController:FP_Cam.rotateX" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateY" 
		"|Rig_UE4ManController:FP_Cam.rotateY" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateZ" 
		"|Rig_UE4ManController:FP_Cam.rotateZ" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[422]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[423]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[424]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[425]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[426]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[427]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.visibility" 
		"Rig_UE4ManControllerRN.placeHolderList[428]" ""
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.blendParent1" 
		"Rig_UE4ManControllerRN.placeHolderList[429]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.blendParent1" 
		"Rig_UE4ManControllerRN.placeHolderList[430]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleX" "Rig_UE4ManControllerRN.placeHolderList[431]" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleY" "Rig_UE4ManControllerRN.placeHolderList[432]" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleZ" "Rig_UE4ManControllerRN.placeHolderList[433]" 
		""
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateX" 
		"Rig_UE4ManControllerRN.placeHolderList[434]" "Rig_UE4ManController:FP_Cam.tx"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateY" 
		"Rig_UE4ManControllerRN.placeHolderList[435]" "Rig_UE4ManController:FP_Cam.ty"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[436]" "Rig_UE4ManController:FP_Cam.tz"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[437]" "Rig_UE4ManController:FP_Cam.rx"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[438]" "Rig_UE4ManController:FP_Cam.ry"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[439]" "Rig_UE4ManController:FP_Cam.rz"
		5 3 "Rig_UE4ManControllerRN" "Rig_UE4ManController:groupParts117.outputGeometry" 
		"Rig_UE4ManControllerRN.placeHolderList[440]" "Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.i"
		
		"Rig_UE4ManController:SK_MannequinRN" 504
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"instObjGroups.objectGroups" " -s 12"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvPivot" " -type \"double2\" 0.39557498693466187 0.898499995470047"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"DiffuseUV\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].colorName" " -type \"string\" \"colorSet0\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].clamped" " 0"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].representation" " 4"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim" 
		"translate" " -type \"double3\" -4.48864421396094393 -104.46575450778041727 16.78071642886854065"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translate" " -type \"double3\" 58.31169761613745095 -31.39243220600112494 -124.06763409124141617"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotate" " -type \"double3\" 0.91988953453900246 -36.24290265941694855 87.00492517982853258"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim" 
		"rotate" " -type \"double3\" 1.16220426546386602 13.1137772112005706 7.96953616569119561"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotate" " -type \"double3\" 0.23271504602852613 -1.52310872312861179 6.22549634055159107"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotate" " -type \"double3\" -0.60980411450110827 -6.50547198119442616 2.94693477713727292"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotate" " -type \"double3\" -0.26153155550901347 -19.37305384299165212 2.39516426507202507"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotate" " -type \"double3\" 5.16490030488799157 -10.89886719547214611 -10.69265210923589926"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim" 
		"translate" " -type \"double3\" 0 -0.042347615424996037 7.6047750442683304"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim" 
		"rotate" " -type \"double3\" 0.071240636182588385 0.069809511384960929 9.7316771829935913"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim" 
		"rotate" " -type \"double3\" 0.071240636182588385 0.069809511384960929 9.7316771829935913"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"translate" " -type \"double3\" 14.77665890718744279 -25.79671183515815613 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"rotate" " -type \"double3\" 0 0 -17.80257296430417924"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim" 
		"knee_twist" " -av -k 1 26.37916135204082124"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"translate" " -type \"double3\" 0 36.1637905454177897 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"rotate" " -type \"double3\" 94.35533532258570233 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"translate" " -type \"double3\" -66.49444956574349419 1.49958998241666186 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"rotate" " -type \"double3\" 0 0 -32.25215616151530895"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim" 
		"rotate" " -type \"double3\" 0.4796320024691455 0.24722751466531376 -50.92449250713303144"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim" 
		"rotate" " -type \"double3\" 0.4796320024691455 0.24722751466531376 -50.92449250713303144"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"translate" " -type \"double3\" -0.0072509593577483478 56.67214769449672218 11.23299685113238588"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"rotate" " -type \"double3\" -89.89816724004850812 -45.77464083342997725 89.90563873242318493"
		
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "displayType" " 2"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "visibility" " 1"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "displayOrder" " 2"
		3 "Rig_UE4ManController:groupParts117.outputGeometry" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.instObjGroups.objectGroups[20].objectGroupId" 
		"Rig_UE4ManControllerRN.placeHolderList[1]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		"Rig_UE4ManControllerRN.placeHolderList[2]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[3]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[4]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[5]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[6]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[7]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[8]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[9]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings.clavMode" 
		"Rig_UE4ManControllerRN.placeHolderList[10]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[11]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[12]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[13]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[14]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[15]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[16]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[17]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[18]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[19]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[20]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[21]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[22]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[23]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[24]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[25]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[26]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[27]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[28]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[29]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[30]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[31]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[32]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[33]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[34]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[35]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[36]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[37]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[38]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[39]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[40]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[41]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[42]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[43]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[44]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[45]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[46]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[47]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[48]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[49]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[50]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[51]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[52]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[53]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[54]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[55]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[56]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[57]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[58]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[59]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[60]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[61]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[62]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[63]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[64]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[65]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[66]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[67]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[68]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[69]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[70]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[71]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[72]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[73]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[74]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[75]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[76]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[77]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[78]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[79]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[80]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[81]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[82]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[83]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[84]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[85]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[86]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[87]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[88]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[89]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[90]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[91]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[92]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[93]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[94]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[95]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[96]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[97]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[98]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[99]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[100]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[101]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[102]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[103]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[104]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[105]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[106]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[107]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[108]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[109]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[110]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[111]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[112]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[113]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[114]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[115]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[116]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[117]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[118]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[119]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[120]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[121]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[122]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[123]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[124]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[125]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[126]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[127]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[128]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[129]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[130]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[131]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[132]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[133]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[134]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[135]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[136]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[137]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[138]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[139]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[140]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[141]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[142]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[143]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[144]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[145]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[146]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[147]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[148]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[149]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[150]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[151]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[152]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[153]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[154]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[155]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[156]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[157]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[158]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[159]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[160]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[161]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[162]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[163]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[164]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[165]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[166]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[167]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[168]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[169]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[170]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_settings.clavMode" 
		"Rig_UE4ManControllerRN.placeHolderList[171]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[172]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[173]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[174]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[175]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[176]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[177]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[178]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[179]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[180]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[181]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[182]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[183]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[184]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[185]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[186]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[187]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[188]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[189]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[190]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[191]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[192]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[193]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[194]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[195]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[196]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[197]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[198]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[199]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[200]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[201]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[202]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[203]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[204]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[205]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[206]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[207]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[208]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[209]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[210]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[211]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[212]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[213]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[214]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[215]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[216]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[217]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[218]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[219]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[220]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[221]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[222]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[223]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[224]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[225]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[226]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[227]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[228]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[229]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[230]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[231]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[232]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[233]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[234]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[235]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[236]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[237]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[238]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[239]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[240]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[241]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[242]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[243]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[244]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[245]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[246]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[247]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[248]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[249]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[250]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[251]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[252]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[253]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[254]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[255]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[256]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[257]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[258]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[259]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[260]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[261]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[262]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[263]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[264]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[265]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[266]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[267]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[268]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[269]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[270]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[271]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[272]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[273]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[274]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[275]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[276]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[277]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[278]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[279]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[280]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[281]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[282]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[283]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[284]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[285]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[286]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[287]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[288]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[289]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[290]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[291]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[292]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[293]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[294]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[295]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[296]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[297]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[298]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[299]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[300]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[301]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[302]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[303]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[304]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[305]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[306]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[307]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[308]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[309]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[310]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[311]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[312]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[313]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[314]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[315]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[316]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[317]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[318]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[319]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[320]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[321]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[322]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[323]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[324]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[325]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[326]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[327]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[328]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[329]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[330]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[331]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[332]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[333]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[334]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[335]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[336]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[337]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[338]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[339]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[340]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[341]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[342]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[343]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[344]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[345]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:leg_L_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[346]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[347]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[348]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[349]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[350]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.stretchBias" 
		"Rig_UE4ManControllerRN.placeHolderList[351]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[352]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.knee_twist" 
		"Rig_UE4ManControllerRN.placeHolderList[353]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[354]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[355]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[356]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.toeCtrlVis" 
		"Rig_UE4ManControllerRN.placeHolderList[357]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[358]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[359]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[360]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[361]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[362]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[363]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.ballPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[364]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.heelPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[365]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[366]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[367]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[368]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:leg_R_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[369]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[370]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[371]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[372]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[373]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.stretchBias" 
		"Rig_UE4ManControllerRN.placeHolderList[374]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[375]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.knee_twist" 
		"Rig_UE4ManControllerRN.placeHolderList[376]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[377]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[378]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[379]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.toeCtrlVis" 
		"Rig_UE4ManControllerRN.placeHolderList[380]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[381]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[382]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[383]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[384]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[385]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[386]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.ballPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[387]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.heelPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[388]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[389]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[390]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[391]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[392]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[393]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[394]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[395]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[396]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[397]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[398]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[399]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[400]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[401]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[402]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[403]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[404]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[405]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[406]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[407]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[408]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[409]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[410]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[411]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[412]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[413]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[414]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[415]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[416]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[417]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.twist_amount" 
		"Rig_UE4ManControllerRN.placeHolderList[418]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.autoSpine" 
		"Rig_UE4ManControllerRN.placeHolderList[419]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotationInfluence" 
		"Rig_UE4ManControllerRN.placeHolderList[420]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[421]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "F174DB98-42F4-C6EB-6B69-21B88D373DCE";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "A3756E06-4C22-85FA-A873-A883FBF7A71A";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "72578236-4D72-4F99-CF17-64885EFB820C";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "DE8000B5-4133-0645-B895-429447408B7E";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "1B9B6DC3-42D7-7AB3-E5DA-F5ACED2968F7";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1303\n            -height 651\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n"
		+ "            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n"
		+ "                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n"
		+ "\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n"
		+ "                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n"
		+ "                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n"
		+ "                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Model Panel5\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Model Panel5\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"Rig_UE4ManController:FP_CamShape\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1914\n            -height 973\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1303\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1303\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 1000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1AFC679D-489C-E075-DD59-13B48A3EFC8B";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 100 -ast 0 -aet 100 ";
	setAttr ".st" 6;
createNode groupId -n "groupId5";
	rename -uid "30D16B17-4481-D4C0-5F15-7AB2420F31B8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "DBDFA0E2-4441-71F6-C9FB-76B05E6D0ED6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[25958:27625]" "f[31580:31595]";
createNode reference -n "sharedReferenceNode";
	rename -uid "320DD024-4F9C-ABDA-F405-B3814DAD3CCE";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
createNode hyperLayout -n "hyperLayout2";
	rename -uid "9FCB43DB-473A-243F-525E-4FAF22579166";
	setAttr ".ihi" 0;
	setAttr -s 3 ".hyp";
createNode hyperLayout -n "hyperLayout3";
	rename -uid "27616C22-468F-F644-C088-049764AAA013";
	setAttr ".ihi" 0;
	setAttr -s 3 ".hyp";
createNode animCurveTU -n "FP_Cam_visibility";
	rename -uid "636077A7-4961-194D-1432-6397C2D1F20D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
	setAttr -s 3 ".kit[0:2]"  9 18 9;
createNode pairBlend -n "pairBlend1";
	rename -uid "68281F25-4CD5-D3BE-4E55-0783694E66A1";
createNode animCurveTL -n "pairBlend1_inTranslateX1";
	rename -uid "4022C964-40F8-ED62-9CDD-A0AD61E26DFF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -2.7430723359241172e-12 56 0 100 -2.418384651086887e-12;
createNode animCurveTL -n "pairBlend1_inTranslateY1";
	rename -uid "6ECBEEA5-426E-D62E-8EEA-DCB7E16E8B06";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 167.62431881957681 56 54.163317224856044
		 100 54.163317224856044;
createNode animCurveTL -n "pairBlend1_inTranslateZ1";
	rename -uid "8BF81F35-4C3A-9D82-DA53-72A263DF12EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0.44744682008782194 56 49.892537502007407
		 100 49.892537502007407;
createNode animCurveTA -n "pairBlend1_inRotateX1";
	rename -uid "729A2C0C-4DC2-A6E2-06A1-7E9BCC14DCF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -89.680948543075544 100 -89.680948543075544;
createNode animCurveTA -n "pairBlend1_inRotateY1";
	rename -uid "AA459B57-45AD-CEDB-63CC-3CB98D611592";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 179.9999999999998 56 -179.99999999999989
		 100 -179.99999999999986;
createNode animCurveTA -n "pairBlend1_inRotateZ1";
	rename -uid "6797D232-4B49-BA5C-877A-A780DD87F129";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -7.6333312355124402e-14 56 0 100 5.597776239375789e-13;
createNode animCurveTU -n "FP_Cam_scaleX";
	rename -uid "EF01250E-429D-FEF4-BC38-E0A95B8D1678";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 24.37772922952978 56 24.37772922952978
		 100 24.37772922952978;
createNode animCurveTU -n "FP_Cam_scaleY";
	rename -uid "F8DDEC99-43B3-D97E-CE77-D88F822AFD73";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 24.37772922952978 56 24.37772922952978
		 100 24.37772922952978;
createNode animCurveTU -n "FP_Cam_scaleZ";
	rename -uid "11482657-4450-CEF5-02C6-BA950D3872C6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 24.37772922952978 56 24.37772922952978
		 100 24.37772922952978;
createNode animCurveTL -n "pinky_03_L_anim_translateX";
	rename -uid "C6267711-419C-55AF-B9D1-B586E7E0C3FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_03_L_anim_translateY";
	rename -uid "47EA789A-4943-803C-FD9C-ECAC9E68250E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_03_L_anim_translateZ";
	rename -uid "6325C7B9-4D77-4577-2763-C7B3FC2DC651";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_L_anim_rotateX";
	rename -uid "B934A7F9-4CEB-9457-FC14-BD9C482D38BB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_L_anim_rotateY";
	rename -uid "83A1D948-4E73-1181-5FF3-7DB59B0439C6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_L_anim_rotateZ";
	rename -uid "3919D203-4EB1-C5DE-1196-86806656EE67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "pinky_03_L_anim_scaleX";
	rename -uid "75E4ADBE-43BA-0318-8940-CC8DD2BD52E4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_03_L_anim_scaleY";
	rename -uid "55494D75-42A1-D497-7BE2-65ACA1820777";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_03_L_anim_scaleZ";
	rename -uid "878DBEEB-4512-9F00-68F8-789B1A3B06E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "pinky_02_L_anim_translateX";
	rename -uid "F006E1CC-4861-FA09-B3B6-888D96AF3FF9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_02_L_anim_translateY";
	rename -uid "3BE765F1-4B58-13FE-B10A-E4BDBDED56A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_02_L_anim_translateZ";
	rename -uid "A6FDEAAF-4989-D082-7C1B-80ABB8D53546";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_L_anim_rotateX";
	rename -uid "A34BF75A-4003-36C0-4F10-0F994BA9A752";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_L_anim_rotateY";
	rename -uid "B3BC7DB0-4DC9-564D-7F0B-A39F48B8F923";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_L_anim_rotateZ";
	rename -uid "75A4F0A8-4CA1-2839-1620-BF8051A7DEF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "pinky_02_L_anim_scaleX";
	rename -uid "5CDC971A-41E4-7038-DA0B-8A96C50AEB31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_02_L_anim_scaleY";
	rename -uid "27D2E289-420B-151C-A09C-6A84F4193B19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_02_L_anim_scaleZ";
	rename -uid "80006587-4ADD-B689-DA29-D69B554337E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "pinky_01_L_anim_translateX";
	rename -uid "6F8EF57F-4B01-427E-FBBE-8A953A1B788A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_01_L_anim_translateY";
	rename -uid "36B6217C-43F6-09AF-A389-B7BD4ADF746A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_01_L_anim_translateZ";
	rename -uid "0FEFD130-4F24-F155-A8CC-1EB2C929699D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_01_L_anim_rotateX";
	rename -uid "8F178D88-41AE-1265-9BA2-978E0FEA9577";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_01_L_anim_rotateY";
	rename -uid "B28BEE03-4D5F-1F45-D487-21A634292707";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_01_L_anim_rotateZ";
	rename -uid "EBB302F1-40ED-F971-19B8-A4B11D2C7C14";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "pinky_01_L_anim_scaleX";
	rename -uid "F654815A-4E82-FFCC-67D3-608984B01B11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_L_anim_scaleY";
	rename -uid "48A93495-49DD-CC40-B804-CDB2C204D131";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_L_anim_scaleZ";
	rename -uid "976512DF-4142-5972-1A36-42B3DB36863A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_L_anim_sticky";
	rename -uid "580B7CED-472B-721B-1825-6B98E75EC493";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_L_anim_translateX";
	rename -uid "9A168411-4537-B24D-6125-A4BB685C479E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_L_anim_translateY";
	rename -uid "C7D7CD59-44D4-350A-7D48-B7A1FC14C7F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_L_anim_translateZ";
	rename -uid "14E3C239-4C17-6AE1-AA9E-FCB469AE0E91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_L_anim_rotateX";
	rename -uid "61E3E4E9-4F55-1614-31D5-36BEE45F9585";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_L_anim_rotateY";
	rename -uid "877D0793-43F0-8CE6-7556-75A72BB2FA9C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_L_anim_rotateZ";
	rename -uid "3FE00B1A-4AF6-45DB-D17F-3995DBD2B784";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ring_03_L_anim_scaleX";
	rename -uid "02CDF667-4613-F5F5-106B-35AF5F1397FB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_03_L_anim_scaleY";
	rename -uid "DB21AA72-47F8-286A-9243-97928CC8399C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_03_L_anim_scaleZ";
	rename -uid "0EBD2DF5-42E3-2B0A-4C69-A7966979B043";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "ring_02_L_anim_translateX";
	rename -uid "8B79DF0B-487E-6CE0-904F-8AB2E4B75AC3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_02_L_anim_translateY";
	rename -uid "DADB7558-4F08-08B0-653E-4185DE82C804";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_02_L_anim_translateZ";
	rename -uid "BDA3FF91-4B89-9BB6-5CDC-09A365F9FEBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_L_anim_rotateX";
	rename -uid "3434FF8B-4834-2296-386E-56899819A2E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_L_anim_rotateY";
	rename -uid "3AD1D6D5-4953-88B6-217D-DA9B4357F002";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_L_anim_rotateZ";
	rename -uid "77E8DAEE-465D-1C58-391B-A18880755316";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ring_02_L_anim_scaleX";
	rename -uid "19539F24-467C-C661-128F-CEB80A816571";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_02_L_anim_scaleY";
	rename -uid "FEEA0B7C-4A28-A2E5-0593-3480F9A015AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_02_L_anim_scaleZ";
	rename -uid "334F574D-4FF0-9363-927E-FE968BB4E1EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_03_L_anim_translateX";
	rename -uid "97115450-4264-1BD9-B9A4-A0B17BC2E57C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_03_L_anim_translateY";
	rename -uid "54A4FD62-4803-B71E-EA00-D5B55B21E4B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_03_L_anim_translateZ";
	rename -uid "642B8F0A-4B5F-DAC1-7553-569560B20514";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_L_anim_rotateX";
	rename -uid "E11D40A2-4C00-2A70-D1B4-8291806C90F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_L_anim_rotateY";
	rename -uid "5C099335-413B-054B-DCC7-AEAC880A515A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_L_anim_rotateZ";
	rename -uid "2AB6AAD9-42FF-E76D-FD32-01934FAB26BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "thumb_03_L_anim_scaleX";
	rename -uid "A66A6AA3-42E8-8F75-E83F-968A356F0B7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_03_L_anim_scaleY";
	rename -uid "F04D8012-4E30-7033-1181-509185C472AC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_03_L_anim_scaleZ";
	rename -uid "4D05D087-4909-77B1-33C8-55811E8898C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_02_L_anim_translateX";
	rename -uid "DCB5E8C0-4215-5FE0-77BD-02AB37A109D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_02_L_anim_translateY";
	rename -uid "0DC52016-402F-5EA4-283D-8CA2788EDC71";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_02_L_anim_translateZ";
	rename -uid "B23C18D0-4B58-2F1E-F13D-A0AADFB68D97";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_L_anim_rotateX";
	rename -uid "95D0E2AA-44A4-9F10-FB87-1BB4EBD05D2B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_L_anim_rotateY";
	rename -uid "EFE3B9A0-493F-813F-D8C0-0DB9C3A7A3F8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_L_anim_rotateZ";
	rename -uid "AF89C1A5-4B3D-6E2B-E548-A88EE84CC348";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -11.625641870682387 56 -11.625641870682387
		 100 -11.625641870682387;
createNode animCurveTU -n "thumb_02_L_anim_scaleX";
	rename -uid "C753737D-4737-CCAD-A9F9-6F9F25E5FD2F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_02_L_anim_scaleY";
	rename -uid "B2EC3292-42D0-894A-2EBD-DC81C1074BFD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_02_L_anim_scaleZ";
	rename -uid "F6DA6FF4-4FCC-278F-1D36-099E4129DB77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_01_L_anim_translateX";
	rename -uid "CAD28D34-41CA-4039-EBB7-37AABE2C482E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_01_L_anim_translateY";
	rename -uid "5049E150-4DD4-A4B6-146A-EAB0DAC95D3C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_01_L_anim_translateZ";
	rename -uid "E4A55122-40B0-4641-8DDC-FDA80CCEAA12";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateX";
	rename -uid "FFD99C24-49BF-55F1-41D6-8A88BD683DD5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateY";
	rename -uid "687BF4D4-4F64-2FFC-9934-AB82B5D7F103";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateZ";
	rename -uid "1E02F30A-46A0-DE0C-FFD8-9DB42E2CF14B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "thumb_01_L_anim_scaleX";
	rename -uid "73AB3467-4838-990E-E952-A0BE12B9D3AF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_01_L_anim_scaleY";
	rename -uid "52A624C2-4D28-9623-EB74-5C8AFDD2A75D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_01_L_anim_scaleZ";
	rename -uid "FC515629-4848-FF51-0619-4AA5D058FBAF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "ring_01_L_anim_translateX";
	rename -uid "9CE0438D-475C-C368-5EE7-D9B38702C21A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_01_L_anim_translateY";
	rename -uid "4C498A37-490C-5458-D3CE-3286700E9A0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_01_L_anim_translateZ";
	rename -uid "BCD93E40-4675-988A-9523-AA9706BB30CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_01_L_anim_rotateX";
	rename -uid "8AB85413-4985-326A-28ED-EBA44A108AF9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_01_L_anim_rotateY";
	rename -uid "27E61A09-4F4D-3841-156D-5BACA141CC39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_01_L_anim_rotateZ";
	rename -uid "6BB4BBCA-44AE-0C3C-51C2-A6AB8420E138";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ring_01_L_anim_scaleX";
	rename -uid "6D05086F-4EE1-B672-30DB-BEA014F9F68F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_L_anim_scaleY";
	rename -uid "860A36AB-4A35-D620-A25E-48B873E48EFC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_L_anim_scaleZ";
	rename -uid "17409CD0-4C7C-5ED6-6617-10A223E64465";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_L_anim_sticky";
	rename -uid "478BF81E-4FC2-F140-7D25-E9B893B4511A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_L_anim_translateX";
	rename -uid "E88EF214-40B5-5F86-8ECB-F3979FBC17F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_L_anim_translateY";
	rename -uid "E023EEAB-4B3B-B115-9AF4-F2BF62CD6470";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_L_anim_translateZ";
	rename -uid "5C274644-44DB-62DD-20D7-18B99576C7A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_L_anim_rotateX";
	rename -uid "489AEB5F-46E2-3DDF-C410-4B9D16BEB232";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_L_anim_rotateY";
	rename -uid "4CD5CFD8-4477-F71B-3B06-AB9C404454F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_L_anim_rotateZ";
	rename -uid "BBBCCCCF-4C7E-FC78-13E0-9282BF7C5640";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "middle_03_L_anim_scaleX";
	rename -uid "2E6D9FF8-4DD6-3B18-10C3-739E7D32BA9F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_03_L_anim_scaleY";
	rename -uid "5012E89B-4A80-28F7-79B9-1E992EFA7468";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_03_L_anim_scaleZ";
	rename -uid "C48889A0-421A-8780-6F8E-7CAA34279BCE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "middle_02_L_anim_translateX";
	rename -uid "110FBD54-4BF0-C9D3-81EA-B0B03FE92A38";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_02_L_anim_translateY";
	rename -uid "D38B08C9-48B3-9CC4-6BD4-6C94C038D3EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_02_L_anim_translateZ";
	rename -uid "153DC864-43BC-0061-4F2F-69A7D2419142";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_L_anim_rotateX";
	rename -uid "BF0C8086-4FE8-6205-F6B5-B0B4547EB1A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_L_anim_rotateY";
	rename -uid "E2698FCE-4EC8-0F03-F41A-FFBEAE825EE7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_L_anim_rotateZ";
	rename -uid "A9808625-40C5-D5DA-492D-78BA750B9CF0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "middle_02_L_anim_scaleX";
	rename -uid "C6114ABE-4187-57C1-8EFC-31B8B2E8825A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_02_L_anim_scaleY";
	rename -uid "C13C70A3-445B-B73B-6060-6E961F4FE9BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_02_L_anim_scaleZ";
	rename -uid "634E7F42-4D2D-9BEA-212C-C480C3BE7657";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "middle_01_L_anim_translateX";
	rename -uid "16D3EAD3-464F-29B2-D9A2-C29FDB4FE5E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_01_L_anim_translateY";
	rename -uid "D56E290F-4E9A-5093-5F31-88AC52FF4F07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_01_L_anim_translateZ";
	rename -uid "E4BC846A-49A8-B850-6D01-28BBB0783834";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_01_L_anim_rotateX";
	rename -uid "CEFE09A6-4C73-7FF0-1AEE-72BA093C4F08";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_01_L_anim_rotateY";
	rename -uid "956604F4-42F3-A5C0-DD8B-9F8EB6D9FAAD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_01_L_anim_rotateZ";
	rename -uid "835A6D51-4432-841A-8712-C792842E6BFE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "middle_01_L_anim_scaleX";
	rename -uid "B192494B-4F2C-D3E9-A87C-788DFB65B690";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_L_anim_scaleY";
	rename -uid "011B1B1A-44F5-3C47-375D-E59D5180A4AC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_L_anim_scaleZ";
	rename -uid "3F7EDC7D-47C1-3508-76E1-6AB685D46F5A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_L_anim_sticky";
	rename -uid "BC92DD6B-4DD2-511B-FA6B-F18340CB4849";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_L_anim_translateX";
	rename -uid "9361CEC3-4C99-FFE6-B586-46AFA3CB5E55";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_L_anim_translateY";
	rename -uid "FA7C65FF-4895-A270-E9C7-A2BF4B598140";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_L_anim_translateZ";
	rename -uid "EAA3F16B-4FF8-8E69-EC40-8E95F16DD9C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_L_anim_rotateX";
	rename -uid "2A284426-4E5A-4D5A-0E1F-5B8E76AFAEB4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_L_anim_rotateY";
	rename -uid "CD1038EF-4FF6-17B7-00A1-8CA460C37C53";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_L_anim_rotateZ";
	rename -uid "8FFE2163-436C-6725-0A74-AC91752F942B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "index_03_L_anim_scaleX";
	rename -uid "DC0A5678-4659-F404-6B50-A6945D875793";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_03_L_anim_scaleY";
	rename -uid "FCACDC5E-4E06-FFDD-E40F-EE8CAC856E99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_03_L_anim_scaleZ";
	rename -uid "CFE099EC-4B81-9F2B-EAC0-9B912DF88D33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "index_02_L_anim_translateX";
	rename -uid "A9BADE28-44CA-D447-F164-0B96BB2CCEF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_02_L_anim_translateY";
	rename -uid "3868990F-4C6F-514A-35D4-29AC0A0ABC04";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_02_L_anim_translateZ";
	rename -uid "D1CE040D-4D4B-8E17-9E99-7180260EFE7F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_L_anim_rotateX";
	rename -uid "769FF3DC-4ACC-A5F6-8449-9A8609FC5FD7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_L_anim_rotateY";
	rename -uid "B513FFF3-43E7-30D6-71E0-B0B9CCA1BC92";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_L_anim_rotateZ";
	rename -uid "56273D06-411F-4BCE-8FAF-48BAD6B1A1E3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "index_02_L_anim_scaleX";
	rename -uid "3B65F0EE-4C64-3088-122C-26BD1AD5A06D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_02_L_anim_scaleY";
	rename -uid "2B848179-4E62-62B2-7BD8-01B529A147F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_02_L_anim_scaleZ";
	rename -uid "4B862CB2-4677-85D3-DE16-C891DCF738D4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "index_01_L_anim_translateX";
	rename -uid "7643464A-4FF9-0B53-93DF-E2943071E5D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_01_L_anim_translateY";
	rename -uid "18B60B9B-48E2-0351-594C-0493752CFC9A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_01_L_anim_translateZ";
	rename -uid "5F30C053-4AA9-4EE8-5451-30B951522308";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_01_L_anim_rotateX";
	rename -uid "869F269A-41FB-4332-A057-20A2A7817A99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_01_L_anim_rotateY";
	rename -uid "447AFF36-4BD5-5833-D437-A5BC69741BC1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_01_L_anim_rotateZ";
	rename -uid "5469A2D0-4B2A-6E11-F820-A9BAFBBE5D6C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "index_01_L_anim_scaleX";
	rename -uid "14531998-4899-E606-CB26-4AAB11AED85B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_L_anim_scaleY";
	rename -uid "6E7B7673-41BB-E4FB-4D66-ED8205311F1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_L_anim_scaleZ";
	rename -uid "7F661B52-40C4-F4D4-F43E-09B0DFA8C9D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_L_anim_sticky";
	rename -uid "353F7522-474F-4082-ABC5-C18C39905052";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_hand_L_anim_translateX";
	rename -uid "984051AC-4B35-C0A4-5763-C48C6B2DE1C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_hand_L_anim_translateY";
	rename -uid "9FAB5618-4327-1794-3292-C8B467B7D9FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_hand_L_anim_translateZ";
	rename -uid "A2FDEEA1-45E4-81A8-29CA-E2A94A14F437";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -4.1359588453021239e-15 56 -4.1359588453021239e-15
		 100 -4.1359588453021239e-15;
createNode animCurveTA -n "ik_hand_L_anim_rotateX";
	rename -uid "77C42E94-47EE-35F1-A076-3189AE12D527";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_hand_L_anim_rotateY";
	rename -uid "70D02C88-4D3E-4368-A128-0197E130DB82";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_hand_L_anim_rotateZ";
	rename -uid "EDE84A15-44D7-9A0B-27F8-6B90DBB1227D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_stretch";
	rename -uid "D886A139-4547-B2E7-84E9-F4B38440CA76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_elbowLock";
	rename -uid "3AFF8637-4CA4-0828-0C40-6FA1C8E2F003";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_slide";
	rename -uid "82D2CC06-4CCA-46E9-0D51-25A41628FE45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "arm_L_settings_mode";
	rename -uid "B4467C51-4C7D-337A-639E-C59D49B7D4CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ik_hand_L_anim_side";
	rename -uid "0BAB053F-48DB-2EDE-7827-7EB58BE0242A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_bend";
	rename -uid "5FB85479-4384-7704-0C7A-C3B1D2336FE4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_swivel";
	rename -uid "AC227960-4569-AE25-1130-CD9B15A53778";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_pivot";
	rename -uid "F2A3439B-45D8-2148-6063-378C3526CEC8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_swivel";
	rename -uid "1470991A-4CC3-3770-866B-88AFFC60D1BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateX";
	rename -uid "9A2AFDFD-4D42-237A-E8B6-A4B3098F6D6B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateY";
	rename -uid "8D5E0BF0-4765-7FD2-52F8-F3979CAA2D93";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateZ";
	rename -uid "29832547-46F7-8CE5-5CBB-FDB6B02BB029";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "root_anim_translateX";
	rename -uid "18F43784-4036-DDCD-5736-AE954D879976";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "root_anim_translateY";
	rename -uid "408DB499-48CA-02CC-875F-E69A2FE439E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "root_anim_translateZ";
	rename -uid "16ADB245-4E7D-E672-3EFA-1EB22B29957E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "root_anim_rotateX";
	rename -uid "7116AE87-4746-60EC-38BC-20970AF2F45F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "root_anim_rotateY";
	rename -uid "6F4686D1-41B2-7D50-2C7E-D3A85F0B0A55";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "root_anim_rotateZ";
	rename -uid "C0C8BC73-4F56-E388-B4FC-8AB1DA273039";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_chest_ik_anim_translateX";
	rename -uid "4AD5968F-462F-CF82-35F6-D991B06AEEB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_chest_ik_anim_translateY";
	rename -uid "520183CF-4D60-C277-6096-6097A9313CC7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_chest_ik_anim_translateZ";
	rename -uid "EF39D0CC-4D75-E82D-A5F2-79B78CD3C7F1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateX";
	rename -uid "4A463B6C-448C-5FE2-6DF3-0E95338B78BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 -2.8135865501596267 20 -4.3073247924573721
		 38 4.6372215990631025 49 1.6995176239540757 56 0 73 2.1370082411889539 100 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateY";
	rename -uid "E1BC4E8E-41AE-4F9C-146A-FC8850E666DD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 2.2261144259250734 20 2.6369982764334363
		 38 3.5979686858900011 49 1.0012636972789815 56 0 73 0.27671982177375903 100 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateZ";
	rename -uid "D3CBA0D6-4285-99D1-BB68-139A4DB3CB58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 13.303073364097427 20 -3.6398117888774442
		 38 -14.02339104763927 49 -37.832199805001586 56 -57.158786733335681 73 -59.404696262756907
		 100 -57.158786733335681;
createNode animCurveTU -n "torso_chest_ik_anim_stretch";
	rename -uid "40F1F927-41E3-6568-5675-A4886954C8F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "torso_chest_ik_anim_squash";
	rename -uid "F52457DE-40F6-B22D-01B0-629FECF60298";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "torso_chest_ik_anim_twist_amount";
	rename -uid "F3367B21-45A9-A9FA-3690-659FA3170D00";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "torso_chest_ik_anim_autoSpine";
	rename -uid "EF309F81-4C11-9D68-BD23-83AE7E9A2F0E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "torso_chest_ik_anim_rotationInfluence";
	rename -uid "797476F5-45FC-9273-726B-E38C65A54092";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0.25 56 0.25 100 0.25;
createNode animCurveTU -n "torso_settings_mode";
	rename -uid "9E9EF1C0-42A1-89C0-FC4F-F1B7D03C9008";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "torso_mid_ik_anim_translateX";
	rename -uid "3E8CE42C-429B-62DF-88F4-B4A109B2C890";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_mid_ik_anim_translateY";
	rename -uid "26C899C1-4262-026C-D91D-2183C24C158C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_mid_ik_anim_translateZ";
	rename -uid "093B35C9-415A-5F67-04E3-5D983F31ADD6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_mid_ik_anim_rotateX";
	rename -uid "CD882591-400E-5274-23F2-27A614D2848F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 -2.8135865501596267 20 -4.3073247924573721
		 38 4.6372215990631025 49 1.6995176239540757 56 0 73 2.1370082411889539 100 0;
createNode animCurveTA -n "torso_mid_ik_anim_rotateY";
	rename -uid "79A14870-4220-FFA8-A4EA-F6B742AC0C3B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 2.2261144259250734 20 2.6369982764334363
		 38 3.5979686858900011 49 1.0012636972789815 56 0 73 0.27671982177375903 100 0;
createNode animCurveTA -n "torso_mid_ik_anim_rotateZ";
	rename -uid "A020486A-4516-1E16-CFFB-99B769BF47EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 8 ".ktv[0:7]"  0 0 5 13.303073364097427 20 -3.6398117888774442
		 38 -14.02339104763927 49 -37.832199805001586 56 -57.158786733335681 73 -59.404696262756907
		 100 -57.158786733335681;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateX";
	rename -uid "CBA202C2-43AB-3023-4D61-30A3C32FF4E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateY";
	rename -uid "58C4ACD4-4E5D-E73B-24CB-8AB86E199050";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateZ";
	rename -uid "208EED85-4A62-772C-8452-84A4761907B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_R_settings_mode";
	rename -uid "49C37B56-4E6D-301C-8BEF-B3B8C84AD200";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateX";
	rename -uid "DF99D58F-4C27-F4FF-A3B3-589B1D4C152F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateY";
	rename -uid "CCA0279C-4A6E-56CD-AFB3-EDBCDCA225B3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateZ";
	rename -uid "8EA2B55D-4E0F-21B1-7BEE-F1A9D166F268";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_R_heel_ctrl_heelPivot";
	rename -uid "BC1B3D79-4C51-A560-9BE8-5BAD753FE0DC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_R_heel_ctrl_ballPivot";
	rename -uid "06306954-49E0-F564-AD83-DBA09B8385A8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_hip_anim_translateX";
	rename -uid "1FDAE584-4FAA-5D65-EBFB-C99A22475096";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_hip_anim_translateY";
	rename -uid "C150C6A9-4DE0-E8FF-22EC-509EA23DF415";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_hip_anim_translateZ";
	rename -uid "E4ECFB33-4F7D-1EDB-7B18-DC9035E5D899";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_hip_anim_rotateX";
	rename -uid "B7F4E199-4FC5-55C9-CB52-64ABDEE6EE82";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_hip_anim_rotateY";
	rename -uid "7BFDC876-405E-8F91-D3E6-2EB969600EE6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_hip_anim_rotateZ";
	rename -uid "A1D373AD-46F4-3ACA-4132-B38D2E2E1162";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "torso_body_anim_translateX";
	rename -uid "33AF9A6B-49C6-8139-39C2-388E81FA8292";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -67.051121710961382 100 -67.051121710961382;
createNode animCurveTL -n "torso_body_anim_translateY";
	rename -uid "E2E3F978-4CA9-A63F-E1E0-73B2F148B636";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 1.5121441125419706 100 1.5121441125419706;
createNode animCurveTL -n "torso_body_anim_translateZ";
	rename -uid "D513455E-48A8-A9BC-C49A-9EB4CE9C1EE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_body_anim_rotateX";
	rename -uid "4F934BDC-48E3-78B2-5F44-7286C41B48E2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_body_anim_rotateY";
	rename -uid "C5CAAF1D-4581-DA59-B661-EFB189955AE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "torso_body_anim_rotateZ";
	rename -uid "8B97EA8F-4E2A-E676-1212-4686AB767CF2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -32.522161809739863 100 -32.522161809739863;
createNode animCurveTL -n "ik_foot_R_anim_translateX";
	rename -uid "91E16565-49C0-3D37-34E5-2AA6D1EA8F2F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_foot_R_anim_translateY";
	rename -uid "44E4F426-44B8-3A02-7E73-4ABA3CF2FC5F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 36.466543256262085 100 36.466543256262085;
createNode animCurveTL -n "ik_foot_R_anim_translateZ";
	rename -uid "1BB51D66-44C5-14F4-0747-AC88D0BCC5BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_foot_R_anim_rotateX";
	rename -uid "A27E10A9-42C0-3409-A450-C28A6DA16568";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 95.145250680481055 100 95.145250680481055;
createNode animCurveTA -n "ik_foot_R_anim_rotateY";
	rename -uid "AC1E66E4-472C-A805-B492-D1BE04C7406E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_foot_R_anim_rotateZ";
	rename -uid "28FBCEEC-41DA-803A-0A03-2FA5B1989A05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_R_anim_knee_twist";
	rename -uid "2C318BE8-42BE-E533-363C-0684695A3805";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_R_anim_stretch";
	rename -uid "3B114A5C-4A6C-C6C0-C6F4-48B01851CDA7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_R_anim_squash";
	rename -uid "8C375196-49A8-A607-1B5A-0498C6B1FE3E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_R_anim_toeCtrlVis";
	rename -uid "64C70874-4D05-216F-59BC-A5A86933E289";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
	setAttr -s 3 ".kit[0:2]"  9 18 9;
createNode animCurveTU -n "ik_foot_R_anim_stretchBias";
	rename -uid "671A0313-4C44-78F7-3D7F-979D05A8B337";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateX";
	rename -uid "F928B7FB-479D-FC0D-7D8F-B4A5953E4C0A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateY";
	rename -uid "7DB1964F-483B-32F4-09DF-D9B6A7086E84";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateZ";
	rename -uid "32F17832-4516-E420-1C75-E2820E88EF31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_foot_L_anim_translateX";
	rename -uid "245FAC6C-4FF3-B1F2-83C2-CCA5B609405B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 14.900364787403561 100 14.900364787403561;
createNode animCurveTL -n "ik_foot_L_anim_translateY";
	rename -uid "E3914B3A-4DB4-0ADB-5992-C5BCC91E199C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -26.012674385575956 100 -26.012674385575956;
createNode animCurveTL -n "ik_foot_L_anim_translateZ";
	rename -uid "C88E4FC9-4253-50D2-CD7C-2192E514DBB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateX";
	rename -uid "3871D312-4689-5AAB-0993-EE9988E201CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateY";
	rename -uid "B1B5C3BE-49E0-C8C5-DB4B-92AFD083C0F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateZ";
	rename -uid "228AAC1C-4E2D-1190-CD58-B98DAC91B41A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -17.951610914797147 100 -17.951610914797147;
createNode animCurveTU -n "ik_foot_L_anim_knee_twist";
	rename -uid "24501FEC-4EE7-C42D-C922-D4978B28CF80";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 26.6 100 26.6;
createNode animCurveTU -n "ik_foot_L_anim_stretch";
	rename -uid "D22A6D77-466C-BB3B-5240-7AB2B17CE4AF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_L_anim_squash";
	rename -uid "637CF2FF-462C-1A64-51F7-9C9354052337";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ik_foot_L_anim_toeCtrlVis";
	rename -uid "50DE2505-4B14-4AD7-6671-E08B231AF883";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
	setAttr -s 3 ".kit[0:2]"  9 18 9;
createNode animCurveTU -n "ik_foot_L_anim_stretchBias";
	rename -uid "5EB4019A-41CE-25F4-D2AF-0F946D10FA56";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_L_settings_mode";
	rename -uid "5ADB00BA-49DC-A3B7-9193-D582EA546C8F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateX";
	rename -uid "5434EE61-4438-DF71-8350-B3BB6550B0C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateY";
	rename -uid "09D08D63-428B-2BAC-0EED-EF8A53ADF59E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateZ";
	rename -uid "8B63A196-4F04-47C3-6416-24AAF677B447";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateX";
	rename -uid "96C51124-426C-536B-8476-AFA47449CD67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateY";
	rename -uid "E368D8BE-4D38-9212-5BD8-B88DA0040AF0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateZ";
	rename -uid "1AF766DD-430E-B905-C1EA-2AA534A09A1B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_L_heel_ctrl_heelPivot";
	rename -uid "42493269-4ECD-C1E3-A978-1D881AADB584";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "leg_L_heel_ctrl_ballPivot";
	rename -uid "129B0CAC-48EE-6312-1C96-17A2F275C3D4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateX";
	rename -uid "9C04DDFD-4F45-A234-5B74-7E8AE184071D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateY";
	rename -uid "44E7896B-4660-B964-D9AA-01A54DC2A420";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateZ";
	rename -uid "E23D307C-4118-6736-6874-A0AE449B8B64";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "fk_head_anim_translateX";
	rename -uid "AD880F0B-452B-FBB5-99B6-01A1731721CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTL -n "fk_head_anim_translateY";
	rename -uid "7296F1F3-4138-2CFB-5B5F-43AC97C993FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTL -n "fk_head_anim_translateZ";
	rename -uid "7016225C-4292-FBBD-A388-33AB95BBA817";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTA -n "fk_head_anim_rotateX";
	rename -uid "ECE7D285-499B-45FF-891E-96ADA422FF86";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 -4.8415607248258787 20 -5.6202364702615135
		 38 3.3097219058958127 49 1.1750476487231389 65 -2.6896514446034474 100 0;
createNode animCurveTA -n "fk_head_anim_rotateY";
	rename -uid "94583E8E-44F4-803F-AF80-AB98F5E0EA37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 -0.46158817048909162 20 -0.22666317883689097
		 38 1.2300012351788585 49 0.16547439735694444 65 0 100 0;
createNode animCurveTA -n "fk_head_anim_rotateZ";
	rename -uid "9B10DCDC-4387-B52B-40CA-FF939B073B3A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 8.2091739641874781 20 -4.7284039032808494
		 38 0.0067146745708793866 49 12.37169688440577 65 -4.5244292046321801 100 0;
createNode animCurveTU -n "fk_head_anim_scaleX";
	rename -uid "484174E1-4476-B3E4-627E-9B9D284E27CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 65 1 100 1;
createNode animCurveTU -n "fk_head_anim_scaleY";
	rename -uid "DF5BE309-458E-0594-1734-52B10798A885";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 65 1 100 1;
createNode animCurveTU -n "fk_head_anim_scaleZ";
	rename -uid "64F2BE22-4365-45A8-E39E-7CBB8D397D9F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 65 1 100 1;
createNode animCurveTL -n "fk_neck_01_anim_translateX";
	rename -uid "D922438A-4D85-5E32-6167-95B54C3F3F23";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTL -n "fk_neck_01_anim_translateY";
	rename -uid "7CBB1298-49F2-ED3C-B84B-6388013DC2EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTL -n "fk_neck_01_anim_translateZ";
	rename -uid "C2A4D4BC-46AB-5324-5F10-7BA2824FD286";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 65 0 100 0;
createNode animCurveTA -n "fk_neck_01_anim_rotateX";
	rename -uid "4EF464F8-40C8-8D34-B7C6-C3A61D4F4079";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 -4.8415607248258787 20 -5.6202364702615135
		 38 3.3097219058958127 49 1.1750476487231389 65 -2.6896514446034474 100 0;
createNode animCurveTA -n "fk_neck_01_anim_rotateY";
	rename -uid "BC9BFA7D-4BDD-4A75-B68D-24AFFA32191F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 -0.46158817048909162 20 -0.22666317883689097
		 38 1.2300012351788585 49 0.16547439735694444 65 0 100 0;
createNode animCurveTA -n "fk_neck_01_anim_rotateZ";
	rename -uid "251E1E6B-4A40-ACBB-F9D1-4A8F882D51B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 7 ".ktv[0:6]"  0 0 5 8.2091739641874781 20 -4.7284039032808494
		 38 0.0067146745708793866 49 12.37169688440577 65 -4.5244292046321801 100 0;
createNode animCurveTL -n "ik_clavicle_R_anim_translateX";
	rename -uid "81A1B668-4A8D-866C-8B57-668BBCCF56A8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 -8.1356707014881779e-14;
createNode animCurveTL -n "ik_clavicle_R_anim_translateY";
	rename -uid "A443FDD8-4C9C-B254-AF5C-A6829AA33518";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -0.042702137314829663 100 -0.042702137314829663;
createNode animCurveTL -n "ik_clavicle_R_anim_translateZ";
	rename -uid "E1F77169-49EF-81AB-85CA-FBB48B8037D2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 7.6684400037565164 100 7.6684400037565164;
createNode animCurveTU -n "arm_R_settings_clavMode";
	rename -uid "A75DA4FB-4713-78E6-18BC-F586707593DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "pinky_01_R_anim_translateX";
	rename -uid "311BC2DD-4C05-EB22-7725-5AB087A3E1C9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_01_R_anim_translateY";
	rename -uid "20D9A449-400D-5C6A-5A6E-AA9FF08EAF9B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_01_R_anim_translateZ";
	rename -uid "582641CA-43F3-0EC4-AD4D-67BF6A5EC09B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_01_R_anim_rotateX";
	rename -uid "21923CC6-4C8C-5DCB-B854-1A9721682F25";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -0.26372102144185688 100 -0.26372102144185688;
createNode animCurveTA -n "pinky_01_R_anim_rotateY";
	rename -uid "4D1B90DC-43F3-00ED-4FB6-C38BC20E25FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -19.535239401525175 100 -19.535239401525175;
createNode animCurveTA -n "pinky_01_R_anim_rotateZ";
	rename -uid "7F1C7994-47DD-4703-94F5-4AB3E62F0878";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 2.4152158819858318 100 2.4152158819858318;
createNode animCurveTU -n "pinky_01_R_anim_scaleX";
	rename -uid "B15655AF-4FF3-6150-5004-EE93960430E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleY";
	rename -uid "32B792B2-4850-828B-BFD2-2F9B55233905";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleZ";
	rename -uid "206B46B2-48FE-2D65-F422-34980C809317";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_01_R_anim_sticky";
	rename -uid "C29347A1-4F7D-47D6-0742-AA99E9E314BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_R_anim_translateX";
	rename -uid "BF250959-46DF-03E4-3260-0E9C7A7AE36D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_R_anim_translateY";
	rename -uid "35ADE5AB-49F7-1F25-FF80-91A3B8A0DC7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_03_R_anim_translateZ";
	rename -uid "CA4D29E3-4D7C-632D-6CCF-3BAE5D5678C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_R_anim_rotateX";
	rename -uid "620E98D5-4F75-812B-43F8-09A5E67F9E91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_R_anim_rotateY";
	rename -uid "E25ADFE8-46F3-05B6-4D85-40B3311B98DB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_03_R_anim_rotateZ";
	rename -uid "37807B41-448E-FE77-5647-CCB2AAFC5EBC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ring_03_R_anim_scaleX";
	rename -uid "10B14ECE-4401-114C-611F-099B8D652EDC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_03_R_anim_scaleY";
	rename -uid "1B798A50-4560-97AD-3AF8-4B9906E6E1BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_03_R_anim_scaleZ";
	rename -uid "98AD32E1-4D3C-C283-2715-8F803C467487";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "ring_02_R_anim_translateX";
	rename -uid "E4B1336F-4686-AE53-D9EF-08A506F28C33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_02_R_anim_translateY";
	rename -uid "796C47E5-4EE2-DABF-3227-5B8215DF14A2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_02_R_anim_translateZ";
	rename -uid "12E34F79-4EFF-7844-A563-BEA9ADADD364";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_R_anim_rotateX";
	rename -uid "556579FF-452B-0B26-B729-0CA0FBB180C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_R_anim_rotateY";
	rename -uid "6136CDD9-4A40-A600-DC42-D09775BA5521";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_02_R_anim_rotateZ";
	rename -uid "CB78CBFD-47A3-A708-CD6C-93A260AAC555";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "ring_02_R_anim_scaleX";
	rename -uid "A850F75A-4EE2-217B-36F7-46B567F2213B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_02_R_anim_scaleY";
	rename -uid "5AC49D45-47E3-E73C-BC85-67AD5B67EC11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_02_R_anim_scaleZ";
	rename -uid "D5A0CAA7-4E32-659B-9042-90A93A57390D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "ring_01_R_anim_translateX";
	rename -uid "AD8CFDAC-4315-F29E-7DC2-539E2A451F4C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_01_R_anim_translateY";
	rename -uid "5114C15B-48EF-86BF-5F90-0A918F3254A1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ring_01_R_anim_translateZ";
	rename -uid "DD0F9523-4C48-6C6C-F7E6-BCBD7942C2E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "ring_01_R_anim_rotateX";
	rename -uid "7825A463-400A-E671-80C4-9D83D5E74156";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -0.61490921675849863 100 -0.61490921675849863;
createNode animCurveTA -n "ring_01_R_anim_rotateY";
	rename -uid "A15B9249-4A2A-0966-C15F-0F87E660784A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -6.5599338959418487 100 -6.5599338959418487;
createNode animCurveTA -n "ring_01_R_anim_rotateZ";
	rename -uid "3DACA33A-4BAA-CD98-F47F-A2A8517AC79A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 2.9716056559086534 100 2.9716056559086534;
createNode animCurveTU -n "ring_01_R_anim_scaleX";
	rename -uid "9C645B59-4FEF-28A7-D82D-6689B1964D8D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_R_anim_scaleY";
	rename -uid "CE6D0C30-434C-93C6-44F1-0A83966503B3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_R_anim_scaleZ";
	rename -uid "82D44D62-44B1-D884-54AE-F1AEDEA296EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "ring_01_R_anim_sticky";
	rename -uid "521F86B1-4DFF-5226-4AA6-7191A1941897";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_R_anim_translateX";
	rename -uid "11FAEA88-4F47-7E01-773E-33BA5E1D33B1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_R_anim_translateY";
	rename -uid "4E522026-47B6-0EC8-C250-8D8B299F2EC6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_03_R_anim_translateZ";
	rename -uid "BD87C235-4077-ECEB-E4EB-88945CA6005E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_R_anim_rotateX";
	rename -uid "8163024D-47C9-6632-33BE-04B945C8F41C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_R_anim_rotateY";
	rename -uid "46391D12-4EF4-2930-5D92-F884B24395AE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_03_R_anim_rotateZ";
	rename -uid "65DA87E8-4A29-BC47-0AB3-12977CE70FB4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "middle_03_R_anim_scaleX";
	rename -uid "B67776EE-4C3A-6AA3-DE26-879DC0F70E16";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_03_R_anim_scaleY";
	rename -uid "BF358AE7-4545-1F3B-3FBD-1C99B0BBBBE3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_03_R_anim_scaleZ";
	rename -uid "C56E198C-47DD-2261-9A54-3BB6639E0907";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_03_R_anim_translateX";
	rename -uid "CE49E01E-4E32-BB97-8794-2C8C79329D91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_03_R_anim_translateY";
	rename -uid "16A87D20-4AB1-A907-D90F-BF8A17CF1F68";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_03_R_anim_translateZ";
	rename -uid "64EFE8E3-4D07-0D32-094B-12B36CF694EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateX";
	rename -uid "F04BF6C8-4627-0C1E-94A7-D4A7EAC46EAA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateY";
	rename -uid "93E4A90B-431D-DC02-7D2F-A3B0D6D8CB7E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateZ";
	rename -uid "B0258524-4A70-21CB-154D-E798E158E8E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "thumb_03_R_anim_scaleX";
	rename -uid "348BF32C-442F-AD22-1801-96A76C43D977";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleY";
	rename -uid "070A19FC-4DA6-DBD4-BB87-D5ACD7687756";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleZ";
	rename -uid "AFC40A0A-403F-CE95-E394-25AD61581311";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_02_R_anim_translateX";
	rename -uid "019B0EB9-4B08-4074-3813-DDB4818F7991";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_02_R_anim_translateY";
	rename -uid "32374E11-483E-45DF-89D9-D2A86E071ABF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_02_R_anim_translateZ";
	rename -uid "611499CE-48A2-9D17-2060-1098F0CE91EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateX";
	rename -uid "46D5DD8A-49B1-1E39-D993-C09F7606EE85";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateY";
	rename -uid "C5A1E0B2-4227-608B-12C1-6EAE9CA4478F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateZ";
	rename -uid "42F4A05B-4271-E9B5-DCB3-23B47DBC1EB4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "thumb_02_R_anim_scaleX";
	rename -uid "2F6CCD34-4764-54B8-8CDC-C4A5D6B72772";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleY";
	rename -uid "121CF0D5-4B05-C557-3728-C6B0BFD3D059";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleZ";
	rename -uid "BC281E12-484F-90E6-1F52-2C91A4285910";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "thumb_01_R_anim_translateX";
	rename -uid "0ADF8359-4596-40F0-B129-CBAF7241FB3C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_01_R_anim_translateY";
	rename -uid "9B091D06-494B-9E38-A660-F1A1AE5DC3C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "thumb_01_R_anim_translateZ";
	rename -uid "A6089FE3-4DB7-420B-30BC-56940CC6CC3D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "thumb_01_R_anim_rotateX";
	rename -uid "5D2CBC61-4C73-D7E8-92A2-049763FEB52C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 5.2081393444068578 100 5.2081393444068578;
createNode animCurveTA -n "thumb_01_R_anim_rotateY";
	rename -uid "06770B5A-4C34-4C67-C363-DFBC07664BF3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -10.9901093340532 100 -10.9901093340532;
createNode animCurveTA -n "thumb_01_R_anim_rotateZ";
	rename -uid "E1D5DCA1-4E20-B905-CC4D-0FB609ED7798";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -10.782167875237267 100 -10.782167875237267;
createNode animCurveTU -n "thumb_01_R_anim_scaleX";
	rename -uid "2A8B4EF0-490E-2761-29FD-B798F19108E7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleY";
	rename -uid "B992914A-4929-F828-4CB0-1BB3EA09D2AE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleZ";
	rename -uid "C3CBCE7C-4EE8-BD6A-C3A4-03AA98BB38E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "pinky_03_R_anim_translateX";
	rename -uid "B05681DE-479E-0075-AFF4-F68C9A9F9A5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_03_R_anim_translateY";
	rename -uid "E2292D68-4E02-560A-DC6A-F09A47153B63";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_03_R_anim_translateZ";
	rename -uid "22C45B5E-42A8-35E0-CDB4-6386EB65693E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateX";
	rename -uid "F995178A-45BD-5764-67B2-FE9B932BA0FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateY";
	rename -uid "19C861E7-4786-54FB-F205-0A858D8A4B39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateZ";
	rename -uid "91EECF3F-4D76-75A5-303D-F0AAC17FD8E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "pinky_03_R_anim_scaleX";
	rename -uid "FF92C642-4F89-6183-EED9-739AA0E52559";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleY";
	rename -uid "CED2981A-4340-BCE9-7039-7DA0F3CF2172";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleZ";
	rename -uid "1C235357-417C-795A-6FB3-8098B686DE42";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "pinky_02_R_anim_translateX";
	rename -uid "93BB54F5-4852-14FF-E628-B28B1AE80444";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_02_R_anim_translateY";
	rename -uid "6C7AE8DE-446A-B8D0-18D2-03ADA7F9B507";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "pinky_02_R_anim_translateZ";
	rename -uid "490531B8-4DF7-D219-D2DA-86B4C43C034B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateX";
	rename -uid "1C9D0B20-4381-525B-3F14-13883CB80FBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateY";
	rename -uid "302461EF-4766-74DD-F54F-6C9AD53B53BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateZ";
	rename -uid "7C1780B9-4119-E496-6F06-DBA811A0F1CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "pinky_02_R_anim_scaleX";
	rename -uid "FAF9CF5C-4513-BA08-A1B5-DFA23B7615CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleY";
	rename -uid "A65CBE02-407E-5D9E-0AC7-688D2A14B44F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleZ";
	rename -uid "A015D3C2-4350-1DED-EF7C-CA9FE235D7FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "middle_02_R_anim_translateX";
	rename -uid "C63564BA-4580-BF2A-EFCF-EDA649043936";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_02_R_anim_translateY";
	rename -uid "C397DE71-4A82-9651-C907-7E8F50887B11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_02_R_anim_translateZ";
	rename -uid "71C08405-401C-670F-E016-E4A41E8827DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_R_anim_rotateX";
	rename -uid "A9464441-41C0-7AB6-CE70-B082B8076F6F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_R_anim_rotateY";
	rename -uid "C67854F3-40A8-104D-7AE4-319A2ED77F1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_02_R_anim_rotateZ";
	rename -uid "93C56F54-45D1-74E6-C766-CC948E8E65CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "middle_02_R_anim_scaleX";
	rename -uid "DCEC61FD-4AD2-2000-8F3C-8DB7A566EC7E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_02_R_anim_scaleY";
	rename -uid "F4869C04-4D98-D979-13DA-AF98D90D58F6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_02_R_anim_scaleZ";
	rename -uid "08841EF5-47E9-58AC-13A5-FB81085FDF82";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "middle_01_R_anim_translateX";
	rename -uid "0BEC16CD-45DF-1944-4CFD-0E986FAD7C59";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_01_R_anim_translateY";
	rename -uid "3DA287AE-4EF4-A94E-D7DE-A8BB01908C80";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "middle_01_R_anim_translateZ";
	rename -uid "88B8A8C5-4E65-2CE5-05F1-6C8FD442777C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "middle_01_R_anim_rotateX";
	rename -uid "FAB7C546-4B5A-3B00-E81F-EAAB51093F51";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0.23466326854549116 100 0.23466326854549116;
createNode animCurveTA -n "middle_01_R_anim_rotateY";
	rename -uid "FA5E96C7-4139-DEA8-FBC8-CFB93E34D6BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -1.5358597452942404 100 -1.5358597452942404;
createNode animCurveTA -n "middle_01_R_anim_rotateZ";
	rename -uid "8E72F2AC-4BDE-5502-9772-F3BABCC32C5A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 6.2776143808628264 100 6.2776143808628264;
createNode animCurveTU -n "middle_01_R_anim_scaleX";
	rename -uid "370C8387-45B2-6AB3-D38C-6C9132B9BBB1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_R_anim_scaleY";
	rename -uid "18A7FF2D-4B18-6D8B-2023-8C90B224AD6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_R_anim_scaleZ";
	rename -uid "E412C598-4340-F941-8BE4-FD9F51E4DFD9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "middle_01_R_anim_sticky";
	rename -uid "B3A7DB3D-40A9-57AA-DEF2-D2B8AC8FD17D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_R_anim_translateX";
	rename -uid "FF47F3E6-4695-FFD8-3021-138A1BAF9A15";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_R_anim_translateY";
	rename -uid "8368EE37-4D08-24F0-B3D7-B790090C823A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_03_R_anim_translateZ";
	rename -uid "01BCE9CB-4085-618C-69D4-F7B75D559DF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_R_anim_rotateX";
	rename -uid "80AB387A-41E8-27DA-DC70-52BD1B11A411";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_R_anim_rotateY";
	rename -uid "4BCD6746-46CD-588C-CCE9-5F8BDCA3A270";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_03_R_anim_rotateZ";
	rename -uid "0CB86A25-44C4-7B02-9612-E897550FF5E9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "index_03_R_anim_scaleX";
	rename -uid "E6EA1538-4A09-C356-DB30-898FC93624C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_03_R_anim_scaleY";
	rename -uid "101A496C-4D4E-2DC7-AF17-53BD6BC76D0D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_03_R_anim_scaleZ";
	rename -uid "C5C4299A-496D-1D29-82BC-56ACF31F30C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "index_02_R_anim_translateX";
	rename -uid "ACDA5F7C-4833-2BC5-8532-B4ABAA353B87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_02_R_anim_translateY";
	rename -uid "52F97182-4C52-6BBE-FC88-ABB1540160ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_02_R_anim_translateZ";
	rename -uid "DA6E3BEF-4386-A83B-1369-CEAE2974679C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_R_anim_rotateX";
	rename -uid "15561F06-45DA-D66C-7BEF-E7AE4D992401";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_R_anim_rotateY";
	rename -uid "4FF77422-46F9-379B-AC29-B9A2FAF15B24";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_02_R_anim_rotateZ";
	rename -uid "4C353496-4A5D-CE3D-AE3F-CF8448CA9D99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "index_02_R_anim_scaleX";
	rename -uid "F773D3E6-46F0-C4B8-E9A0-3D83E1549ED5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_02_R_anim_scaleY";
	rename -uid "8CE1787E-479B-C3D0-2D6A-DDB2EAD05AC9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_02_R_anim_scaleZ";
	rename -uid "33959ED1-4011-258B-FDE1-728824FC8C04";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTL -n "index_01_R_anim_translateX";
	rename -uid "ED524DF3-4C6C-5111-844A-71B90BDFDDF6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_01_R_anim_translateY";
	rename -uid "05386071-4EC9-C848-17A6-FCB722BDCF28";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "index_01_R_anim_translateZ";
	rename -uid "E2F469E4-4FB0-E68B-81E2-B0BA68BDDA18";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTA -n "index_01_R_anim_rotateX";
	rename -uid "DFCA5650-465B-E93C-B9C8-7A98F7253B00";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 1.171933900732107 100 1.171933900732107;
createNode animCurveTA -n "index_01_R_anim_rotateY";
	rename -uid "FFD0A70D-4EA0-703F-BB18-0BA487CE0F07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 13.2235619306733 100 13.2235619306733;
createNode animCurveTA -n "index_01_R_anim_rotateZ";
	rename -uid "B6EFED09-4DA2-B7B6-4F61-DE9F6E96D7F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 8.0362547989413358 100 8.0362547989413358;
createNode animCurveTU -n "index_01_R_anim_scaleX";
	rename -uid "345732B7-4518-2300-E092-F78C0B3BC166";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_R_anim_scaleY";
	rename -uid "EE8BB3B8-4AD3-04B4-69C2-ACB6E1326379";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_R_anim_scaleZ";
	rename -uid "EB9AC3AF-4468-5F48-2F11-CA8C1FD6217A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "index_01_R_anim_sticky";
	rename -uid "AED50A2C-47A7-5190-5AEA-9DB2705EF27C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_hand_R_anim_translateX";
	rename -uid "36DAEA49-4AAF-0FBD-730D-328D639E127F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 58.311697616137451 59 61.462111161804678
		 100 61.462111161804678;
createNode animCurveTL -n "ik_hand_R_anim_translateY";
	rename -uid "6C8C55B5-42B4-A569-CC7D-8DB6783AF81D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 -31.392432206001125 59 -40.540155261707667
		 100 -40.540155261707667;
createNode animCurveTL -n "ik_hand_R_anim_translateZ";
	rename -uid "49E7B012-4E58-BABC-6B03-1E83FD7C6353";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 -124.06763409124142 59 -146.85208698961819
		 100 -146.85208698961819;
createNode animCurveTA -n "ik_hand_R_anim_rotateX";
	rename -uid "80E98791-4AA0-684D-2173-FA8F9BC0C760";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 0.91988953453900246 59 0.76531257871607572
		 100 0.76531257871607572;
createNode animCurveTA -n "ik_hand_R_anim_rotateY";
	rename -uid "BF4E68F6-47D4-64E0-417D-6DA4063DB112";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 -36.242902659416949 59 -3.4659125531440726
		 100 -3.4659125531440726;
createNode animCurveTA -n "ik_hand_R_anim_rotateZ";
	rename -uid "2F72F139-466B-ABD1-C3DF-D6B6CA95960B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 53 87.004925179828533 59 90.111384085243245
		 100 90.111384085243245;
createNode animCurveTU -n "ik_hand_R_anim_stretch";
	rename -uid "E08906EF-4CE4-5FC5-6C99-13B382C6F67B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_elbowLock";
	rename -uid "4A51957B-42FD-8A0A-085A-AA821C0B2554";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_slide";
	rename -uid "0D708972-4E2A-0F36-B7AA-CFABB36E30DC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "arm_R_settings_mode";
	rename -uid "01313F1E-4004-9963-8E37-52829AFD58EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 59 1 100 1;
createNode animCurveTU -n "ik_hand_R_anim_side";
	rename -uid "8A0106E6-4264-62C6-235B-F38CE5BA67F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_mid_bend";
	rename -uid "3AC2DBA4-44A4-651F-BE89-CDAD2590023E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_mid_swivel";
	rename -uid "97524D4A-41DD-B0A2-4662-A891C14B38C3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_tip_pivot";
	rename -uid "B54BE91C-4FF2-FB28-747C-9F800BA00EF4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTU -n "ik_hand_R_anim_tip_swivel";
	rename -uid "2F2A181C-4DD4-0B6C-83A9-ECAF56594DE0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 59 0 100 0;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateX";
	rename -uid "89674787-4665-8D5D-8217-F397CA49EEC8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -4.5262218346499452 100 -4.5262218346499452;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateY";
	rename -uid "DD7AE1E2-425C-9F12-D34A-C1968E518F39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 -105.3403113473878 100 -105.3403113473878;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateZ";
	rename -uid "084A7911-4906-8294-4D50-F5B7F782D451";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 16.9211996943705 100 16.9211996943705;
createNode animCurveTL -n "ik_clavicle_L_anim_translateX";
	rename -uid "2EDADFC1-4718-250F-D8A9-5DB0429E0549";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_clavicle_L_anim_translateY";
	rename -uid "0CC041E0-4695-7A4A-3E98-9D9E0A917270";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTL -n "ik_clavicle_L_anim_translateZ";
	rename -uid "A9BD3A8C-48FA-BA96-9289-239999A284D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 56 0 100 0;
createNode animCurveTU -n "arm_L_settings_clavMode";
	rename -uid "6ADE2584-4ECA-2C41-1F6F-AD94EA0D7882";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 56 1 100 1;
createNode animCurveTU -n "FP_Cam_blendParent1";
	rename -uid "BDF8905A-43D1-FB1B-FE5A-7CB978290039";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  56 1 100 1;
createNode hyperLayout -n "hyperLayout4";
	rename -uid "F2ABDB7F-4C4F-AB35-AE71-439B6E4879D0";
	setAttr ".ihi" 0;
createNode hyperLayout -n "hyperLayout5";
	rename -uid "B293171C-476B-6904-8F6A-BC9D04E75C87";
	setAttr ".ihi" 0;
select -ne :time1;
	setAttr ".o" 53;
	setAttr ".unw" 53;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 90 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 13 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 203 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
	setAttr -s 60 ".tx";
select -ne :initialShadingGroup;
	setAttr -s 141 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 123 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
select -ne :ikSystem;
	setAttr -s 6 ".sol";
connectAttr "pairBlend1.otx" "Rig_UE4ManControllerRN.phl[422]";
connectAttr "pairBlend1.oty" "Rig_UE4ManControllerRN.phl[423]";
connectAttr "pairBlend1.otz" "Rig_UE4ManControllerRN.phl[424]";
connectAttr "pairBlend1.orx" "Rig_UE4ManControllerRN.phl[425]";
connectAttr "pairBlend1.ory" "Rig_UE4ManControllerRN.phl[426]";
connectAttr "pairBlend1.orz" "Rig_UE4ManControllerRN.phl[427]";
connectAttr "FP_Cam_visibility.o" "Rig_UE4ManControllerRN.phl[428]";
connectAttr "Rig_UE4ManControllerRN.phl[429]" "pairBlend1.w";
connectAttr "FP_Cam_blendParent1.o" "Rig_UE4ManControllerRN.phl[430]";
connectAttr "FP_Cam_scaleX.o" "Rig_UE4ManControllerRN.phl[431]";
connectAttr "FP_Cam_scaleY.o" "Rig_UE4ManControllerRN.phl[432]";
connectAttr "FP_Cam_scaleZ.o" "Rig_UE4ManControllerRN.phl[433]";
connectAttr "Rig_UE4ManControllerRN.phl[434]" "pairBlend1.itx2";
connectAttr "Rig_UE4ManControllerRN.phl[435]" "pairBlend1.ity2";
connectAttr "Rig_UE4ManControllerRN.phl[436]" "pairBlend1.itz2";
connectAttr "Rig_UE4ManControllerRN.phl[437]" "pairBlend1.irx2";
connectAttr "Rig_UE4ManControllerRN.phl[438]" "pairBlend1.iry2";
connectAttr "Rig_UE4ManControllerRN.phl[439]" "pairBlend1.irz2";
connectAttr "Rig_UE4ManControllerRN.phl[440]" "groupParts5.ig";
connectAttr "groupId5.id" "Rig_UE4ManControllerRN.phl[1]";
connectAttr "groupParts5.og" "Rig_UE4ManControllerRN.phl[2]";
connectAttr "root_anim_translateX.o" "Rig_UE4ManControllerRN.phl[3]";
connectAttr "root_anim_translateY.o" "Rig_UE4ManControllerRN.phl[4]";
connectAttr "root_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[5]";
connectAttr "root_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[6]";
connectAttr "root_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[7]";
connectAttr "root_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[8]";
connectAttr "arm_L_settings_mode.o" "Rig_UE4ManControllerRN.phl[9]";
connectAttr "arm_L_settings_clavMode.o" "Rig_UE4ManControllerRN.phl[10]";
connectAttr "arm_L_ik_elbow_anim_translateX.o" "Rig_UE4ManControllerRN.phl[11]";
connectAttr "arm_L_ik_elbow_anim_translateY.o" "Rig_UE4ManControllerRN.phl[12]";
connectAttr "arm_L_ik_elbow_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[13]";
connectAttr "ik_hand_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[14]";
connectAttr "ik_hand_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[15]";
connectAttr "ik_hand_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[16]";
connectAttr "ik_hand_L_anim_stretch.o" "Rig_UE4ManControllerRN.phl[17]";
connectAttr "ik_hand_L_anim_slide.o" "Rig_UE4ManControllerRN.phl[18]";
connectAttr "ik_hand_L_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[19]";
connectAttr "ik_hand_L_anim_side.o" "Rig_UE4ManControllerRN.phl[20]";
connectAttr "ik_hand_L_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[21]";
connectAttr "ik_hand_L_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[22]";
connectAttr "ik_hand_L_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[23]";
connectAttr "ik_hand_L_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[24]";
connectAttr "ik_hand_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[25]";
connectAttr "ik_hand_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[26]";
connectAttr "ik_hand_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[27]";
connectAttr "index_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[28]";
connectAttr "index_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[29]";
connectAttr "index_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[30]";
connectAttr "index_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[31]";
connectAttr "index_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[32]";
connectAttr "index_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[33]";
connectAttr "index_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[34]";
connectAttr "index_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[35]";
connectAttr "index_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[36]";
connectAttr "index_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[37]";
connectAttr "index_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[38]";
connectAttr "index_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[39]";
connectAttr "index_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[40]";
connectAttr "index_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[41]";
connectAttr "index_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[42]";
connectAttr "index_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[43]";
connectAttr "index_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[44]";
connectAttr "index_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[45]";
connectAttr "index_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[46]";
connectAttr "index_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[47]";
connectAttr "index_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[48]";
connectAttr "index_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[49]";
connectAttr "index_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[50]";
connectAttr "index_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[51]";
connectAttr "index_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[52]";
connectAttr "index_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[53]";
connectAttr "index_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[54]";
connectAttr "index_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[55]";
connectAttr "middle_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[56]";
connectAttr "middle_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[57]";
connectAttr "middle_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[58]";
connectAttr "middle_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[59]";
connectAttr "middle_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[60]";
connectAttr "middle_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[61]";
connectAttr "middle_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[62]";
connectAttr "middle_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[63]";
connectAttr "middle_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[64]";
connectAttr "middle_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[65]";
connectAttr "middle_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[66]";
connectAttr "middle_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[67]";
connectAttr "middle_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[68]";
connectAttr "middle_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[69]";
connectAttr "middle_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[70]";
connectAttr "middle_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[71]";
connectAttr "middle_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[72]";
connectAttr "middle_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[73]";
connectAttr "middle_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[74]";
connectAttr "middle_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[75]";
connectAttr "middle_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[76]";
connectAttr "middle_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[77]";
connectAttr "middle_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[78]";
connectAttr "middle_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[79]";
connectAttr "middle_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[80]";
connectAttr "middle_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[81]";
connectAttr "middle_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[82]";
connectAttr "middle_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[83]";
connectAttr "ring_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[84]";
connectAttr "ring_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[85]";
connectAttr "ring_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[86]";
connectAttr "ring_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[87]";
connectAttr "ring_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[88]";
connectAttr "ring_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[89]";
connectAttr "ring_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[90]";
connectAttr "ring_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[91]";
connectAttr "ring_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[92]";
connectAttr "ring_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[93]";
connectAttr "ring_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[94]";
connectAttr "ring_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[95]";
connectAttr "ring_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[96]";
connectAttr "ring_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[97]";
connectAttr "ring_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[98]";
connectAttr "ring_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[99]";
connectAttr "ring_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[100]";
connectAttr "ring_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[101]";
connectAttr "ring_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[102]";
connectAttr "ring_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[103]";
connectAttr "ring_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[104]";
connectAttr "ring_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[105]";
connectAttr "ring_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[106]";
connectAttr "ring_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[107]";
connectAttr "ring_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[108]";
connectAttr "ring_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[109]";
connectAttr "ring_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[110]";
connectAttr "ring_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[111]";
connectAttr "pinky_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[112]";
connectAttr "pinky_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[113]";
connectAttr "pinky_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[114]";
connectAttr "pinky_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[115]";
connectAttr "pinky_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[116]";
connectAttr "pinky_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[117]";
connectAttr "pinky_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[118]";
connectAttr "pinky_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[119]";
connectAttr "pinky_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[120]";
connectAttr "pinky_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[121]";
connectAttr "pinky_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[122]";
connectAttr "pinky_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[123]";
connectAttr "pinky_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[124]";
connectAttr "pinky_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[125]";
connectAttr "pinky_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[126]";
connectAttr "pinky_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[127]";
connectAttr "pinky_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[128]";
connectAttr "pinky_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[129]";
connectAttr "pinky_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[130]";
connectAttr "pinky_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[131]";
connectAttr "pinky_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[132]";
connectAttr "pinky_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[133]";
connectAttr "pinky_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[134]";
connectAttr "pinky_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[135]";
connectAttr "pinky_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[136]";
connectAttr "pinky_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[137]";
connectAttr "pinky_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[138]";
connectAttr "pinky_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[139]";
connectAttr "thumb_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[140]";
connectAttr "thumb_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[141]";
connectAttr "thumb_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[142]";
connectAttr "thumb_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[143]";
connectAttr "thumb_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[144]";
connectAttr "thumb_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[145]";
connectAttr "thumb_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[146]";
connectAttr "thumb_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[147]";
connectAttr "thumb_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[148]";
connectAttr "thumb_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[149]";
connectAttr "thumb_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[150]";
connectAttr "thumb_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[151]";
connectAttr "thumb_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[152]";
connectAttr "thumb_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[153]";
connectAttr "thumb_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[154]";
connectAttr "thumb_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[155]";
connectAttr "thumb_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[156]";
connectAttr "thumb_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[157]";
connectAttr "thumb_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[158]";
connectAttr "thumb_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[159]";
connectAttr "thumb_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[160]";
connectAttr "thumb_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[161]";
connectAttr "thumb_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[162]";
connectAttr "thumb_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[163]";
connectAttr "thumb_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[164]";
connectAttr "thumb_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[165]";
connectAttr "thumb_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[166]";
connectAttr "ik_clavicle_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[167]";
connectAttr "ik_clavicle_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[168]";
connectAttr "ik_clavicle_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[169]";
connectAttr "arm_R_settings_mode.o" "Rig_UE4ManControllerRN.phl[170]";
connectAttr "arm_R_settings_clavMode.o" "Rig_UE4ManControllerRN.phl[171]";
connectAttr "arm_R_ik_elbow_anim_translateX.o" "Rig_UE4ManControllerRN.phl[172]"
		;
connectAttr "arm_R_ik_elbow_anim_translateY.o" "Rig_UE4ManControllerRN.phl[173]"
		;
connectAttr "arm_R_ik_elbow_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[174]"
		;
connectAttr "ik_hand_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[175]";
connectAttr "ik_hand_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[176]";
connectAttr "ik_hand_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[177]";
connectAttr "ik_hand_R_anim_stretch.o" "Rig_UE4ManControllerRN.phl[178]";
connectAttr "ik_hand_R_anim_slide.o" "Rig_UE4ManControllerRN.phl[179]";
connectAttr "ik_hand_R_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[180]";
connectAttr "ik_hand_R_anim_side.o" "Rig_UE4ManControllerRN.phl[181]";
connectAttr "ik_hand_R_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[182]";
connectAttr "ik_hand_R_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[183]";
connectAttr "ik_hand_R_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[184]";
connectAttr "ik_hand_R_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[185]";
connectAttr "ik_hand_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[186]";
connectAttr "ik_hand_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[187]";
connectAttr "ik_hand_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[188]";
connectAttr "index_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[189]";
connectAttr "index_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[190]";
connectAttr "index_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[191]";
connectAttr "index_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[192]";
connectAttr "index_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[193]";
connectAttr "index_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[194]";
connectAttr "index_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[195]";
connectAttr "index_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[196]";
connectAttr "index_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[197]";
connectAttr "index_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[198]";
connectAttr "index_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[199]";
connectAttr "index_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[200]";
connectAttr "index_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[201]";
connectAttr "index_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[202]";
connectAttr "index_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[203]";
connectAttr "index_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[204]";
connectAttr "index_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[205]";
connectAttr "index_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[206]";
connectAttr "index_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[207]";
connectAttr "index_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[208]";
connectAttr "index_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[209]";
connectAttr "index_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[210]";
connectAttr "index_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[211]";
connectAttr "index_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[212]";
connectAttr "index_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[213]";
connectAttr "index_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[214]";
connectAttr "index_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[215]";
connectAttr "index_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[216]";
connectAttr "middle_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[217]";
connectAttr "middle_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[218]";
connectAttr "middle_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[219]";
connectAttr "middle_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[220]";
connectAttr "middle_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[221]";
connectAttr "middle_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[222]";
connectAttr "middle_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[223]";
connectAttr "middle_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[224]";
connectAttr "middle_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[225]";
connectAttr "middle_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[226]";
connectAttr "middle_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[227]";
connectAttr "middle_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[228]";
connectAttr "middle_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[229]";
connectAttr "middle_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[230]";
connectAttr "middle_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[231]";
connectAttr "middle_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[232]";
connectAttr "middle_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[233]";
connectAttr "middle_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[234]";
connectAttr "middle_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[235]";
connectAttr "middle_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[236]";
connectAttr "middle_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[237]";
connectAttr "middle_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[238]";
connectAttr "middle_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[239]";
connectAttr "middle_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[240]";
connectAttr "middle_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[241]";
connectAttr "middle_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[242]";
connectAttr "middle_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[243]";
connectAttr "middle_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[244]";
connectAttr "ring_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[245]";
connectAttr "ring_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[246]";
connectAttr "ring_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[247]";
connectAttr "ring_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[248]";
connectAttr "ring_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[249]";
connectAttr "ring_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[250]";
connectAttr "ring_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[251]";
connectAttr "ring_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[252]";
connectAttr "ring_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[253]";
connectAttr "ring_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[254]";
connectAttr "ring_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[255]";
connectAttr "ring_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[256]";
connectAttr "ring_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[257]";
connectAttr "ring_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[258]";
connectAttr "ring_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[259]";
connectAttr "ring_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[260]";
connectAttr "ring_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[261]";
connectAttr "ring_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[262]";
connectAttr "ring_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[263]";
connectAttr "ring_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[264]";
connectAttr "ring_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[265]";
connectAttr "ring_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[266]";
connectAttr "ring_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[267]";
connectAttr "ring_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[268]";
connectAttr "ring_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[269]";
connectAttr "ring_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[270]";
connectAttr "ring_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[271]";
connectAttr "ring_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[272]";
connectAttr "pinky_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[273]";
connectAttr "pinky_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[274]";
connectAttr "pinky_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[275]";
connectAttr "pinky_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[276]";
connectAttr "pinky_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[277]";
connectAttr "pinky_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[278]";
connectAttr "pinky_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[279]";
connectAttr "pinky_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[280]";
connectAttr "pinky_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[281]";
connectAttr "pinky_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[282]";
connectAttr "pinky_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[283]";
connectAttr "pinky_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[284]";
connectAttr "pinky_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[285]";
connectAttr "pinky_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[286]";
connectAttr "pinky_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[287]";
connectAttr "pinky_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[288]";
connectAttr "pinky_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[289]";
connectAttr "pinky_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[290]";
connectAttr "pinky_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[291]";
connectAttr "pinky_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[292]";
connectAttr "pinky_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[293]";
connectAttr "pinky_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[294]";
connectAttr "pinky_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[295]";
connectAttr "pinky_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[296]";
connectAttr "pinky_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[297]";
connectAttr "pinky_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[298]";
connectAttr "pinky_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[299]";
connectAttr "pinky_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[300]";
connectAttr "thumb_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[301]";
connectAttr "thumb_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[302]";
connectAttr "thumb_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[303]";
connectAttr "thumb_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[304]";
connectAttr "thumb_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[305]";
connectAttr "thumb_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[306]";
connectAttr "thumb_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[307]";
connectAttr "thumb_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[308]";
connectAttr "thumb_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[309]";
connectAttr "thumb_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[310]";
connectAttr "thumb_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[311]";
connectAttr "thumb_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[312]";
connectAttr "thumb_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[313]";
connectAttr "thumb_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[314]";
connectAttr "thumb_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[315]";
connectAttr "thumb_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[316]";
connectAttr "thumb_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[317]";
connectAttr "thumb_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[318]";
connectAttr "thumb_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[319]";
connectAttr "thumb_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[320]";
connectAttr "thumb_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[321]";
connectAttr "thumb_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[322]";
connectAttr "thumb_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[323]";
connectAttr "thumb_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[324]";
connectAttr "thumb_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[325]";
connectAttr "thumb_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[326]";
connectAttr "thumb_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[327]";
connectAttr "ik_clavicle_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[328]";
connectAttr "ik_clavicle_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[329]";
connectAttr "ik_clavicle_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[330]";
connectAttr "fk_neck_01_anim_translateX.o" "Rig_UE4ManControllerRN.phl[331]";
connectAttr "fk_neck_01_anim_translateY.o" "Rig_UE4ManControllerRN.phl[332]";
connectAttr "fk_neck_01_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[333]";
connectAttr "fk_neck_01_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[334]";
connectAttr "fk_neck_01_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[335]";
connectAttr "fk_neck_01_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[336]";
connectAttr "fk_head_anim_translateX.o" "Rig_UE4ManControllerRN.phl[337]";
connectAttr "fk_head_anim_translateY.o" "Rig_UE4ManControllerRN.phl[338]";
connectAttr "fk_head_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[339]";
connectAttr "fk_head_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[340]";
connectAttr "fk_head_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[341]";
connectAttr "fk_head_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[342]";
connectAttr "fk_head_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[343]";
connectAttr "fk_head_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[344]";
connectAttr "fk_head_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[345]";
connectAttr "leg_L_settings_mode.o" "Rig_UE4ManControllerRN.phl[346]";
connectAttr "ik_foot_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[347]";
connectAttr "ik_foot_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[348]";
connectAttr "ik_foot_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[349]";
connectAttr "ik_foot_L_anim_stretch.o" "Rig_UE4ManControllerRN.phl[350]";
connectAttr "ik_foot_L_anim_stretchBias.o" "Rig_UE4ManControllerRN.phl[351]";
connectAttr "ik_foot_L_anim_squash.o" "Rig_UE4ManControllerRN.phl[352]";
connectAttr "ik_foot_L_anim_knee_twist.o" "Rig_UE4ManControllerRN.phl[353]";
connectAttr "ik_foot_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[354]";
connectAttr "ik_foot_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[355]";
connectAttr "ik_foot_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[356]";
connectAttr "ik_foot_L_anim_toeCtrlVis.o" "Rig_UE4ManControllerRN.phl[357]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[358]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[359]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[360]";
connectAttr "leg_L_heel_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[361]";
connectAttr "leg_L_heel_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[362]";
connectAttr "leg_L_heel_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[363]";
connectAttr "leg_L_heel_ctrl_ballPivot.o" "Rig_UE4ManControllerRN.phl[364]";
connectAttr "leg_L_heel_ctrl_heelPivot.o" "Rig_UE4ManControllerRN.phl[365]";
connectAttr "leg_L_toe_tip_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[366]";
connectAttr "leg_L_toe_tip_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[367]";
connectAttr "leg_L_toe_tip_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[368]";
connectAttr "leg_R_settings_mode.o" "Rig_UE4ManControllerRN.phl[369]";
connectAttr "ik_foot_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[370]";
connectAttr "ik_foot_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[371]";
connectAttr "ik_foot_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[372]";
connectAttr "ik_foot_R_anim_stretch.o" "Rig_UE4ManControllerRN.phl[373]";
connectAttr "ik_foot_R_anim_stretchBias.o" "Rig_UE4ManControllerRN.phl[374]";
connectAttr "ik_foot_R_anim_squash.o" "Rig_UE4ManControllerRN.phl[375]";
connectAttr "ik_foot_R_anim_knee_twist.o" "Rig_UE4ManControllerRN.phl[376]";
connectAttr "ik_foot_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[377]";
connectAttr "ik_foot_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[378]";
connectAttr "ik_foot_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[379]";
connectAttr "ik_foot_R_anim_toeCtrlVis.o" "Rig_UE4ManControllerRN.phl[380]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[381]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[382]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[383]";
connectAttr "leg_R_heel_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[384]";
connectAttr "leg_R_heel_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[385]";
connectAttr "leg_R_heel_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[386]";
connectAttr "leg_R_heel_ctrl_ballPivot.o" "Rig_UE4ManControllerRN.phl[387]";
connectAttr "leg_R_heel_ctrl_heelPivot.o" "Rig_UE4ManControllerRN.phl[388]";
connectAttr "leg_R_toe_tip_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[389]";
connectAttr "leg_R_toe_tip_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[390]";
connectAttr "leg_R_toe_tip_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[391]";
connectAttr "torso_settings_mode.o" "Rig_UE4ManControllerRN.phl[392]";
connectAttr "torso_body_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[393]";
connectAttr "torso_body_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[394]";
connectAttr "torso_body_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[395]";
connectAttr "torso_body_anim_translateX.o" "Rig_UE4ManControllerRN.phl[396]";
connectAttr "torso_body_anim_translateY.o" "Rig_UE4ManControllerRN.phl[397]";
connectAttr "torso_body_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[398]";
connectAttr "torso_hip_anim_translateX.o" "Rig_UE4ManControllerRN.phl[399]";
connectAttr "torso_hip_anim_translateY.o" "Rig_UE4ManControllerRN.phl[400]";
connectAttr "torso_hip_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[401]";
connectAttr "torso_hip_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[402]";
connectAttr "torso_hip_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[403]";
connectAttr "torso_hip_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[404]";
connectAttr "torso_mid_ik_anim_translateX.o" "Rig_UE4ManControllerRN.phl[405]";
connectAttr "torso_mid_ik_anim_translateY.o" "Rig_UE4ManControllerRN.phl[406]";
connectAttr "torso_mid_ik_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[407]";
connectAttr "torso_mid_ik_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[408]";
connectAttr "torso_mid_ik_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[409]";
connectAttr "torso_mid_ik_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[410]";
connectAttr "torso_chest_ik_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[411]";
connectAttr "torso_chest_ik_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[412]";
connectAttr "torso_chest_ik_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[413]";
connectAttr "torso_chest_ik_anim_translateX.o" "Rig_UE4ManControllerRN.phl[414]"
		;
connectAttr "torso_chest_ik_anim_translateY.o" "Rig_UE4ManControllerRN.phl[415]"
		;
connectAttr "torso_chest_ik_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[416]"
		;
connectAttr "torso_chest_ik_anim_stretch.o" "Rig_UE4ManControllerRN.phl[417]";
connectAttr "torso_chest_ik_anim_twist_amount.o" "Rig_UE4ManControllerRN.phl[418]"
		;
connectAttr "torso_chest_ik_anim_autoSpine.o" "Rig_UE4ManControllerRN.phl[419]";
connectAttr "torso_chest_ik_anim_rotationInfluence.o" "Rig_UE4ManControllerRN.phl[420]"
		;
connectAttr "torso_chest_ik_anim_squash.o" "Rig_UE4ManControllerRN.phl[421]";
connectAttr "hyperLayout1.msg" "animBot.hl";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "animBot.animBot_Anim_Recovery_Scene_ID"
		;
connectAttr "animBot_Select_Sets.msg" "animBot.animBot_Select_Sets";
connectAttr "__Purple__.msg" "animBot.__Purple__";
connectAttr "__Light_Yellow__.msg" "animBot.__Light_Yellow__";
connectAttr "__Light_Green__.msg" "animBot.__Light_Green__";
connectAttr "hyperLayout2.msg" "animBot_Select_Sets.hl";
connectAttr "hyperLayout3.msg" "__Purple__.hl";
connectAttr "hyperLayout4.msg" "__Light_Yellow__.hl";
connectAttr "hyperLayout5.msg" "__Light_Green__.hl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "hyperLayout1.hyp[0].dn";
connectAttr "animBot_Select_Sets.msg" "hyperLayout1.hyp[1].dn";
connectAttr "sharedReferenceNode.sr" "Rig_UE4ManControllerRN.sr";
connectAttr "groupId5.id" "groupParts5.gi";
connectAttr "__Purple__.msg" "hyperLayout2.hyp[0].dn";
connectAttr "__Light_Yellow__.msg" "hyperLayout2.hyp[1].dn";
connectAttr "__Light_Green__.msg" "hyperLayout2.hyp[2].dn";
connectAttr "All.msg" "hyperLayout3.hyp[0].dn";
connectAttr "pairBlend1_inTranslateX1.o" "pairBlend1.itx1";
connectAttr "pairBlend1_inTranslateY1.o" "pairBlend1.ity1";
connectAttr "pairBlend1_inTranslateZ1.o" "pairBlend1.itz1";
connectAttr "pairBlend1_inRotateX1.o" "pairBlend1.irx1";
connectAttr "pairBlend1_inRotateY1.o" "pairBlend1.iry1";
connectAttr "pairBlend1_inRotateZ1.o" "pairBlend1.irz1";
connectAttr "Spine.msg" "hyperLayout4.hyp[0].dn";
connectAttr "Neck.msg" "hyperLayout5.hyp[0].dn";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "groupId5.msg" ":defaultLastHiddenSet.gn" -na;
// End of 1P_Mannequin_Death_01.ma
