//Maya ASCII 2018ff09 scene
//Name: 1P_Mannequin_Crouch_01.ma
//Last modified: Sun, Oct 06, 2019 12:49:33 PM
//Codeset: 1252
file -rdi 1 -ns "Rig_UE4ManController" -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
file -rdi 2 -ns "SK_Mannequin" -rfn "Rig_UE4ManController:SK_MannequinRN" -op
		 "v=0;" -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/SkinnedMesh/SK_Mannequin.ma";
file -rdi 1 -ns "SK_Mannequin" -rfn "SK_MannequinRN" -typ "Fbx" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Mesh/SK_Mannequin.FBX";
file -r -ns "Rig_UE4ManController" -dr 1 -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
file -r -ns "SK_Mannequin" -dr 1 -rfn "SK_MannequinRN" -typ "Fbx" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Mesh/SK_Mannequin.FBX";
requires maya "2018ff09";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2018.0.0";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201811122215-49253d42f6";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "9B71BA30-4D91-37E0-F8A1-76860572EF0C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -469.08872890484201 147.5879611535706 167.99798587114446 ;
	setAttr ".r" -type "double3" -8.1383527295959421 -70.199999999974622 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "040CCD28-47B7-36F4-7075-158AB9BE94F6";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 693.01844086144888;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "22E1EC1C-4B99-370C-4ED5-64A46726A3C8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "3C451472-4A71-EA39-E829-3EA2ED993072";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "FAA32395-4DDE-EAF3-353A-EF83E3FBBA46";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "899DFA0A-411A-D494-52C5-E29F29354815";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "4BC5C9E7-4524-0750-E579-1283483E856A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E25881E2-466F-FDA8-671F-CC887393B509";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode dagContainer -n "animBot";
	rename -uid "A4FECEBD-4BFD-FC3B-C1D0-34829DE42924";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -s false -ci true -sn "animBot_Anim_Recovery_Scene_ID" -ln "animBot_Anim_Recovery_Scene_ID" 
		-at "message";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".iconSimpleName" -type "string" "animBot";
createNode dagContainer -n "animBot_Anim_Recovery_Scene_ID" -p "animBot";
	rename -uid "48859900-462F-A61E-3BCA-BC99D53BDBE6";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "sceneID" -ln "sceneID" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "anim_recovery";
	setAttr ".sceneID" -type "string" "1569311827.351000";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "E7942B3C-47AF-1EA4-03A9-519DFAB05321";
	setAttr -s 92 ".lnk";
	setAttr -s 92 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "0FFD85F3-476E-08AE-7F8D-CA97E2944297";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "72465AA8-4124-BEBE-5EB4-5185CC7C5E3C";
createNode displayLayerManager -n "layerManager";
	rename -uid "E1D50723-40A2-3796-984E-CC809DA1FA3F";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  1 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "24CE7960-4C6C-ECA7-197A-23BAA0C634A7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "15782544-41AE-BC87-796B-6AA811BB7DAD";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C4DFE456-470E-4AC8-6521-E889CF0E7EB6";
	setAttr ".g" yes;
createNode hyperLayout -n "hyperLayout1";
	rename -uid "7A75F400-4B93-2B36-D3EC-0D99FB2FE2B8";
	setAttr ".ihi" 0;
createNode reference -n "Rig_UE4ManControllerRN";
	rename -uid "CBC4EE03-4672-3F8F-F837-8DA731DA7206";
	setAttr ".fn[0]" -type "string" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
	setAttr -s 440 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".phl[440]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Rig_UE4ManControllerRN"
		"Rig_UE4ManControllerRN" 0
		"Rig_UE4ManController:SK_MannequinRN" 0
		"Rig_UE4ManControllerRN" 37
		1 |Rig_UE4ManController:FP_Cam "blendParent1" "blendParent1" " -ci 1 -k 1 -dv 1 -smn 0 -smx 1 -at \"double\""
		
		2 "|Rig_UE4ManController:FP_Cam" "translate" " -type \"double3\" 0 115.82918854614553084 0.44744682008782061"
		
		2 "|Rig_UE4ManController:FP_Cam" "translateX" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "translateY" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "translateZ" " -av"
		2 "|Rig_UE4ManController:FP_Cam" "blendParent1" " -k 1"
		2 "Rig_UE4ManController:Skel" "visibility" " 0"
		2 "Rig_UE4ManController:Skel" "displayOrder" " 3"
		2 "Rig_UE4ManController:MannequinMesh" "visibility" " 1"
		2 "Rig_UE4ManController:IKBones" "visibility" " 0"
		2 "Rig_UE4ManController:IKBones" "displayOrder" " 4"
		2 "Rig_UE4ManController:proxy_geo_layer" "displayOrder" " 5"
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateX" 
		"|Rig_UE4ManController:FP_Cam.translateX" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateY" 
		"|Rig_UE4ManController:FP_Cam.translateY" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateZ" 
		"|Rig_UE4ManController:FP_Cam.translateZ" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateX" 
		"|Rig_UE4ManController:FP_Cam.rotateX" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateY" 
		"|Rig_UE4ManController:FP_Cam.rotateY" ""
		3 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateZ" 
		"|Rig_UE4ManController:FP_Cam.rotateZ" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[422]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[423]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[424]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[425]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[426]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[427]" ""
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.blendParent1" 
		"Rig_UE4ManControllerRN.placeHolderList[428]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.blendParent1" 
		"Rig_UE4ManControllerRN.placeHolderList[429]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.visibility" 
		"Rig_UE4ManControllerRN.placeHolderList[430]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleX" "Rig_UE4ManControllerRN.placeHolderList[431]" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleY" "Rig_UE4ManControllerRN.placeHolderList[432]" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam.scaleZ" "Rig_UE4ManControllerRN.placeHolderList[433]" 
		""
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateX" 
		"Rig_UE4ManControllerRN.placeHolderList[434]" "Rig_UE4ManController:FP_Cam.tx"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateY" 
		"Rig_UE4ManControllerRN.placeHolderList[435]" "Rig_UE4ManController:FP_Cam.ty"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintTranslateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[436]" "Rig_UE4ManController:FP_Cam.tz"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[437]" "Rig_UE4ManController:FP_Cam.rx"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[438]" "Rig_UE4ManController:FP_Cam.ry"
		5 3 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_Cam_parentConstraint1.constraintRotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[439]" "Rig_UE4ManController:FP_Cam.rz"
		5 3 "Rig_UE4ManControllerRN" "Rig_UE4ManController:groupParts117.outputGeometry" 
		"Rig_UE4ManControllerRN.placeHolderList[440]" "Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.i"
		
		"Rig_UE4ManController:SK_MannequinRN" 432
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"instObjGroups.objectGroups" " -s 12"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvPivot" " -type \"double2\" 0.39557498693466187 0.898499995470047"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"DiffuseUV\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].colorName" " -type \"string\" \"colorSet0\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].clamped" " 0"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].representation" " 4"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"translate" " -type \"double3\" 0 76.76717683457529517 -1.65479764133226537"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"rotate" " -type \"double3\" -89.99999999999998579 0 89.99999999999968736"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "visibility" " 1"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "displayOrder" " 2"
		3 "Rig_UE4ManController:groupParts117.outputGeometry" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.instObjGroups.objectGroups[20].objectGroupId" 
		"Rig_UE4ManControllerRN.placeHolderList[1]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		"Rig_UE4ManControllerRN.placeHolderList[2]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[3]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[4]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[5]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[6]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[7]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[8]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[9]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings.clavMode" 
		"Rig_UE4ManControllerRN.placeHolderList[10]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[11]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[12]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[13]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[14]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[15]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[16]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[17]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[18]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[19]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[20]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[21]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[22]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[23]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[24]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[25]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[26]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[27]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[28]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[29]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[30]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[31]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[32]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[33]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[34]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[35]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[36]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[37]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[38]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[39]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[40]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[41]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[42]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[43]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[44]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[45]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[46]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[47]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[48]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[49]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[50]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[51]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[52]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[53]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[54]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[55]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[56]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[57]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[58]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[59]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[60]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[61]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[62]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[63]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[64]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[65]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[66]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[67]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[68]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[69]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[70]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[71]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[72]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[73]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[74]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[75]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[76]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[77]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[78]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[79]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[80]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[81]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[82]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[83]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[84]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[85]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[86]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[87]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[88]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[89]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[90]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[91]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[92]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[93]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[94]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[95]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[96]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[97]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[98]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[99]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[100]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[101]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[102]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[103]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[104]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[105]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[106]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[107]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[108]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[109]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[110]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[111]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[112]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[113]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[114]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[115]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[116]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[117]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[118]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[119]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[120]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[121]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[122]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[123]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[124]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[125]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[126]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[127]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[128]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[129]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[130]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[131]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[132]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[133]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[134]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[135]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[136]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[137]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[138]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[139]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[140]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[141]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[142]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[143]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[144]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[145]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[146]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[147]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[148]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[149]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[150]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[151]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[152]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[153]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[154]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[155]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[156]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[157]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[158]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[159]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[160]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[161]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[162]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[163]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[164]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[165]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[166]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[167]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[168]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[169]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[170]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_settings.clavMode" 
		"Rig_UE4ManControllerRN.placeHolderList[171]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[172]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[173]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[174]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[175]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[176]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[177]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[178]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[179]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[180]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[181]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[182]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[183]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[184]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[185]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[186]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[187]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[188]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[189]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[190]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[191]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[192]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[193]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[194]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[195]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[196]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[197]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[198]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[199]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[200]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[201]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[202]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[203]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[204]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[205]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[206]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[207]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[208]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[209]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[210]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[211]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[212]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[213]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[214]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[215]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[216]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[217]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[218]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[219]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[220]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[221]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[222]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[223]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[224]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[225]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[226]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[227]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[228]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[229]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[230]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[231]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[232]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[233]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[234]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[235]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[236]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[237]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[238]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[239]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[240]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[241]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[242]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[243]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[244]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[245]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[246]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[247]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[248]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[249]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[250]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[251]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[252]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[253]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[254]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[255]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[256]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[257]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[258]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[259]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[260]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[261]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[262]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[263]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[264]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[265]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[266]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[267]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[268]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[269]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[270]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[271]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[272]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[273]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[274]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[275]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[276]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[277]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[278]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[279]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[280]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[281]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[282]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[283]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[284]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[285]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[286]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[287]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[288]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[289]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[290]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[291]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[292]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[293]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[294]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[295]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[296]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[297]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[298]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[299]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[300]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[301]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[302]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[303]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[304]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[305]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[306]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[307]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[308]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[309]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[310]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[311]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[312]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[313]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[314]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[315]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[316]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[317]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[318]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[319]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[320]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[321]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[322]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[323]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[324]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[325]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[326]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[327]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[328]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[329]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[330]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[331]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[332]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[333]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[334]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[335]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[336]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[337]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[338]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[339]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[340]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[341]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[342]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[343]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[344]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[345]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:leg_L_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[346]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[347]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[348]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[349]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[350]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.stretchBias" 
		"Rig_UE4ManControllerRN.placeHolderList[351]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[352]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.knee_twist" 
		"Rig_UE4ManControllerRN.placeHolderList[353]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[354]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[355]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[356]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim.toeCtrlVis" 
		"Rig_UE4ManControllerRN.placeHolderList[357]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[358]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[359]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[360]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[361]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[362]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[363]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.ballPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[364]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl.heelPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[365]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[366]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[367]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[368]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:leg_R_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[369]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[370]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[371]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[372]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[373]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.stretchBias" 
		"Rig_UE4ManControllerRN.placeHolderList[374]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[375]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.knee_twist" 
		"Rig_UE4ManControllerRN.placeHolderList[376]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[377]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[378]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[379]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim.toeCtrlVis" 
		"Rig_UE4ManControllerRN.placeHolderList[380]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[381]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[382]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[383]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[384]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[385]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[386]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.ballPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[387]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl.heelPivot" 
		"Rig_UE4ManControllerRN.placeHolderList[388]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[389]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[390]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[391]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[392]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[393]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[394]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[395]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[396]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[397]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[398]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[399]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[400]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[401]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[402]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[403]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[404]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[405]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[406]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[407]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[408]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[409]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[410]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[411]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[412]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[413]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[414]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[415]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[416]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[417]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.twist_amount" 
		"Rig_UE4ManControllerRN.placeHolderList[418]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.autoSpine" 
		"Rig_UE4ManControllerRN.placeHolderList[419]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.rotationInfluence" 
		"Rig_UE4ManControllerRN.placeHolderList[420]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim.squash" 
		"Rig_UE4ManControllerRN.placeHolderList[421]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "F174DB98-42F4-C6EB-6B69-21B88D373DCE";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "A3756E06-4C22-85FA-A873-A883FBF7A71A";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "72578236-4D72-4F99-CF17-64885EFB820C";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "DE8000B5-4133-0645-B895-429447408B7E";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "1B9B6DC3-42D7-7AB3-E5DA-F5ACED2968F7";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1175\n            -height 651\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n"
		+ "            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n"
		+ "                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n"
		+ "\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n"
		+ "                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n"
		+ "                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n"
		+ "                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Model Panel5\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Model Panel5\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"Rig_UE4ManController:FP_CamShape\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1303\n            -height 651\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1175\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1175\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 1000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1AFC679D-489C-E075-DD59-13B48A3EFC8B";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 10 -ast 0 -aet 10 ";
	setAttr ".st" 6;
createNode groupId -n "groupId5";
	rename -uid "30D16B17-4481-D4C0-5F15-7AB2420F31B8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "DBDFA0E2-4441-71F6-C9FB-76B05E6D0ED6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[25958:27625]" "f[31580:31595]";
createNode reference -n "sharedReferenceNode";
	rename -uid "320DD024-4F9C-ABDA-F405-B3814DAD3CCE";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
createNode reference -n "SK_MannequinRN";
	rename -uid "2DE4F1F9-424C-BFCD-5EA3-31A09DD31B0E";
	setAttr ".ed" -type "dataReferenceEdits" 
		"SK_MannequinRN"
		"SK_MannequinRN" 0
		"SK_MannequinRN" 3
		2 "|SK_Mannequin:SK_Mannequin|SK_Mannequin:root" "translate" " -type \"double3\" 0 -106.66084583289865861 0"
		
		2 "|SK_Mannequin:SK_Mannequin|SK_Mannequin:root" "rotate" " -type \"double3\" 0 0 -180"
		
		2 "|SK_Mannequin:SK_Mannequin|SK_Mannequin:root|SK_Mannequin:ik_hand_root" 
		"translate" " -type \"double3\" 0 0 0";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode pairBlend -n "pairBlend1";
	rename -uid "A184E73F-4581-96C4-0C0F-8383A418E510";
createNode animCurveTL -n "pairBlend1_inTranslateX1";
	rename -uid "83F3B47A-4D91-5E5C-EE4C-7A871492906A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -2.7430723359241172e-12 10 -2.7430723359241172e-12;
createNode animCurveTL -n "pairBlend1_inTranslateY1";
	rename -uid "6C5481E7-4C06-B848-0296-E395FBFB991D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 115.82918854614553 10 115.82918854614553;
createNode animCurveTL -n "pairBlend1_inTranslateZ1";
	rename -uid "88B86B69-40CE-E42A-36A3-C18CB28092A4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0.44744682008782061 10 0.44744682008782061;
createNode animCurveTL -n "fk_neck_01_anim_translateX";
	rename -uid "3215429E-4536-0C4A-7FE0-12910952078D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "fk_neck_01_anim_translateY";
	rename -uid "106A6E4F-4B21-3726-C627-1E860B63A7D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "fk_neck_01_anim_translateZ";
	rename -uid "11431643-4D87-2729-6145-86ACC997EF7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "fk_head_anim_translateX";
	rename -uid "BBAEC008-4066-A2A0-3E9F-719E6BCB5DA1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "fk_head_anim_translateY";
	rename -uid "C8062B1E-4518-F64A-61C9-14B91E16CFC1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "fk_head_anim_translateZ";
	rename -uid "18FBCB34-436D-7F8B-E4CB-9AAB24685EED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateX";
	rename -uid "C73AB955-4099-D4B5-5F86-2D82E050F8FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateY";
	rename -uid "6AF2F3CC-4B78-A14B-F2AC-0A84AD41D5D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_L_ik_elbow_anim_translateZ";
	rename -uid "52FC684A-4178-614E-E44F-ECB3037BE06D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_hand_L_anim_translateX";
	rename -uid "1AE18B23-4A49-06E4-6311-158852525A7F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_hand_L_anim_translateY";
	rename -uid "69B61277-4ED2-DF41-427D-0DB07012B3F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -9.6316477899641075e-15 10 -9.6316477899641075e-15;
createNode animCurveTL -n "ik_hand_L_anim_translateZ";
	rename -uid "3DA31648-4863-CC0F-D9B4-669FB51FE6AE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -51.79484441267865 10 -51.79484441267865;
createNode animCurveTL -n "ik_clavicle_L_anim_translateX";
	rename -uid "D9A66A7F-4CA0-9311-D25E-03A802A2CB78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_clavicle_L_anim_translateY";
	rename -uid "2B9DDAB6-4A23-8DAF-134B-DC9B0DF7B1B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_clavicle_L_anim_translateZ";
	rename -uid "5BC71FE2-4A81-9DB0-3B93-889151FE7CD8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_L_anim_translateX";
	rename -uid "A877BAC7-48E6-4C22-5604-4392AC85FDB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_L_anim_translateY";
	rename -uid "EA53B6B9-45F1-7BAA-F42D-BBA0ECD1DFD7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_L_anim_translateZ";
	rename -uid "8D307F86-4777-02B9-B6EA-B2B7073CDFEF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_L_anim_translateX";
	rename -uid "1001CCDB-4D1C-2AF3-BF43-CA9D66CE5A2B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_L_anim_translateY";
	rename -uid "3BA3F445-46FE-4ED1-33D5-FC91C9883629";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_L_anim_translateZ";
	rename -uid "E86B6320-4C65-36D1-922E-3B858253E0BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_L_anim_translateX";
	rename -uid "A458595D-43BB-3E22-3A18-E1B304B1EA62";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_L_anim_translateY";
	rename -uid "C3413789-4052-3CDA-2B6F-60B93D7AAA45";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_L_anim_translateZ";
	rename -uid "CB314C34-4172-5EEB-BF03-A996034E3C8E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_L_anim_translateX";
	rename -uid "F73DA87B-4F0B-57C8-15D9-4C97511E5E19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_L_anim_translateY";
	rename -uid "CE4C01F5-445F-744D-6F00-34AA76CA735F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_L_anim_translateZ";
	rename -uid "799E614F-4A88-DF9E-680B-9C891D7F9126";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_L_anim_translateX";
	rename -uid "53452AD6-4E14-EF0B-1C64-328FC5EFF329";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_L_anim_translateY";
	rename -uid "42F31D7D-4B4D-95BF-004E-C3B24072530E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_L_anim_translateZ";
	rename -uid "6867E044-4994-CF8B-7439-33B2CD41488D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_L_anim_translateX";
	rename -uid "8070D715-47AF-911A-6C58-268476E11BB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_L_anim_translateY";
	rename -uid "442F5837-4B04-270C-7D24-65979321B4CC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_L_anim_translateZ";
	rename -uid "8F9D230F-45F3-1698-8D69-35B10BE83C37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_L_anim_translateX";
	rename -uid "BD816285-495F-31C1-30E8-B89FBD76589F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_L_anim_translateY";
	rename -uid "A40BF384-4A9F-075B-7C6C-9FBACC849434";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_L_anim_translateZ";
	rename -uid "E3503457-4C49-18DA-41E3-BC8BA1DA24B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_L_anim_translateX";
	rename -uid "E45DF31C-4B28-0FB7-BDDB-93BB301A991E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_L_anim_translateY";
	rename -uid "86B8D235-4099-6E2A-954B-DD944E482510";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_L_anim_translateZ";
	rename -uid "5E03F2EB-4C9B-F46D-560F-DA87CA780BE9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_L_anim_translateX";
	rename -uid "A3D61EF6-4D24-584D-6551-DB81574DF7E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_L_anim_translateY";
	rename -uid "13500F22-4D5D-1F23-F4D9-C4AEBA8A3281";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_L_anim_translateZ";
	rename -uid "F9F656DE-42E4-79A3-F6DD-178F5D3C4248";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_L_anim_translateX";
	rename -uid "122413F3-411B-039D-C31A-65A443366E52";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_L_anim_translateY";
	rename -uid "74BA1769-4D5C-4ACF-860C-3C95A5BC97C9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_L_anim_translateZ";
	rename -uid "8C2E3E79-4CEE-4291-28D1-70A2E16F469F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_L_anim_translateX";
	rename -uid "8423A7D4-4D21-7D21-C790-718B68BB65DB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_L_anim_translateY";
	rename -uid "3112594D-47BF-8639-8781-659A06E64418";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_L_anim_translateZ";
	rename -uid "90257584-415C-8294-C49C-2CA164E8BC54";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_L_anim_translateX";
	rename -uid "FED6723A-4F16-79D5-CB6E-429918452065";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_L_anim_translateY";
	rename -uid "D4C23B9E-42B3-8946-F25C-01B7674DBE16";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_L_anim_translateZ";
	rename -uid "6F9B9FD3-45E6-626A-55AE-D7AC7F3F4CCB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_L_anim_translateX";
	rename -uid "D961A0AC-4C0C-63B5-BADC-B299280FE364";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_L_anim_translateY";
	rename -uid "E8B40DC0-4B13-C44C-01BB-A7BAA188FA8E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_L_anim_translateZ";
	rename -uid "BCDF6E63-4355-C946-B5B4-38B3816A439F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_L_anim_translateX";
	rename -uid "4130D9DA-4EE3-E685-F2ED-29B7348E7D33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_L_anim_translateY";
	rename -uid "5A87AE53-4C8D-13BC-FAF3-BD942A5D0A77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_L_anim_translateZ";
	rename -uid "FC600EEF-4AC6-0E47-C767-64A8169B180A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_L_anim_translateX";
	rename -uid "F7C23517-4FC0-ED42-8AE3-CCAAAF4BA0D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_L_anim_translateY";
	rename -uid "840F8EE2-4A72-FB63-C622-F9812D6A9361";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_L_anim_translateZ";
	rename -uid "3C70476F-4824-8A5C-7165-FF95257D9FBD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateX";
	rename -uid "FBFE7B66-4CD8-43D8-D51E-4D81FEE8CD68";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateY";
	rename -uid "20E82F47-42F3-AD86-39FE-D98E3D1AB216";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "arm_R_ik_elbow_anim_translateZ";
	rename -uid "0AF228DE-434E-DD14-5A9D-28B087C55F4B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_hand_R_anim_translateX";
	rename -uid "1DCF2009-4F61-B1C4-4EBC-1FAEF7A1BD8C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_hand_R_anim_translateY";
	rename -uid "BE5EA5E1-479C-36B0-C3A8-24B1C46F57EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -9.6316477899641075e-15 10 -9.6316477899641075e-15;
createNode animCurveTL -n "ik_hand_R_anim_translateZ";
	rename -uid "0098FAEA-41DF-F07D-7A7E-27B109E02F31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -51.79484441267865 10 -51.79484441267865;
createNode animCurveTL -n "ik_clavicle_R_anim_translateX";
	rename -uid "2148609A-4476-67E4-8766-1AB39268E651";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_clavicle_R_anim_translateY";
	rename -uid "AD7E11B9-4B40-64AB-9550-BC96F6F5DFBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_clavicle_R_anim_translateZ";
	rename -uid "59A21AA0-4AE3-6E35-42CE-9DAF50CB9DDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_R_anim_translateX";
	rename -uid "EE855910-4E0A-864C-3BCF-D3808665D299";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_R_anim_translateY";
	rename -uid "EBEDE24F-47E6-C40E-9DB3-EC96237DC668";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_01_R_anim_translateZ";
	rename -uid "EDF3A2CF-490F-BD3F-F1AD-F5ACA4480501";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_R_anim_translateX";
	rename -uid "4B6B682D-40A1-BDF6-96DC-FDB63B793A1D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_R_anim_translateY";
	rename -uid "F5FDA99F-418F-5D3E-455F-17B0B235A225";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_02_R_anim_translateZ";
	rename -uid "D3ADECDF-4B79-D9A7-FB06-71B5DD1F1CC1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_R_anim_translateX";
	rename -uid "66D8FBC3-4427-4500-BC1C-7F81AC6FFA57";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_R_anim_translateY";
	rename -uid "9D2CB887-42AA-6888-CF35-7E8396A6A0A8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "index_03_R_anim_translateZ";
	rename -uid "E05508C7-4849-3847-103D-DAB3179E1DB7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_R_anim_translateX";
	rename -uid "5B53705B-47FC-775D-33E7-BB86BB27A100";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_R_anim_translateY";
	rename -uid "B8535DA7-482C-A0A3-21D6-0FA60409C49E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_01_R_anim_translateZ";
	rename -uid "2713789A-44A6-4736-C95E-F4B4F0C2B942";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_R_anim_translateX";
	rename -uid "81AC2FB4-4A57-95B9-A0E3-3E979B0AF603";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_R_anim_translateY";
	rename -uid "3509B602-4F47-84DB-8820-6992A5F14DAD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_02_R_anim_translateZ";
	rename -uid "04394448-477E-B148-65DC-2B8C4C25D693";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_R_anim_translateX";
	rename -uid "7DC1D176-4362-4FF2-5206-93AE6113636C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_R_anim_translateY";
	rename -uid "ED02A92C-42D2-FEAF-82BE-9796F9AD95CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "middle_03_R_anim_translateZ";
	rename -uid "0AF59E88-4C5A-A799-0261-598E555A891D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_R_anim_translateX";
	rename -uid "9399A824-45D3-15F8-4C7A-ACB995C24720";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_R_anim_translateY";
	rename -uid "606DA512-4A4E-4710-AFE2-14831F58CD23";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_01_R_anim_translateZ";
	rename -uid "F94CDC6C-4CC1-E6F5-45F1-2193B018C0EA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_R_anim_translateX";
	rename -uid "2E398F8E-41FD-6961-03F1-33BD03576E47";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_R_anim_translateY";
	rename -uid "6D26FCF9-468A-FEA7-DD1F-839CEF77FF27";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_02_R_anim_translateZ";
	rename -uid "62BAEFCB-4C96-3654-C902-D786C44AA1BD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_R_anim_translateX";
	rename -uid "1CE2D118-4052-246D-28C7-25851FC0AAD8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_R_anim_translateY";
	rename -uid "8B530A23-44D5-A3B2-E588-DA957B285D77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "pinky_03_R_anim_translateZ";
	rename -uid "CA55D516-4439-2C73-D4A4-9D9B42F32DBF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_R_anim_translateX";
	rename -uid "56D02BF3-4341-CAB7-1713-28BFEA9401EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_R_anim_translateY";
	rename -uid "E0767DED-49DE-1EC1-5DC9-72A2C1D5F7E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_01_R_anim_translateZ";
	rename -uid "F4025681-4BBF-0682-C8D4-A9BA951799C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_R_anim_translateX";
	rename -uid "CDA93538-4C0F-7615-734A-66BB2B83B856";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_R_anim_translateY";
	rename -uid "C759990C-44BF-4997-5437-5FA248815C69";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_02_R_anim_translateZ";
	rename -uid "4611AF4B-46C3-571E-5A88-BEA227C82F5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_R_anim_translateX";
	rename -uid "2B8BB40E-4096-8EB0-FCBE-7EB1DEC21D4D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_R_anim_translateY";
	rename -uid "714DAD0B-4D3E-7DDF-2199-07BD96BEA952";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ring_03_R_anim_translateZ";
	rename -uid "FD8CE40F-4ADD-52C0-D526-36B70151B9C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_R_anim_translateX";
	rename -uid "ABF05736-4592-E417-5D26-9EABC433F5B0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_R_anim_translateY";
	rename -uid "F3591E41-4E6A-FE3E-6846-26A538556449";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_01_R_anim_translateZ";
	rename -uid "5C4542F0-4A78-ACA7-7973-5491C0715233";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_R_anim_translateX";
	rename -uid "EACA41E3-4760-0AA9-0B90-E1861DE2B0C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_R_anim_translateY";
	rename -uid "CC756EF1-407D-A177-464C-12A61D604A05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_02_R_anim_translateZ";
	rename -uid "8764B366-4FC7-45EF-3263-3F89801DA448";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_R_anim_translateX";
	rename -uid "7AA81BAE-42DE-5A91-7769-A99E00551B60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_R_anim_translateY";
	rename -uid "0E067B89-421E-C945-D9C7-F1B203759089";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "thumb_03_R_anim_translateZ";
	rename -uid "4ABAB2FE-40E0-746A-835E-19A031487CCF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "ik_foot_L_anim_translateX";
	rename -uid "77AED5A1-420C-13F8-C241-6EBA8F8002DE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 6.5419305946850565e-14 10 6.5419305946850565e-14;
createNode animCurveTL -n "ik_foot_L_anim_translateY";
	rename -uid "5BB3689B-45A4-6D04-5D3E-19BCF8B94231";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -19.641490792338175 10 -19.641490792338175;
createNode animCurveTL -n "ik_foot_L_anim_translateZ";
	rename -uid "A67DDA1F-4702-D332-4B6A-71B9E33EF46C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -6.3914417351127987e-28 10 -6.3914417351127987e-28;
createNode animCurveTL -n "ik_foot_R_anim_translateX";
	rename -uid "4C5DE7B1-4463-528A-51F3-AD8EAD1BDBFA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -9.2118524516571027e-14 10 -9.2118524516571027e-14;
createNode animCurveTL -n "ik_foot_R_anim_translateY";
	rename -uid "9E759100-4325-A29E-E97F-A09548E44A8E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 29.257663512450112 10 29.257663512450112;
createNode animCurveTL -n "ik_foot_R_anim_translateZ";
	rename -uid "2D62D8F8-4D09-96EC-48B4-16BA3CDB604A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 5.327438362417138e-13 10 5.327438362417138e-13;
createNode animCurveTL -n "root_anim_translateX";
	rename -uid "FB8ED8EF-476D-8BD7-7A08-089366A47500";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "root_anim_translateY";
	rename -uid "8FD6E913-4428-3DC6-5DB0-EC91AE73B5B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "root_anim_translateZ";
	rename -uid "D005D811-4187-177D-7FC6-22904B8A23A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_body_anim_translateX";
	rename -uid "DA0C8942-4A51-49D9-1EED-76AEF7383BC2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -51.795130273431255 10 -51.795130273431255;
createNode animCurveTL -n "torso_body_anim_translateY";
	rename -uid "24130572-477C-ED87-C05B-69BAAE1CE2E7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1.5543122344744912e-15 10 1.5543122344744912e-15;
createNode animCurveTL -n "torso_body_anim_translateZ";
	rename -uid "FC9B08ED-465B-8268-6C9E-1E8F5C13E6B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 2.1851575553348684e-13 10 2.1851575553348684e-13;
createNode animCurveTL -n "torso_chest_ik_anim_translateX";
	rename -uid "73F0384C-405E-FAFB-4B67-70B05C654CDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -1.004563046299741e-28 10 -1.004563046299741e-28;
createNode animCurveTL -n "torso_chest_ik_anim_translateY";
	rename -uid "70EA6518-4A7A-A911-9CD0-A2BB7647C04F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_chest_ik_anim_translateZ";
	rename -uid "01A37097-41B4-7B79-B909-83B63757C05C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -2.3811314531503594e-14 10 -2.3811314531503594e-14;
createNode animCurveTL -n "torso_hip_anim_translateX";
	rename -uid "B1CAC4ED-4A3B-FAA5-46BA-13AB8C6DFE22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_hip_anim_translateY";
	rename -uid "242D3550-4DF2-583B-472C-D09917A68E25";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_hip_anim_translateZ";
	rename -uid "EE8645E8-4A13-9D35-914C-C8A117BA38CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_mid_ik_anim_translateX";
	rename -uid "368DD64E-42F6-9E5C-D184-34BBDB2C901B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_mid_ik_anim_translateY";
	rename -uid "D0982D40-4CB7-8642-3A0D-8B9ECF696FAB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTL -n "torso_mid_ik_anim_translateZ";
	rename -uid "438CF3BF-4D51-1570-9DF5-FD99EC6C69CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "FP_Cam_visibility";
	rename -uid "688F520C-45F7-F3BC-A507-2FA28619269C";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "pairBlend1_inRotateX1";
	rename -uid "1FEA9EF1-469B-693D-41E5-D3B1CED71E79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pairBlend1_inRotateY1";
	rename -uid "CE1E0590-41ED-B6C8-05EA-E6B3D46042FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 179.9999999999998 10 179.9999999999998;
createNode animCurveTA -n "pairBlend1_inRotateZ1";
	rename -uid "D18950F7-4EC6-3238-62E9-1DB07641C58B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -7.6333312355124402e-14 10 -7.6333312355124402e-14;
createNode animCurveTU -n "FP_Cam_scaleX";
	rename -uid "8247D18D-4D78-60F9-21E8-719E62F64B1F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 24.37772922952978 10 24.37772922952978;
createNode animCurveTU -n "FP_Cam_scaleY";
	rename -uid "0F29F51B-4E02-E71E-047D-3DBF7794A33F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 24.37772922952978 10 24.37772922952978;
createNode animCurveTU -n "FP_Cam_scaleZ";
	rename -uid "0D0178BE-453B-8A71-ED80-B8977C31F17D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 24.37772922952978 10 24.37772922952978;
createNode animCurveTU -n "FP_Cam_blendParent1";
	rename -uid "5C9BC2DA-4611-C35A-A4DB-EE9F0FF77376";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateX";
	rename -uid "D0B521D3-439D-AE8B-AA67-F19AF591C520";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateY";
	rename -uid "B7FEE613-4AEA-B16A-9161-6B9012948F56";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_chest_ik_anim_rotateZ";
	rename -uid "80282AEE-4EEA-BE10-5FBB-8B8DCE3EB3BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "torso_chest_ik_anim_stretch";
	rename -uid "FDA07283-4858-7B3C-2120-A18812322C51";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "torso_chest_ik_anim_squash";
	rename -uid "CCBF2169-456D-5CBD-50DE-DE9C9816DF97";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "torso_chest_ik_anim_twist_amount";
	rename -uid "8850FDE5-4DA0-B391-42D0-36A9DFBDEB19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "torso_chest_ik_anim_autoSpine";
	rename -uid "A5DC7CD4-4BE7-C2C2-3E70-EB8DA9DD206E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "torso_chest_ik_anim_rotationInfluence";
	rename -uid "4AC7EDE7-4D90-8FD9-5F22-B5B953803EA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0.25 10 0.25;
createNode animCurveTU -n "torso_settings_mode";
	rename -uid "296F6B41-449A-8FDB-B5C5-65B9A279D863";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "torso_mid_ik_anim_rotateX";
	rename -uid "57A3BC8E-4636-1697-69A7-64878251FACB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_mid_ik_anim_rotateY";
	rename -uid "D384AA16-4669-30A5-4210-45A3CFEAE50D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_mid_ik_anim_rotateZ";
	rename -uid "84E273D6-40E3-8B41-D6DB-D69FF3F4AAEC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_hip_anim_rotateX";
	rename -uid "7C3608A2-45A7-8659-FC53-21ADB31E1419";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_hip_anim_rotateY";
	rename -uid "794C9321-457C-C39F-70C6-2A9F7221B512";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_hip_anim_rotateZ";
	rename -uid "FDAAC373-467E-087C-5606-858F286600FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_body_anim_rotateX";
	rename -uid "61B281CB-4A4D-1DA0-C6FC-A68C918CD92A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_body_anim_rotateY";
	rename -uid "8DC99467-493E-D143-C824-78B5479ABB67";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "torso_body_anim_rotateZ";
	rename -uid "8910DB3D-4F8D-6AA7-F0DC-7D9EBDBF6183";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateX";
	rename -uid "C3CB40F4-4F4E-26D8-7583-C4929B5387BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateY";
	rename -uid "C0645477-4E48-786D-FD05-19BEF72C5B0B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_toe_wiggle_ctrl_rotateZ";
	rename -uid "6F73B8B8-43EE-AAC2-1CED-1DA9B50E9D3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_R_settings_mode";
	rename -uid "78345636-4EE0-1CC1-38E7-0D9CE5E40640";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateX";
	rename -uid "0B960E79-4732-53D6-E75B-CE8308B07384";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateY";
	rename -uid "EE2554AC-4BF5-AAFD-AA4A-85AB6D20259F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_toe_tip_ctrl_rotateZ";
	rename -uid "67C3272F-410D-3A89-73FB-3EA0D48D5D0E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateX";
	rename -uid "7B0E3621-4167-AD09-F19F-DC8F875E540F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateY";
	rename -uid "F6F051F7-456D-7211-7A2F-2A8DFF09F7BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_R_heel_ctrl_rotateZ";
	rename -uid "4DAA8A87-4B56-B5FE-30C7-D5B3E8EA07E7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_R_heel_ctrl_heelPivot";
	rename -uid "34CDB905-42DE-4F4A-F708-B293FCC81F3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_R_heel_ctrl_ballPivot";
	rename -uid "42E68797-4B7B-8B01-68FB-9783C42A1175";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_R_anim_rotateX";
	rename -uid "D3F87DAB-4908-0281-D81C-0FAF3C59FF02";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_R_anim_rotateY";
	rename -uid "AACDAA3E-4963-10AA-1960-41BC01489D60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_R_anim_rotateZ";
	rename -uid "84395C53-4A4E-E42C-EF71-E88820B7E1A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_R_anim_knee_twist";
	rename -uid "8A56EDAE-4E21-D28C-EC57-FB9194B8253A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_R_anim_stretch";
	rename -uid "18467003-4858-750A-9865-2C97CAC8BF52";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_R_anim_squash";
	rename -uid "130CB1ED-4815-0B81-3868-4B89FB7F7FEB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_R_anim_toeCtrlVis";
	rename -uid "4AFA8685-4BA0-8E62-BA73-DFAA8E3192C6";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "ik_foot_R_anim_stretchBias";
	rename -uid "7DF72C54-417E-53CF-D836-C1A706F07EB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateX";
	rename -uid "278BE1DF-4971-1961-1733-82977D1CE95B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateY";
	rename -uid "AC74793F-482F-C328-06CC-059ACE122354";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_tip_ctrl_rotateZ";
	rename -uid "C5804EA1-4C2C-7CAB-AED9-6790AD517C31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_L_settings_mode";
	rename -uid "69E12183-41C7-6FE0-5AEE-D296D54CC354";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateX";
	rename -uid "40B48401-4A8B-C90C-DE96-A4828FC9658D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateY";
	rename -uid "0FE1177A-45E8-F07B-1227-53AA53525735";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_heel_ctrl_rotateZ";
	rename -uid "5F18975C-4BEE-6E64-0DA1-1384947F5639";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_L_heel_ctrl_heelPivot";
	rename -uid "238B220E-4365-323E-0B30-69B11DBE0997";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "leg_L_heel_ctrl_ballPivot";
	rename -uid "FDA5BB55-4C45-A27C-07B1-BA906777E1F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateX";
	rename -uid "5CCB148B-44E3-B959-15F3-788507E42176";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateY";
	rename -uid "FF725A4E-4A5C-B03E-661B-E398A8307C4C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "leg_L_toe_wiggle_ctrl_rotateZ";
	rename -uid "1AC7E611-466B-DB27-1E70-7C8715B81801";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateX";
	rename -uid "778DAE75-4406-FDBD-39D2-80B352D01A29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateY";
	rename -uid "93D58D11-49F3-830B-7272-85825DF771BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_foot_L_anim_rotateZ";
	rename -uid "700F123E-45E7-D177-0935-719D042B2CC1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_L_anim_knee_twist";
	rename -uid "D688CBE7-4E45-7F50-E075-808752FCD99E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_L_anim_stretch";
	rename -uid "1781E4D9-4994-2629-2DBC-78B87C30A80F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_L_anim_squash";
	rename -uid "7ECACC94-449C-4736-E779-77994B8ED3AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_foot_L_anim_toeCtrlVis";
	rename -uid "3BF900DD-42D7-66E4-B667-0DB76DC15D09";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "ik_foot_L_anim_stretchBias";
	rename -uid "26FEDEAF-49D4-8756-AF37-79A7F34097C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "fk_head_anim_rotateX";
	rename -uid "8E8DFFA6-4653-7740-4EC1-0FB7EA7480DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "fk_head_anim_rotateY";
	rename -uid "F3ACA123-432C-192B-0F4A-8CAAFEBB85F5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "fk_head_anim_rotateZ";
	rename -uid "86577F85-4B15-15D8-FBF4-4FAF596C805D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "fk_head_anim_scaleX";
	rename -uid "5F3E023B-4FBA-B1C9-E7D8-318E6BB36C18";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "fk_head_anim_scaleY";
	rename -uid "11727AD8-4FCF-7EEC-B638-1B87C8D8F501";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "fk_head_anim_scaleZ";
	rename -uid "B3D50F33-499E-76BF-8A01-91A896253283";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "fk_neck_01_anim_rotateX";
	rename -uid "17598721-4CA1-AA65-C0D7-9C8B2E867DA3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "fk_neck_01_anim_rotateY";
	rename -uid "CA4B4FBA-4597-2A5A-E438-B49E04D4972E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "fk_neck_01_anim_rotateZ";
	rename -uid "9DA9B9C9-4AA4-A013-005E-6084B61D49E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "arm_R_settings_clavMode";
	rename -uid "D176835E-4D7E-FF30-70E4-60A6E0D8A2CB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "thumb_03_R_anim_rotateX";
	rename -uid "D348E4BE-4E4B-DEAE-19CB-AD9708933BEA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateY";
	rename -uid "9DCF3959-408D-1059-7648-DAB4832C1239";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateZ";
	rename -uid "30B4DDBE-4512-481D-BDF2-848B5DA8E459";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "thumb_03_R_anim_scaleX";
	rename -uid "3569A6CE-4C91-54CC-84D9-50B9DA683311";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleY";
	rename -uid "5D3AB583-4580-A472-1873-D8B228F1F707";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleZ";
	rename -uid "D437FB1A-43D6-0252-2ED4-9EBEA6B9B0BE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "thumb_02_R_anim_rotateX";
	rename -uid "724749F4-415D-A471-CD09-A7821035A3F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateY";
	rename -uid "2B3FD55A-41B6-C51A-AC15-A7B27063547B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateZ";
	rename -uid "EE2C80EB-4C4F-FD37-ED2B-F182C9BD29DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "thumb_02_R_anim_scaleX";
	rename -uid "A3DA1660-4C72-6092-B1CF-29A03B106B7D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleY";
	rename -uid "94E4B59E-4555-CE46-A3CE-5582DD770F3B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleZ";
	rename -uid "F6922435-4D9A-67FB-2D00-B1BE2A0867C2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "thumb_01_R_anim_rotateX";
	rename -uid "52D13EA1-407D-DD66-F28F-8CB2449BE2AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_01_R_anim_rotateY";
	rename -uid "BB7C935A-4D10-C2EA-C6DB-78B073931CCA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_01_R_anim_rotateZ";
	rename -uid "69469101-4E1E-62AA-96CD-2092D7EEF36A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "thumb_01_R_anim_scaleX";
	rename -uid "FD6EF306-45FA-8665-DBF5-F6B3DD75D60C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleY";
	rename -uid "E8088F5B-4BE6-17E0-B045-1F9CAF36A7BA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleZ";
	rename -uid "857DA789-4B62-0A61-65C1-96B0DFCDBC47";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_03_R_anim_rotateX";
	rename -uid "F9C9D138-4BAE-7522-742C-12BE70008AE2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateY";
	rename -uid "614B5EA1-4CB3-111C-FF11-E9BE4A91F200";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateZ";
	rename -uid "612A6326-4207-EC76-C6BF-F392F64D8FDB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_03_R_anim_scaleX";
	rename -uid "CB433D10-4C40-1879-52F2-1FAD5485F0C0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleY";
	rename -uid "3AF2B730-492B-4F8C-D1F9-1AA8687647E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleZ";
	rename -uid "2C9A5F81-491C-50AD-C39C-4DAFD4F27561";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_02_R_anim_rotateX";
	rename -uid "D7A265F6-4AB6-B1F9-DEF7-C88D3F299438";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateY";
	rename -uid "A236B513-49B1-1230-041F-7088FED7E778";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateZ";
	rename -uid "7B096940-4425-9E5E-EC0F-06BA65642D40";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_02_R_anim_scaleX";
	rename -uid "CD87496B-4B43-8966-134C-8FAEFE4D3ADF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleY";
	rename -uid "5D5BB203-4BEA-4AA2-90AA-6C9E0F042470";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleZ";
	rename -uid "542277F2-4535-0DD8-7D0B-8DB87A19B566";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_01_R_anim_rotateX";
	rename -uid "20EE0887-4916-760A-A9EC-7CAC51523DFA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_01_R_anim_rotateY";
	rename -uid "D1E986EC-42EB-6DA1-0B54-CA8EE93F93A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_01_R_anim_rotateZ";
	rename -uid "024BA063-498A-F39D-9615-B991994C7A91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_01_R_anim_scaleX";
	rename -uid "3B733B4D-400F-7989-F13C-30A7518ECAB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleY";
	rename -uid "27FB2FF8-4FB9-4099-965F-5197A222A566";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleZ";
	rename -uid "7AA71DB8-4E4F-14E3-47C9-21A6E6C4F0E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_R_anim_sticky";
	rename -uid "BD1C70AA-47C6-EACA-DBC1-C493B43339A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_R_anim_rotateX";
	rename -uid "96730895-4B3A-6AFA-9AF6-9DAA4126F96F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_R_anim_rotateY";
	rename -uid "1ADF3521-4563-7B75-1A53-65AA5A072406";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_R_anim_rotateZ";
	rename -uid "C82D6039-424E-1C2F-D8C3-DCB6AA938A3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_03_R_anim_scaleX";
	rename -uid "AB2E19EE-4A62-DEE3-0A33-AD8A1F60CC6A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_03_R_anim_scaleY";
	rename -uid "E9D12CD5-4605-73CA-A464-B3A902FB37C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_03_R_anim_scaleZ";
	rename -uid "B777EA15-4292-C3B8-E2F6-A8B8C5832F2C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "ring_02_R_anim_rotateX";
	rename -uid "7715F4A8-410F-4108-7A7C-B0B2AC576044";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_02_R_anim_rotateY";
	rename -uid "7BC40490-47C8-47BD-320D-B589F51063F0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_02_R_anim_rotateZ";
	rename -uid "EE8E5D0F-410A-0FF4-3700-7E8250203D0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_02_R_anim_scaleX";
	rename -uid "41E7D5C8-49E1-B466-2260-AB989D153346";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_02_R_anim_scaleY";
	rename -uid "73C7A60D-4BDE-CD14-EF2F-D6999067B86E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_02_R_anim_scaleZ";
	rename -uid "D8032745-4A37-0C65-72EA-3FB69CCED760";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "ring_01_R_anim_rotateX";
	rename -uid "66B48DD5-4DBA-792C-9211-D6B34DA0FAA8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_01_R_anim_rotateY";
	rename -uid "ACA632CC-43E6-2DC1-B951-4B83173A907A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_01_R_anim_rotateZ";
	rename -uid "85D73E02-4B88-24FC-ACFB-54B5CF5191AB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_01_R_anim_scaleX";
	rename -uid "6DDEDD6F-43AF-76CA-BD63-A4BBF7F78083";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_R_anim_scaleY";
	rename -uid "D09E81DF-46A5-1C65-6037-9B9EEFC52DE2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_R_anim_scaleZ";
	rename -uid "77D3ADF7-470D-29E2-145F-80A86C46B224";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_R_anim_sticky";
	rename -uid "E769BA8E-4B0C-B868-F2DE-5A8EA9224AA5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_R_anim_rotateX";
	rename -uid "4DE6E362-4EAD-B642-3DF2-41B24740782F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_R_anim_rotateY";
	rename -uid "A8FFB40D-47EF-A85C-A4DA-019914346288";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_R_anim_rotateZ";
	rename -uid "4A41DCAF-452E-915A-5E2A-2184A950AE48";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_03_R_anim_scaleX";
	rename -uid "412A35AF-4C2D-5ECF-E327-059AD084103B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_03_R_anim_scaleY";
	rename -uid "186AD480-4E23-CD0F-FD80-AC99220108C9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_03_R_anim_scaleZ";
	rename -uid "2FBA21EB-419D-61F8-6E9E-23A4D40C0111";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "middle_02_R_anim_rotateX";
	rename -uid "BAA90173-4F85-049C-5065-64BE4A939DAB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_02_R_anim_rotateY";
	rename -uid "2716DA0F-4349-0799-8A8F-018166929185";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_02_R_anim_rotateZ";
	rename -uid "8AC14122-4776-6EF4-E838-77A77DFFB06D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_02_R_anim_scaleX";
	rename -uid "30F3E322-46E5-F2B0-BE0F-34A94C2B7441";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_02_R_anim_scaleY";
	rename -uid "7D5D45AC-44B5-1CD2-1484-5EA59F3FAD90";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_02_R_anim_scaleZ";
	rename -uid "F6C0D20C-474B-837D-2D74-63A8B47EA811";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "middle_01_R_anim_rotateX";
	rename -uid "65ABEE0C-4669-D967-0FA7-41845C977E4A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_01_R_anim_rotateY";
	rename -uid "8D596BA5-4626-209F-06B8-598C007EE631";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_01_R_anim_rotateZ";
	rename -uid "9C22DBDB-4AC8-62DC-72DD-14BBEA88444F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_01_R_anim_scaleX";
	rename -uid "6DE9DA85-4583-D9D6-7FF6-FF9020382457";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_R_anim_scaleY";
	rename -uid "F1DE613A-4524-D329-BA06-0F9E89756822";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_R_anim_scaleZ";
	rename -uid "38C687A8-4200-9EF0-C90B-DA82CE47CE7C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_R_anim_sticky";
	rename -uid "CB614417-4BA4-6701-188A-2CA6F1A981C6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_03_R_anim_rotateX";
	rename -uid "2E576706-4ECF-9555-2624-C0AB76212948";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_03_R_anim_rotateY";
	rename -uid "539CACA8-4177-311D-10E7-7EA98DCA2997";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_03_R_anim_rotateZ";
	rename -uid "D7955CCF-48E8-49CC-8756-9AA174709399";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_03_R_anim_scaleX";
	rename -uid "6506E02B-4026-384E-91C4-EC8116B7ADC7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_03_R_anim_scaleY";
	rename -uid "5B5BF0CD-43E2-2E70-C9C1-71ADEDF4D08B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_03_R_anim_scaleZ";
	rename -uid "D8BCF082-453D-95CF-1838-798A98DC2213";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "index_02_R_anim_rotateX";
	rename -uid "5C0A3008-4CF2-C477-7022-D7940493F4C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_02_R_anim_rotateY";
	rename -uid "AF73E628-4896-2A1E-798B-43AF894EFB28";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_02_R_anim_rotateZ";
	rename -uid "AA29AAEB-43AB-B94E-084E-2D8C2331B3C4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_02_R_anim_scaleX";
	rename -uid "186926C4-4B00-63D4-8446-E183B8412622";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_02_R_anim_scaleY";
	rename -uid "87E0A36A-484A-61F2-B19E-6DBDD4D0C504";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_02_R_anim_scaleZ";
	rename -uid "912E0422-4248-CD3C-B98A-81B29D6E74D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "index_01_R_anim_rotateX";
	rename -uid "0C0975D1-4074-F3D1-6ED5-11B62B3B45F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_01_R_anim_rotateY";
	rename -uid "2A237A83-45C4-4815-A67B-4EB0CACC9A76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_01_R_anim_rotateZ";
	rename -uid "3023CACF-416C-7EC9-6BFC-9A8C579A513A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_01_R_anim_scaleX";
	rename -uid "1615ED3A-4221-9E33-7B0E-DFA0EC24E0EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_R_anim_scaleY";
	rename -uid "1862B6C8-4BA4-F8C8-55E1-97825383BB40";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_R_anim_scaleZ";
	rename -uid "83DDC688-4726-9DC3-3D8D-62B57AEDC178";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_R_anim_sticky";
	rename -uid "22446E07-4870-E90A-5456-B88018D96A39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_R_anim_rotateX";
	rename -uid "F34D45D4-416F-F869-2A83-0F8D3E1657E9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_R_anim_rotateY";
	rename -uid "9D51033D-4DC6-5D24-E72F-2DA2D9582E14";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_R_anim_rotateZ";
	rename -uid "FED9A873-4BF3-60AF-4846-7EB0D6E1D4C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_stretch";
	rename -uid "D96A823F-4006-CA26-A0F0-65AD26B970FC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_elbowLock";
	rename -uid "900A563B-4D8E-242E-DEF6-AAA48D232EF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_slide";
	rename -uid "57438A95-4C5C-11A6-8E6C-1AA2498515B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "arm_R_settings_mode";
	rename -uid "2390E34A-4E2C-7C3A-E962-3E9AEA2A6F43";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ik_hand_R_anim_side";
	rename -uid "7B2FC071-4956-8A1D-1655-D8B243154894";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_mid_bend";
	rename -uid "3F9F7221-4BA0-1749-C3F6-BFA794CBB418";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_mid_swivel";
	rename -uid "A16635A9-4458-2B5F-ABA9-2C974AA28F93";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_tip_pivot";
	rename -uid "449D67AE-42E6-2929-54E4-7E88101DD52D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_R_anim_tip_swivel";
	rename -uid "237131AC-424F-EE40-76B7-E8A74658511C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "arm_L_settings_clavMode";
	rename -uid "7D40EE26-45E8-8DC3-A950-679B126EF3F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "thumb_03_L_anim_rotateX";
	rename -uid "D576AD49-434B-36FC-BBA4-89B98AEFBE30";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_03_L_anim_rotateY";
	rename -uid "5051CF08-4179-6157-13B2-3BAC471DC6B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_03_L_anim_rotateZ";
	rename -uid "9F47E99A-4155-B5F4-BF13-55BEE6144A3F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "thumb_03_L_anim_scaleX";
	rename -uid "403ABFAE-4B8D-5699-80C6-1885FCA1227B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_03_L_anim_scaleY";
	rename -uid "BFD12AE4-4382-0161-68B6-38A198688515";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_03_L_anim_scaleZ";
	rename -uid "29CFF238-4412-2625-1E4E-62B2F0F3DA76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "thumb_02_L_anim_rotateX";
	rename -uid "9148F55F-4E30-AB93-00A1-4B9A00C2140B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_02_L_anim_rotateY";
	rename -uid "25C8F66B-4E9D-D13E-C1D9-E8A90D8C4555";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_02_L_anim_rotateZ";
	rename -uid "0AFE3469-44B6-0B42-C9B8-22AA4F3BE560";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 -11.625641870682387 10 -11.625641870682387;
createNode animCurveTU -n "thumb_02_L_anim_scaleX";
	rename -uid "ADD16F49-4853-6ABD-923F-1D8B3A209872";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_02_L_anim_scaleY";
	rename -uid "7B9FA0EE-4619-70D7-B387-A4B31AFFC68B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_02_L_anim_scaleZ";
	rename -uid "D97C1536-42A7-E5D5-F46D-B1BFAD99B3C8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "ring_02_L_anim_rotateX";
	rename -uid "F291616C-46D8-517A-7219-639929D7FE7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_02_L_anim_rotateY";
	rename -uid "C655C3C3-4429-6624-2CE5-37A25E010DD2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_02_L_anim_rotateZ";
	rename -uid "371B3A81-4FF5-39BC-CFBE-2A90A0BF67EA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_02_L_anim_scaleX";
	rename -uid "7627C779-4102-2D10-2F44-ED8444FE7F10";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_02_L_anim_scaleY";
	rename -uid "05CC2991-489D-FA01-450C-588542E84281";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_02_L_anim_scaleZ";
	rename -uid "FE855CA6-4474-C2B3-290C-2DA931664E94";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "ring_01_L_anim_rotateX";
	rename -uid "5F465FD0-46A9-7F7A-6054-52B12A9D095F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_01_L_anim_rotateY";
	rename -uid "DDE49E9A-4D3F-995B-5817-058471673E4A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_01_L_anim_rotateZ";
	rename -uid "862FD008-4765-14CD-009A-439DD5D01D2D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_01_L_anim_scaleX";
	rename -uid "CE58EF47-4E01-684F-53D2-5AADA6854BFA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_L_anim_scaleY";
	rename -uid "8C6F5BC1-47D6-51A5-3DB2-5696A759F697";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_L_anim_scaleZ";
	rename -uid "D83C5663-4DAC-2F98-239F-59A5919C101B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_01_L_anim_sticky";
	rename -uid "386653CD-491B-2C8D-5A2F-4BBABB0F437D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_L_anim_rotateX";
	rename -uid "64359693-42C2-89AB-56B2-39970D36BDB8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_L_anim_rotateY";
	rename -uid "2DE8B542-449E-FB3F-B7EE-B2923836C791";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_03_L_anim_rotateZ";
	rename -uid "842DB6E0-4A17-537E-BA0B-A8AF1703A5CD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_03_L_anim_scaleX";
	rename -uid "10CF29CD-46D5-27F0-890C-4CB0E966BDE1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_03_L_anim_scaleY";
	rename -uid "D709BCAD-41F8-9424-6C95-0F9613F1C744";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_03_L_anim_scaleZ";
	rename -uid "075A7BAB-4E4D-1B7E-B886-DB867BE80000";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "middle_02_L_anim_rotateX";
	rename -uid "DA6D2954-43FA-6591-F56B-F7AE86C61712";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_02_L_anim_rotateY";
	rename -uid "385477CD-4C90-1A4A-B469-FC9A067EABDD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_02_L_anim_rotateZ";
	rename -uid "AF32979C-4980-DD33-57AA-9FB1CC1167A2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_02_L_anim_scaleX";
	rename -uid "5A3CFBB9-43A7-7328-5D6B-1098E76A9ED7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_02_L_anim_scaleY";
	rename -uid "B24CF934-4ECD-ED3F-D6B7-A3AA07580406";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_02_L_anim_scaleZ";
	rename -uid "167146AE-4297-0F30-7072-4D814947A299";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "middle_01_L_anim_rotateX";
	rename -uid "2AEACA69-48B1-0CEC-FB8D-2E9A30E9115B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_01_L_anim_rotateY";
	rename -uid "EFAFF0D4-407C-BB96-A02C-1F83BABBCE1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "middle_01_L_anim_rotateZ";
	rename -uid "D1578DC2-42FA-3B39-B5AB-A3AE203E3811";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "middle_01_L_anim_scaleX";
	rename -uid "5589710F-4237-0338-5605-E9979A50A13A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_L_anim_scaleY";
	rename -uid "953F483B-4503-C1FA-F057-899231EDB815";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_L_anim_scaleZ";
	rename -uid "C4A807F0-44FA-83B6-D25D-AB9073768DE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "middle_01_L_anim_sticky";
	rename -uid "FEF00DAA-4848-7842-F03F-43984D292A70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateX";
	rename -uid "049530E7-42E5-BF5A-8CF5-CB99590B6B1C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateY";
	rename -uid "37E96806-48F4-ABA7-8E69-FEA18D66D120";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "thumb_01_L_anim_rotateZ";
	rename -uid "D1D95E29-495A-0FF0-1EB3-BF8B04AC9410";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "thumb_01_L_anim_scaleX";
	rename -uid "4341B53B-40DB-B2ED-CA68-5A96B151792A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_01_L_anim_scaleY";
	rename -uid "76EEF0D4-47D0-0BFE-0652-598FC3560409";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "thumb_01_L_anim_scaleZ";
	rename -uid "C1CABFF9-4363-61D2-A23A-6482C39A74C0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_03_L_anim_rotateX";
	rename -uid "57B140F1-42AB-B8EC-EE7E-0BB62EE9D559";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_03_L_anim_rotateY";
	rename -uid "D8E45104-40C4-5621-81B0-1296020958D6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_03_L_anim_rotateZ";
	rename -uid "10491029-4B31-91A5-188B-71B2CE038562";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_03_L_anim_scaleX";
	rename -uid "F0140690-4A71-71CF-1C0D-938C9A6F200E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_03_L_anim_scaleY";
	rename -uid "514F5CD5-46F8-FA4B-256B-C5A8E169F43D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_03_L_anim_scaleZ";
	rename -uid "183A6B95-4C4F-52F9-5B5A-33A9B6FFC7EE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_02_L_anim_rotateX";
	rename -uid "5D6954F0-40A9-FFBD-D426-79AEFA5FC615";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_02_L_anim_rotateY";
	rename -uid "FA0AF1A3-499D-2B28-7B68-32BB1A8CCBAB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_02_L_anim_rotateZ";
	rename -uid "F3C52BD8-478B-F41B-5F87-97B75EEC4E1A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_02_L_anim_scaleX";
	rename -uid "D0272999-45C2-5655-63A3-91A1C2F4326C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_02_L_anim_scaleY";
	rename -uid "70B3EF7F-4442-4208-5642-038DEF07EEF5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_02_L_anim_scaleZ";
	rename -uid "4AC4B822-4813-BEF7-D54F-27994A343597";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "pinky_01_L_anim_rotateX";
	rename -uid "1C9A337B-41C6-70A6-9342-759C34FE1250";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_01_L_anim_rotateY";
	rename -uid "D73FAD84-4407-722E-6A77-9CAE9A497D35";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "pinky_01_L_anim_rotateZ";
	rename -uid "8A02E2FB-4825-374D-0AAB-C9BE0FE7DB07";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "pinky_01_L_anim_scaleX";
	rename -uid "8CAE09B7-4E4C-FD76-38CC-308CB9EA9A23";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_L_anim_scaleY";
	rename -uid "47B3A678-475F-68DA-64DA-E18176924788";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_L_anim_scaleZ";
	rename -uid "78E5A5AE-4159-4DA9-46AD-358D325404A0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "pinky_01_L_anim_sticky";
	rename -uid "5FACE365-4364-A78D-7D13-F58E0B420584";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_L_anim_rotateX";
	rename -uid "8DDB54C5-492D-67C5-F7D6-31AC6FDBB812";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_L_anim_rotateY";
	rename -uid "6DC4F14F-4734-0220-DA15-6DA5D3F9640D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ring_03_L_anim_rotateZ";
	rename -uid "84733699-4ECA-70B0-C33A-1EBD5DA86E91";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ring_03_L_anim_scaleX";
	rename -uid "8FE9DC05-4D28-042D-DAD2-BCB4C3A83B9E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_03_L_anim_scaleY";
	rename -uid "20607D67-4349-8CBB-8C87-A7ADA421DBB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ring_03_L_anim_scaleZ";
	rename -uid "0EC94875-4ACF-BB9A-C21D-20A5EAE5B88A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "index_03_L_anim_rotateX";
	rename -uid "937A4F3C-47DF-7B34-FA8B-DFAB47CB55FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_03_L_anim_rotateY";
	rename -uid "5BDFD33C-4AAC-D888-2C4B-40871D10A30E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_03_L_anim_rotateZ";
	rename -uid "4C954F63-4A07-357D-94B2-94B12072CDD5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_03_L_anim_scaleX";
	rename -uid "BE9ED134-45DF-5DD5-A736-EA8D64E04939";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_03_L_anim_scaleY";
	rename -uid "B86093C5-4D3A-55BF-15F0-CBBC55813C60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_03_L_anim_scaleZ";
	rename -uid "8E0905EC-453E-A406-F3B2-C5A2C1A70A28";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "index_02_L_anim_rotateX";
	rename -uid "9B845C66-46EC-67F5-F2F2-48A247817C29";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_02_L_anim_rotateY";
	rename -uid "A0113E65-44DE-A66C-D6DC-BA825569DD6C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_02_L_anim_rotateZ";
	rename -uid "752DD436-422C-ED46-8029-8080E6933CDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_02_L_anim_scaleX";
	rename -uid "B130ACE5-4371-D7DC-BCB1-FBB36BE8E46D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_02_L_anim_scaleY";
	rename -uid "CDE90A8B-4DAD-0A6B-CCF2-189018C538FA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_02_L_anim_scaleZ";
	rename -uid "AD8BACC4-40F9-F595-88BF-75985CC7C527";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTA -n "index_01_L_anim_rotateX";
	rename -uid "53F49C37-429D-8D6C-BAF3-D1B7891E3172";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_01_L_anim_rotateY";
	rename -uid "5F642620-4F3A-E8D2-04DD-4F983854666B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "index_01_L_anim_rotateZ";
	rename -uid "67D0ED22-4B9C-65FF-BD11-E8A64AD88113";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "index_01_L_anim_scaleX";
	rename -uid "8D45905A-4748-D4A0-D0E2-CCB002B232C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_L_anim_scaleY";
	rename -uid "E894C2BC-4561-D253-2E60-9C8F9B3DD2B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_L_anim_scaleZ";
	rename -uid "A18CBFC6-4D02-1737-E338-BC940FCB9B40";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "index_01_L_anim_sticky";
	rename -uid "13BF9255-498A-3AD7-A2B5-F4A29350179F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_L_anim_rotateX";
	rename -uid "3C2E2DB6-4646-0968-6D13-98987A75C655";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_L_anim_rotateY";
	rename -uid "4046A374-4C43-C78B-D96B-959B61648138";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "ik_hand_L_anim_rotateZ";
	rename -uid "0F31D0A4-4A8B-A519-9D37-5C8E127BFE9D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_stretch";
	rename -uid "65438D69-47AE-7A16-CEB5-83BD7C0BE3CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_elbowLock";
	rename -uid "1C5483D1-4A7D-A8D0-96C0-359F456666B4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_slide";
	rename -uid "C184470E-4797-DE9A-312B-F69A19D8442C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "arm_L_settings_mode";
	rename -uid "B4D99845-4301-812E-EF11-B59806C8E170";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 1 10 1;
createNode animCurveTU -n "ik_hand_L_anim_side";
	rename -uid "66CED282-4B0B-ED69-FDCF-23B7E5AFB76B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_bend";
	rename -uid "B992A527-461F-AACF-69A2-A095CAF60BDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_swivel";
	rename -uid "73E3936D-4578-12BA-69E3-14A281787C00";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_pivot";
	rename -uid "11AD9F86-4620-10D8-7E83-939DDCBF497B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_swivel";
	rename -uid "840527CA-4728-828C-6682-C79D3F1ACF79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "root_anim_rotateX";
	rename -uid "7FF4AB1B-422F-5EEC-7489-A487AF68E5FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "root_anim_rotateY";
	rename -uid "947D2D43-45FC-31FD-4253-799C2DD5A1D0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
createNode animCurveTA -n "root_anim_rotateZ";
	rename -uid "CA3D432D-4DBA-F257-B749-4886CBA4B979";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  0 0 10 0;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 92 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 15 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 203 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
	setAttr -s 60 ".tx";
select -ne :initialShadingGroup;
	setAttr -s 141 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 123 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
select -ne :ikSystem;
	setAttr -s 6 ".sol";
connectAttr "pairBlend1.otx" "Rig_UE4ManControllerRN.phl[422]";
connectAttr "pairBlend1.oty" "Rig_UE4ManControllerRN.phl[423]";
connectAttr "pairBlend1.otz" "Rig_UE4ManControllerRN.phl[424]";
connectAttr "pairBlend1.orx" "Rig_UE4ManControllerRN.phl[425]";
connectAttr "pairBlend1.ory" "Rig_UE4ManControllerRN.phl[426]";
connectAttr "pairBlend1.orz" "Rig_UE4ManControllerRN.phl[427]";
connectAttr "Rig_UE4ManControllerRN.phl[428]" "pairBlend1.w";
connectAttr "FP_Cam_blendParent1.o" "Rig_UE4ManControllerRN.phl[429]";
connectAttr "FP_Cam_visibility.o" "Rig_UE4ManControllerRN.phl[430]";
connectAttr "FP_Cam_scaleX.o" "Rig_UE4ManControllerRN.phl[431]";
connectAttr "FP_Cam_scaleY.o" "Rig_UE4ManControllerRN.phl[432]";
connectAttr "FP_Cam_scaleZ.o" "Rig_UE4ManControllerRN.phl[433]";
connectAttr "Rig_UE4ManControllerRN.phl[434]" "pairBlend1.itx2";
connectAttr "Rig_UE4ManControllerRN.phl[435]" "pairBlend1.ity2";
connectAttr "Rig_UE4ManControllerRN.phl[436]" "pairBlend1.itz2";
connectAttr "Rig_UE4ManControllerRN.phl[437]" "pairBlend1.irx2";
connectAttr "Rig_UE4ManControllerRN.phl[438]" "pairBlend1.iry2";
connectAttr "Rig_UE4ManControllerRN.phl[439]" "pairBlend1.irz2";
connectAttr "Rig_UE4ManControllerRN.phl[440]" "groupParts5.ig";
connectAttr "groupId5.id" "Rig_UE4ManControllerRN.phl[1]";
connectAttr "groupParts5.og" "Rig_UE4ManControllerRN.phl[2]";
connectAttr "root_anim_translateX.o" "Rig_UE4ManControllerRN.phl[3]";
connectAttr "root_anim_translateY.o" "Rig_UE4ManControllerRN.phl[4]";
connectAttr "root_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[5]";
connectAttr "root_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[6]";
connectAttr "root_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[7]";
connectAttr "root_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[8]";
connectAttr "arm_L_settings_mode.o" "Rig_UE4ManControllerRN.phl[9]";
connectAttr "arm_L_settings_clavMode.o" "Rig_UE4ManControllerRN.phl[10]";
connectAttr "arm_L_ik_elbow_anim_translateX.o" "Rig_UE4ManControllerRN.phl[11]";
connectAttr "arm_L_ik_elbow_anim_translateY.o" "Rig_UE4ManControllerRN.phl[12]";
connectAttr "arm_L_ik_elbow_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[13]";
connectAttr "ik_hand_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[14]";
connectAttr "ik_hand_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[15]";
connectAttr "ik_hand_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[16]";
connectAttr "ik_hand_L_anim_stretch.o" "Rig_UE4ManControllerRN.phl[17]";
connectAttr "ik_hand_L_anim_slide.o" "Rig_UE4ManControllerRN.phl[18]";
connectAttr "ik_hand_L_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[19]";
connectAttr "ik_hand_L_anim_side.o" "Rig_UE4ManControllerRN.phl[20]";
connectAttr "ik_hand_L_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[21]";
connectAttr "ik_hand_L_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[22]";
connectAttr "ik_hand_L_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[23]";
connectAttr "ik_hand_L_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[24]";
connectAttr "ik_hand_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[25]";
connectAttr "ik_hand_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[26]";
connectAttr "ik_hand_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[27]";
connectAttr "index_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[28]";
connectAttr "index_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[29]";
connectAttr "index_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[30]";
connectAttr "index_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[31]";
connectAttr "index_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[32]";
connectAttr "index_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[33]";
connectAttr "index_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[34]";
connectAttr "index_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[35]";
connectAttr "index_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[36]";
connectAttr "index_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[37]";
connectAttr "index_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[38]";
connectAttr "index_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[39]";
connectAttr "index_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[40]";
connectAttr "index_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[41]";
connectAttr "index_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[42]";
connectAttr "index_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[43]";
connectAttr "index_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[44]";
connectAttr "index_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[45]";
connectAttr "index_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[46]";
connectAttr "index_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[47]";
connectAttr "index_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[48]";
connectAttr "index_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[49]";
connectAttr "index_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[50]";
connectAttr "index_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[51]";
connectAttr "index_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[52]";
connectAttr "index_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[53]";
connectAttr "index_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[54]";
connectAttr "index_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[55]";
connectAttr "middle_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[56]";
connectAttr "middle_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[57]";
connectAttr "middle_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[58]";
connectAttr "middle_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[59]";
connectAttr "middle_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[60]";
connectAttr "middle_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[61]";
connectAttr "middle_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[62]";
connectAttr "middle_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[63]";
connectAttr "middle_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[64]";
connectAttr "middle_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[65]";
connectAttr "middle_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[66]";
connectAttr "middle_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[67]";
connectAttr "middle_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[68]";
connectAttr "middle_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[69]";
connectAttr "middle_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[70]";
connectAttr "middle_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[71]";
connectAttr "middle_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[72]";
connectAttr "middle_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[73]";
connectAttr "middle_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[74]";
connectAttr "middle_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[75]";
connectAttr "middle_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[76]";
connectAttr "middle_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[77]";
connectAttr "middle_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[78]";
connectAttr "middle_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[79]";
connectAttr "middle_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[80]";
connectAttr "middle_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[81]";
connectAttr "middle_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[82]";
connectAttr "middle_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[83]";
connectAttr "ring_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[84]";
connectAttr "ring_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[85]";
connectAttr "ring_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[86]";
connectAttr "ring_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[87]";
connectAttr "ring_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[88]";
connectAttr "ring_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[89]";
connectAttr "ring_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[90]";
connectAttr "ring_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[91]";
connectAttr "ring_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[92]";
connectAttr "ring_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[93]";
connectAttr "ring_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[94]";
connectAttr "ring_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[95]";
connectAttr "ring_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[96]";
connectAttr "ring_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[97]";
connectAttr "ring_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[98]";
connectAttr "ring_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[99]";
connectAttr "ring_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[100]";
connectAttr "ring_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[101]";
connectAttr "ring_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[102]";
connectAttr "ring_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[103]";
connectAttr "ring_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[104]";
connectAttr "ring_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[105]";
connectAttr "ring_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[106]";
connectAttr "ring_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[107]";
connectAttr "ring_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[108]";
connectAttr "ring_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[109]";
connectAttr "ring_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[110]";
connectAttr "ring_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[111]";
connectAttr "pinky_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[112]";
connectAttr "pinky_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[113]";
connectAttr "pinky_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[114]";
connectAttr "pinky_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[115]";
connectAttr "pinky_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[116]";
connectAttr "pinky_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[117]";
connectAttr "pinky_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[118]";
connectAttr "pinky_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[119]";
connectAttr "pinky_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[120]";
connectAttr "pinky_01_L_anim_sticky.o" "Rig_UE4ManControllerRN.phl[121]";
connectAttr "pinky_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[122]";
connectAttr "pinky_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[123]";
connectAttr "pinky_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[124]";
connectAttr "pinky_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[125]";
connectAttr "pinky_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[126]";
connectAttr "pinky_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[127]";
connectAttr "pinky_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[128]";
connectAttr "pinky_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[129]";
connectAttr "pinky_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[130]";
connectAttr "pinky_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[131]";
connectAttr "pinky_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[132]";
connectAttr "pinky_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[133]";
connectAttr "pinky_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[134]";
connectAttr "pinky_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[135]";
connectAttr "pinky_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[136]";
connectAttr "pinky_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[137]";
connectAttr "pinky_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[138]";
connectAttr "pinky_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[139]";
connectAttr "thumb_01_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[140]";
connectAttr "thumb_01_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[141]";
connectAttr "thumb_01_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[142]";
connectAttr "thumb_01_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[143]";
connectAttr "thumb_01_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[144]";
connectAttr "thumb_01_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[145]";
connectAttr "thumb_01_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[146]";
connectAttr "thumb_01_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[147]";
connectAttr "thumb_01_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[148]";
connectAttr "thumb_02_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[149]";
connectAttr "thumb_02_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[150]";
connectAttr "thumb_02_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[151]";
connectAttr "thumb_02_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[152]";
connectAttr "thumb_02_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[153]";
connectAttr "thumb_02_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[154]";
connectAttr "thumb_02_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[155]";
connectAttr "thumb_02_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[156]";
connectAttr "thumb_02_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[157]";
connectAttr "thumb_03_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[158]";
connectAttr "thumb_03_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[159]";
connectAttr "thumb_03_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[160]";
connectAttr "thumb_03_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[161]";
connectAttr "thumb_03_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[162]";
connectAttr "thumb_03_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[163]";
connectAttr "thumb_03_L_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[164]";
connectAttr "thumb_03_L_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[165]";
connectAttr "thumb_03_L_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[166]";
connectAttr "ik_clavicle_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[167]";
connectAttr "ik_clavicle_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[168]";
connectAttr "ik_clavicle_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[169]";
connectAttr "arm_R_settings_mode.o" "Rig_UE4ManControllerRN.phl[170]";
connectAttr "arm_R_settings_clavMode.o" "Rig_UE4ManControllerRN.phl[171]";
connectAttr "arm_R_ik_elbow_anim_translateX.o" "Rig_UE4ManControllerRN.phl[172]"
		;
connectAttr "arm_R_ik_elbow_anim_translateY.o" "Rig_UE4ManControllerRN.phl[173]"
		;
connectAttr "arm_R_ik_elbow_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[174]"
		;
connectAttr "ik_hand_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[175]";
connectAttr "ik_hand_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[176]";
connectAttr "ik_hand_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[177]";
connectAttr "ik_hand_R_anim_stretch.o" "Rig_UE4ManControllerRN.phl[178]";
connectAttr "ik_hand_R_anim_slide.o" "Rig_UE4ManControllerRN.phl[179]";
connectAttr "ik_hand_R_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[180]";
connectAttr "ik_hand_R_anim_side.o" "Rig_UE4ManControllerRN.phl[181]";
connectAttr "ik_hand_R_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[182]";
connectAttr "ik_hand_R_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[183]";
connectAttr "ik_hand_R_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[184]";
connectAttr "ik_hand_R_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[185]";
connectAttr "ik_hand_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[186]";
connectAttr "ik_hand_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[187]";
connectAttr "ik_hand_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[188]";
connectAttr "index_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[189]";
connectAttr "index_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[190]";
connectAttr "index_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[191]";
connectAttr "index_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[192]";
connectAttr "index_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[193]";
connectAttr "index_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[194]";
connectAttr "index_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[195]";
connectAttr "index_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[196]";
connectAttr "index_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[197]";
connectAttr "index_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[198]";
connectAttr "index_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[199]";
connectAttr "index_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[200]";
connectAttr "index_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[201]";
connectAttr "index_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[202]";
connectAttr "index_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[203]";
connectAttr "index_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[204]";
connectAttr "index_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[205]";
connectAttr "index_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[206]";
connectAttr "index_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[207]";
connectAttr "index_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[208]";
connectAttr "index_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[209]";
connectAttr "index_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[210]";
connectAttr "index_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[211]";
connectAttr "index_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[212]";
connectAttr "index_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[213]";
connectAttr "index_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[214]";
connectAttr "index_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[215]";
connectAttr "index_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[216]";
connectAttr "middle_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[217]";
connectAttr "middle_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[218]";
connectAttr "middle_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[219]";
connectAttr "middle_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[220]";
connectAttr "middle_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[221]";
connectAttr "middle_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[222]";
connectAttr "middle_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[223]";
connectAttr "middle_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[224]";
connectAttr "middle_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[225]";
connectAttr "middle_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[226]";
connectAttr "middle_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[227]";
connectAttr "middle_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[228]";
connectAttr "middle_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[229]";
connectAttr "middle_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[230]";
connectAttr "middle_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[231]";
connectAttr "middle_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[232]";
connectAttr "middle_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[233]";
connectAttr "middle_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[234]";
connectAttr "middle_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[235]";
connectAttr "middle_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[236]";
connectAttr "middle_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[237]";
connectAttr "middle_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[238]";
connectAttr "middle_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[239]";
connectAttr "middle_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[240]";
connectAttr "middle_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[241]";
connectAttr "middle_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[242]";
connectAttr "middle_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[243]";
connectAttr "middle_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[244]";
connectAttr "ring_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[245]";
connectAttr "ring_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[246]";
connectAttr "ring_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[247]";
connectAttr "ring_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[248]";
connectAttr "ring_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[249]";
connectAttr "ring_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[250]";
connectAttr "ring_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[251]";
connectAttr "ring_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[252]";
connectAttr "ring_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[253]";
connectAttr "ring_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[254]";
connectAttr "ring_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[255]";
connectAttr "ring_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[256]";
connectAttr "ring_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[257]";
connectAttr "ring_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[258]";
connectAttr "ring_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[259]";
connectAttr "ring_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[260]";
connectAttr "ring_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[261]";
connectAttr "ring_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[262]";
connectAttr "ring_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[263]";
connectAttr "ring_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[264]";
connectAttr "ring_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[265]";
connectAttr "ring_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[266]";
connectAttr "ring_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[267]";
connectAttr "ring_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[268]";
connectAttr "ring_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[269]";
connectAttr "ring_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[270]";
connectAttr "ring_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[271]";
connectAttr "ring_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[272]";
connectAttr "pinky_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[273]";
connectAttr "pinky_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[274]";
connectAttr "pinky_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[275]";
connectAttr "pinky_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[276]";
connectAttr "pinky_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[277]";
connectAttr "pinky_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[278]";
connectAttr "pinky_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[279]";
connectAttr "pinky_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[280]";
connectAttr "pinky_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[281]";
connectAttr "pinky_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[282]";
connectAttr "pinky_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[283]";
connectAttr "pinky_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[284]";
connectAttr "pinky_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[285]";
connectAttr "pinky_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[286]";
connectAttr "pinky_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[287]";
connectAttr "pinky_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[288]";
connectAttr "pinky_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[289]";
connectAttr "pinky_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[290]";
connectAttr "pinky_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[291]";
connectAttr "pinky_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[292]";
connectAttr "pinky_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[293]";
connectAttr "pinky_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[294]";
connectAttr "pinky_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[295]";
connectAttr "pinky_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[296]";
connectAttr "pinky_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[297]";
connectAttr "pinky_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[298]";
connectAttr "pinky_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[299]";
connectAttr "pinky_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[300]";
connectAttr "thumb_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[301]";
connectAttr "thumb_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[302]";
connectAttr "thumb_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[303]";
connectAttr "thumb_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[304]";
connectAttr "thumb_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[305]";
connectAttr "thumb_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[306]";
connectAttr "thumb_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[307]";
connectAttr "thumb_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[308]";
connectAttr "thumb_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[309]";
connectAttr "thumb_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[310]";
connectAttr "thumb_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[311]";
connectAttr "thumb_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[312]";
connectAttr "thumb_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[313]";
connectAttr "thumb_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[314]";
connectAttr "thumb_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[315]";
connectAttr "thumb_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[316]";
connectAttr "thumb_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[317]";
connectAttr "thumb_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[318]";
connectAttr "thumb_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[319]";
connectAttr "thumb_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[320]";
connectAttr "thumb_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[321]";
connectAttr "thumb_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[322]";
connectAttr "thumb_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[323]";
connectAttr "thumb_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[324]";
connectAttr "thumb_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[325]";
connectAttr "thumb_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[326]";
connectAttr "thumb_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[327]";
connectAttr "ik_clavicle_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[328]";
connectAttr "ik_clavicle_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[329]";
connectAttr "ik_clavicle_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[330]";
connectAttr "fk_neck_01_anim_translateX.o" "Rig_UE4ManControllerRN.phl[331]";
connectAttr "fk_neck_01_anim_translateY.o" "Rig_UE4ManControllerRN.phl[332]";
connectAttr "fk_neck_01_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[333]";
connectAttr "fk_neck_01_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[334]";
connectAttr "fk_neck_01_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[335]";
connectAttr "fk_neck_01_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[336]";
connectAttr "fk_head_anim_translateX.o" "Rig_UE4ManControllerRN.phl[337]";
connectAttr "fk_head_anim_translateY.o" "Rig_UE4ManControllerRN.phl[338]";
connectAttr "fk_head_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[339]";
connectAttr "fk_head_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[340]";
connectAttr "fk_head_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[341]";
connectAttr "fk_head_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[342]";
connectAttr "fk_head_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[343]";
connectAttr "fk_head_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[344]";
connectAttr "fk_head_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[345]";
connectAttr "leg_L_settings_mode.o" "Rig_UE4ManControllerRN.phl[346]";
connectAttr "ik_foot_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[347]";
connectAttr "ik_foot_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[348]";
connectAttr "ik_foot_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[349]";
connectAttr "ik_foot_L_anim_stretch.o" "Rig_UE4ManControllerRN.phl[350]";
connectAttr "ik_foot_L_anim_stretchBias.o" "Rig_UE4ManControllerRN.phl[351]";
connectAttr "ik_foot_L_anim_squash.o" "Rig_UE4ManControllerRN.phl[352]";
connectAttr "ik_foot_L_anim_knee_twist.o" "Rig_UE4ManControllerRN.phl[353]";
connectAttr "ik_foot_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[354]";
connectAttr "ik_foot_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[355]";
connectAttr "ik_foot_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[356]";
connectAttr "ik_foot_L_anim_toeCtrlVis.o" "Rig_UE4ManControllerRN.phl[357]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[358]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[359]";
connectAttr "leg_L_toe_wiggle_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[360]";
connectAttr "leg_L_heel_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[361]";
connectAttr "leg_L_heel_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[362]";
connectAttr "leg_L_heel_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[363]";
connectAttr "leg_L_heel_ctrl_ballPivot.o" "Rig_UE4ManControllerRN.phl[364]";
connectAttr "leg_L_heel_ctrl_heelPivot.o" "Rig_UE4ManControllerRN.phl[365]";
connectAttr "leg_L_toe_tip_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[366]";
connectAttr "leg_L_toe_tip_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[367]";
connectAttr "leg_L_toe_tip_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[368]";
connectAttr "leg_R_settings_mode.o" "Rig_UE4ManControllerRN.phl[369]";
connectAttr "ik_foot_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[370]";
connectAttr "ik_foot_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[371]";
connectAttr "ik_foot_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[372]";
connectAttr "ik_foot_R_anim_stretch.o" "Rig_UE4ManControllerRN.phl[373]";
connectAttr "ik_foot_R_anim_stretchBias.o" "Rig_UE4ManControllerRN.phl[374]";
connectAttr "ik_foot_R_anim_squash.o" "Rig_UE4ManControllerRN.phl[375]";
connectAttr "ik_foot_R_anim_knee_twist.o" "Rig_UE4ManControllerRN.phl[376]";
connectAttr "ik_foot_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[377]";
connectAttr "ik_foot_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[378]";
connectAttr "ik_foot_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[379]";
connectAttr "ik_foot_R_anim_toeCtrlVis.o" "Rig_UE4ManControllerRN.phl[380]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[381]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[382]";
connectAttr "leg_R_toe_wiggle_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[383]";
connectAttr "leg_R_heel_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[384]";
connectAttr "leg_R_heel_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[385]";
connectAttr "leg_R_heel_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[386]";
connectAttr "leg_R_heel_ctrl_ballPivot.o" "Rig_UE4ManControllerRN.phl[387]";
connectAttr "leg_R_heel_ctrl_heelPivot.o" "Rig_UE4ManControllerRN.phl[388]";
connectAttr "leg_R_toe_tip_ctrl_rotateZ.o" "Rig_UE4ManControllerRN.phl[389]";
connectAttr "leg_R_toe_tip_ctrl_rotateY.o" "Rig_UE4ManControllerRN.phl[390]";
connectAttr "leg_R_toe_tip_ctrl_rotateX.o" "Rig_UE4ManControllerRN.phl[391]";
connectAttr "torso_settings_mode.o" "Rig_UE4ManControllerRN.phl[392]";
connectAttr "torso_body_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[393]";
connectAttr "torso_body_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[394]";
connectAttr "torso_body_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[395]";
connectAttr "torso_body_anim_translateX.o" "Rig_UE4ManControllerRN.phl[396]";
connectAttr "torso_body_anim_translateY.o" "Rig_UE4ManControllerRN.phl[397]";
connectAttr "torso_body_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[398]";
connectAttr "torso_hip_anim_translateX.o" "Rig_UE4ManControllerRN.phl[399]";
connectAttr "torso_hip_anim_translateY.o" "Rig_UE4ManControllerRN.phl[400]";
connectAttr "torso_hip_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[401]";
connectAttr "torso_hip_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[402]";
connectAttr "torso_hip_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[403]";
connectAttr "torso_hip_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[404]";
connectAttr "torso_mid_ik_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[405]";
connectAttr "torso_mid_ik_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[406]";
connectAttr "torso_mid_ik_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[407]";
connectAttr "torso_mid_ik_anim_translateX.o" "Rig_UE4ManControllerRN.phl[408]";
connectAttr "torso_mid_ik_anim_translateY.o" "Rig_UE4ManControllerRN.phl[409]";
connectAttr "torso_mid_ik_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[410]";
connectAttr "torso_chest_ik_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[411]";
connectAttr "torso_chest_ik_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[412]";
connectAttr "torso_chest_ik_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[413]";
connectAttr "torso_chest_ik_anim_translateX.o" "Rig_UE4ManControllerRN.phl[414]"
		;
connectAttr "torso_chest_ik_anim_translateY.o" "Rig_UE4ManControllerRN.phl[415]"
		;
connectAttr "torso_chest_ik_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[416]"
		;
connectAttr "torso_chest_ik_anim_stretch.o" "Rig_UE4ManControllerRN.phl[417]";
connectAttr "torso_chest_ik_anim_twist_amount.o" "Rig_UE4ManControllerRN.phl[418]"
		;
connectAttr "torso_chest_ik_anim_autoSpine.o" "Rig_UE4ManControllerRN.phl[419]";
connectAttr "torso_chest_ik_anim_rotationInfluence.o" "Rig_UE4ManControllerRN.phl[420]"
		;
connectAttr "torso_chest_ik_anim_squash.o" "Rig_UE4ManControllerRN.phl[421]";
connectAttr "hyperLayout1.msg" "animBot.hl";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "animBot.animBot_Anim_Recovery_Scene_ID"
		;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "hyperLayout1.hyp[0].dn";
connectAttr "sharedReferenceNode.sr" "Rig_UE4ManControllerRN.sr";
connectAttr "groupId5.id" "groupParts5.gi";
connectAttr "pairBlend1_inTranslateX1.o" "pairBlend1.itx1";
connectAttr "pairBlend1_inTranslateY1.o" "pairBlend1.ity1";
connectAttr "pairBlend1_inTranslateZ1.o" "pairBlend1.itz1";
connectAttr "pairBlend1_inRotateX1.o" "pairBlend1.irx1";
connectAttr "pairBlend1_inRotateY1.o" "pairBlend1.iry1";
connectAttr "pairBlend1_inRotateZ1.o" "pairBlend1.irz1";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "groupId5.msg" ":defaultLastHiddenSet.gn" -na;
// End of 1P_Mannequin_Crouch_01.ma
