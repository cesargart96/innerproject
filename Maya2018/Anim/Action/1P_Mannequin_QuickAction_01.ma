//Maya ASCII 2018ff09 scene
//Name: 1P_Mannequin_QuickAction_01.ma
//Last modified: Tue, Sep 24, 2019 10:54:08 PM
//Codeset: 1252
file -rdi 1 -ns "Rig_UE4ManController" -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
file -rdi 2 -ns "SK_Mannequin" -rfn "Rig_UE4ManController:SK_MannequinRN" -op
		 "v=0;" -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/SkinnedMesh/SK_Mannequin.ma";
file -rdi 1 -ns "TargetCubes" -rfn "TargetCubesRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Cubes/TargetCubes.ma";
file -r -ns "Rig_UE4ManController" -dr 1 -rfn "Rig_UE4ManControllerRN" -op "v=0;"
		 -typ "mayaAscii" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
file -r -ns "TargetCubes" -dr 1 -rfn "TargetCubesRN" -op "v=0;" -typ "mayaAscii"
		 "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Cubes/TargetCubes.ma";
requires maya "2018ff09";
requires -nodeType "ilrOptionsNode" -nodeType "ilrUIOptionsNode" -nodeType "ilrBakeLayerManager"
		 -nodeType "ilrBakeLayer" "Turtle" "2018.0.0";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201811122215-49253d42f6";
fileInfo "osv" "Microsoft Windows 8 Business Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "9B71BA30-4D91-37E0-F8A1-76860572EF0C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 234.52781486032515 132.41511926139984 370.65370742162111 ;
	setAttr ".r" -type "double3" -6.3383527295785074 34.599999999993564 -9.6598656159674635e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "040CCD28-47B7-36F4-7075-158AB9BE94F6";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 880.69068533641996;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -21.09400749206543 135.26528930664063 32.4224853515625 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "22E1EC1C-4B99-370C-4ED5-64A46726A3C8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "3C451472-4A71-EA39-E829-3EA2ED993072";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "FAA32395-4DDE-EAF3-353A-EF83E3FBBA46";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -27.853117444955078 114.66513277451065 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "899DFA0A-411A-D494-52C5-E29F29354815";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 457.73775315995857;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "4BC5C9E7-4524-0750-E579-1283483E856A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E25881E2-466F-FDA8-671F-CC887393B509";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode dagContainer -n "animBot";
	rename -uid "A4FECEBD-4BFD-FC3B-C1D0-34829DE42924";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -s false -ci true -sn "animBot_Anim_Recovery_Scene_ID" -ln "animBot_Anim_Recovery_Scene_ID" 
		-at "message";
	addAttr -s false -ci true -sn "animBot_Select_Sets" -ln "animBot_Select_Sets" -at "message";
	addAttr -s false -ci true -sn "__Red__" -ln "__Red__" -at "message";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".iconSimpleName" -type "string" "animBot";
createNode dagContainer -n "animBot_Anim_Recovery_Scene_ID" -p "animBot";
	rename -uid "48859900-462F-A61E-3BCA-BC99D53BDBE6";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "sceneID" -ln "sceneID" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "anim_recovery";
	setAttr ".sceneID" -type "string" "1569311827.351000";
createNode dagContainer -n "animBot_Select_Sets" -p "animBot";
	rename -uid "98EB45BF-444E-D868-157D-97982EA21004";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "select_sets";
createNode dagContainer -n "__Red__" -p "animBot_Select_Sets";
	rename -uid "848B26C8-4F24-CB4C-4543-D4A867D32C78";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "iconSimpleName" -ln "iconSimpleName" -dt "string";
	addAttr -ci true -sn "colorIndex" -ln "colorIndex" -at "long";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".uocol" yes;
	setAttr ".oclr" -type "float3" 0.72941178 0.32156864 0.31764707 ;
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr ".iconSimpleName" -type "string" "color_7";
	setAttr ".colorIndex" 7;
createNode transform -n "All_BodyIK" -p "__Red__";
	rename -uid "41A2C381-453C-3F1D-EBE1-17AF4BCC1563";
	addAttr -ci true -sn "animBot" -ln "animBot" -nn "animBot" -dt "string";
	addAttr -ci true -sn "contents" -ln "contents" -dt "string";
	setAttr -l on -k off ".v";
	setAttr ".ovdt" 2;
	setAttr ".ove" yes;
	setAttr -l on -k off ".tx";
	setAttr -l on -k off ".ty";
	setAttr -l on -k off ".tz";
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr -l on -k off ".rz";
	setAttr -l on -k off ".sx";
	setAttr -l on -k off ".sy";
	setAttr -l on -k off ".sz";
	setAttr ".animBot" -type "string" "1.0.13";
	setAttr -l on ".contents" -type "string" (
		"|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_chest_ik_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_hip_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_hip_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_trans_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim_driver_grp|Rig_UE4ManController:SK_Mannequin:torso_mid_ik_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_tip_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_heel_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_R_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_R_toe_wiggle_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_R_group|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_tip_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_heel_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_master_foot_ball_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_tip_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_inside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_outside_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_heel_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot_grp|Rig_UE4ManController:SK_Mannequin:leg_L_ik_foot_toe_pivot|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl_grp|Rig_UE4ManController:SK_Mannequin:leg_L_toe_wiggle_ctrl |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:leg_L_group|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_foot_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_R_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_clavicle_grp|Rig_UE4ManController:SK_Mannequin:arm_L_clav_ctrl_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_clavicle_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim |Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:root_anim");
createNode transform -n "left";
	rename -uid "12F39A2E-4FE9-6EBF-0B3B-FB8BD304D3E5";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1090.1970879118292 151.12237691488295 210.82478638803369 ;
	setAttr ".r" -type "double3" 0 -89.999999999999986 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "38A56CEF-41A2-3286-EB86-44BD159C329D";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1140.1970879118271;
	setAttr ".ow" 1078.1167718299623;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".tp" -type "double3" 49.999999999997954 167.62431881957681 400 ;
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "900AD80C-46C3-FCEE-ECEA-A385A3F13367";
	setAttr -s 266 ".lnk";
	setAttr -s 266 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "5A5C5A00-4661-1B47-3CF6-36A00C4E85FB";
	setAttr -s 3 ".bsdt";
	setAttr ".bsdt[0].bscd" -type "Int32Array" 1 0 ;
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "32F0CFB7-4092-3933-8E4B-9DA4C82C4B87";
createNode displayLayerManager -n "layerManager";
	rename -uid "1375649D-4504-E405-C3F7-45BE6B90A5FB";
	setAttr ".cdl" 2;
	setAttr -s 3 ".dli[1:2]"  1 2;
createNode displayLayer -n "defaultLayer";
	rename -uid "24CE7960-4C6C-ECA7-197A-23BAA0C634A7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "25655752-470B-1409-C751-4485C50C4620";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "C4DFE456-470E-4AC8-6521-E889CF0E7EB6";
	setAttr ".g" yes;
createNode hyperLayout -n "hyperLayout1";
	rename -uid "7A75F400-4B93-2B36-D3EC-0D99FB2FE2B8";
	setAttr ".ihi" 0;
	setAttr -s 2 ".hyp";
createNode reference -n "Rig_UE4ManControllerRN";
	rename -uid "CBC4EE03-4672-3F8F-F837-8DA731DA7206";
	setAttr ".fn[0]" -type "string" "C:/Users/Asus User/Documents/SourceTree/UnrealProjects/InnerProject/Maya2018//Ref/Rig/Rig_UE4ManController.ma";
	setAttr -s 173 ".phl";
	setAttr ".phl[1]" 0;
	setAttr ".phl[2]" 0;
	setAttr ".phl[3]" 0;
	setAttr ".phl[4]" 0;
	setAttr ".phl[5]" 0;
	setAttr ".phl[6]" 0;
	setAttr ".phl[7]" 0;
	setAttr ".phl[8]" 0;
	setAttr ".phl[9]" 0;
	setAttr ".phl[10]" 0;
	setAttr ".phl[11]" 0;
	setAttr ".phl[12]" 0;
	setAttr ".phl[13]" 0;
	setAttr ".phl[14]" 0;
	setAttr ".phl[15]" 0;
	setAttr ".phl[16]" 0;
	setAttr ".phl[17]" 0;
	setAttr ".phl[18]" 0;
	setAttr ".phl[19]" 0;
	setAttr ".phl[20]" 0;
	setAttr ".phl[21]" 0;
	setAttr ".phl[22]" 0;
	setAttr ".phl[23]" 0;
	setAttr ".phl[24]" 0;
	setAttr ".phl[25]" 0;
	setAttr ".phl[26]" 0;
	setAttr ".phl[27]" 0;
	setAttr ".phl[28]" 0;
	setAttr ".phl[29]" 0;
	setAttr ".phl[30]" 0;
	setAttr ".phl[31]" 0;
	setAttr ".phl[32]" 0;
	setAttr ".phl[33]" 0;
	setAttr ".phl[34]" 0;
	setAttr ".phl[35]" 0;
	setAttr ".phl[36]" 0;
	setAttr ".phl[37]" 0;
	setAttr ".phl[38]" 0;
	setAttr ".phl[39]" 0;
	setAttr ".phl[40]" 0;
	setAttr ".phl[41]" 0;
	setAttr ".phl[42]" 0;
	setAttr ".phl[43]" 0;
	setAttr ".phl[44]" 0;
	setAttr ".phl[45]" 0;
	setAttr ".phl[46]" 0;
	setAttr ".phl[47]" 0;
	setAttr ".phl[48]" 0;
	setAttr ".phl[49]" 0;
	setAttr ".phl[50]" 0;
	setAttr ".phl[51]" 0;
	setAttr ".phl[52]" 0;
	setAttr ".phl[53]" 0;
	setAttr ".phl[54]" 0;
	setAttr ".phl[55]" 0;
	setAttr ".phl[56]" 0;
	setAttr ".phl[57]" 0;
	setAttr ".phl[58]" 0;
	setAttr ".phl[59]" 0;
	setAttr ".phl[60]" 0;
	setAttr ".phl[61]" 0;
	setAttr ".phl[62]" 0;
	setAttr ".phl[63]" 0;
	setAttr ".phl[64]" 0;
	setAttr ".phl[65]" 0;
	setAttr ".phl[66]" 0;
	setAttr ".phl[67]" 0;
	setAttr ".phl[68]" 0;
	setAttr ".phl[69]" 0;
	setAttr ".phl[70]" 0;
	setAttr ".phl[71]" 0;
	setAttr ".phl[72]" 0;
	setAttr ".phl[73]" 0;
	setAttr ".phl[74]" 0;
	setAttr ".phl[75]" 0;
	setAttr ".phl[76]" 0;
	setAttr ".phl[77]" 0;
	setAttr ".phl[78]" 0;
	setAttr ".phl[79]" 0;
	setAttr ".phl[80]" 0;
	setAttr ".phl[81]" 0;
	setAttr ".phl[82]" 0;
	setAttr ".phl[83]" 0;
	setAttr ".phl[84]" 0;
	setAttr ".phl[85]" 0;
	setAttr ".phl[86]" 0;
	setAttr ".phl[87]" 0;
	setAttr ".phl[88]" 0;
	setAttr ".phl[89]" 0;
	setAttr ".phl[90]" 0;
	setAttr ".phl[91]" 0;
	setAttr ".phl[92]" 0;
	setAttr ".phl[93]" 0;
	setAttr ".phl[94]" 0;
	setAttr ".phl[95]" 0;
	setAttr ".phl[96]" 0;
	setAttr ".phl[97]" 0;
	setAttr ".phl[98]" 0;
	setAttr ".phl[99]" 0;
	setAttr ".phl[100]" 0;
	setAttr ".phl[101]" 0;
	setAttr ".phl[102]" 0;
	setAttr ".phl[103]" 0;
	setAttr ".phl[104]" 0;
	setAttr ".phl[105]" 0;
	setAttr ".phl[106]" 0;
	setAttr ".phl[107]" 0;
	setAttr ".phl[108]" 0;
	setAttr ".phl[109]" 0;
	setAttr ".phl[110]" 0;
	setAttr ".phl[111]" 0;
	setAttr ".phl[112]" 0;
	setAttr ".phl[113]" 0;
	setAttr ".phl[114]" 0;
	setAttr ".phl[115]" 0;
	setAttr ".phl[116]" 0;
	setAttr ".phl[117]" 0;
	setAttr ".phl[118]" 0;
	setAttr ".phl[119]" 0;
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"Rig_UE4ManControllerRN"
		"Rig_UE4ManControllerRN" 0
		"Rig_UE4ManController:SK_MannequinRN" 0
		"Rig_UE4ManControllerRN" 15
		2 "|Rig_UE4ManController:JointMover|Rig_UE4ManController:torso_mover_grp|Rig_UE4ManController:torso_pelvis_mover_grp|Rig_UE4ManController:torso_pelvis_mover|Rig_UE4ManController:torso_spine_01_mover_grp|Rig_UE4ManController:torso_spine_01_mover|Rig_UE4ManController:torso_spine_02_mover_grp|Rig_UE4ManController:torso_spine_02_mover|Rig_UE4ManController:torso_spine_03_mover_grp|Rig_UE4ManController:torso_spine_03_mover|Rig_UE4ManController:torso_rig_fk_spine_03_control" 
		"translate" " -type \"double3\" 0 2.6827646523597064 0"
		2 "|Rig_UE4ManController:JointMover|Rig_UE4ManController:torso_mover_grp|Rig_UE4ManController:torso_pelvis_mover_grp|Rig_UE4ManController:torso_pelvis_mover|Rig_UE4ManController:torso_spine_01_mover_grp|Rig_UE4ManController:torso_spine_01_mover|Rig_UE4ManController:torso_spine_02_mover_grp|Rig_UE4ManController:torso_spine_02_mover|Rig_UE4ManController:torso_rig_fk_spine_02_control" 
		"translate" " -type \"double3\" 0 2.6827646523597064 0"
		2 "|Rig_UE4ManController:JointMover|Rig_UE4ManController:torso_mover_grp|Rig_UE4ManController:torso_pelvis_mover_grp|Rig_UE4ManController:torso_pelvis_mover|Rig_UE4ManController:torso_spine_01_mover_grp|Rig_UE4ManController:torso_spine_01_mover|Rig_UE4ManController:torso_rig_fk_spine_01_control" 
		"translate" " -type \"double3\" 0 2.6827646523597064 0"
		2 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_CamShape" "overscan" 
		" 1.3"
		2 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_CamShape" "tumblePivot" 
		" -type \"double3\" -16.66524124145507813 151.1822357177734375 39.39555740356445313"
		
		2 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_CamShape" "displayFilmGate" 
		" 1"
		2 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_CamShape" "displayResolution" 
		" 0"
		2 "|Rig_UE4ManController:FP_Cam|Rig_UE4ManController:FP_CamShape" "displayFieldChart" 
		" 1"
		2 "Rig_UE4ManController:Skel" "visibility" " 0"
		2 "Rig_UE4ManController:Skel" "displayOrder" " 3"
		2 "Rig_UE4ManController:MannequinMesh" "visibility" " 1"
		2 "Rig_UE4ManController:IKBones" "visibility" " 0"
		2 "Rig_UE4ManController:IKBones" "displayOrder" " 4"
		2 "Rig_UE4ManController:proxy_geo_layer" "displayOrder" " 5"
		5 3 "Rig_UE4ManControllerRN" "Rig_UE4ManController:groupParts117.outputGeometry" 
		"Rig_UE4ManControllerRN.placeHolderList[173]" "Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.i"
		
		"Rig_UE4ManController:SK_MannequinRN" 263
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"instObjGroups.objectGroups" " -s 12"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvPivot" " -type \"double2\" 0.39557498693466187 0.898499995470047"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"DiffuseUV\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].colorName" " -type \"string\" \"colorSet0\""
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].clamped" " 0"
		2 "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape" 
		"colorSet[0].representation" " 4"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings" 
		"mode" " -av -k 1 1"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_L_ik_elbow_anim" 
		"translate" " -type \"double3\" -13.99024188366438182 -5.39792591423446311 -79.21306094157108646"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"translate" " -type \"double3\" -43.93934498704995661 0 -52.61145223110777636"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"rotate" " -type \"double3\" -76.67605431299830343 74.84239177988951042 -77.12387819966173197"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"stretch" " -av -k 1 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"elbowLock" " -av -k 1 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"side" " -av -k 1 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim" 
		"follow" " -av 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_counter_twist_grp|Rig_UE4ManController:SK_Mannequin:upperarm_twist_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:upperarm_twist_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:upperarm_twist_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 8.46313130111348855"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim" 
		"rotate" " -type \"double3\" 0 0 8.46313130111348855"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_L_anim|Rig_UE4ManController:SK_Mannequin:index_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_L_anim|Rig_UE4ManController:SK_Mannequin:index_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_L_anim" 
		"rotate" " -type \"double3\" 0 0 8.46313130111348855"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 14.94750715752827297"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim" 
		"rotate" " -type \"double3\" 0 0 14.94750715752827297"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_L_anim|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_L_anim|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_L_anim" 
		"rotate" " -type \"double3\" 0 0 14.94750715752827297"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 19.09219263896679664"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim" 
		"rotate" " -type \"double3\" 0 0 19.09219263896679664"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_L_anim|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_L_anim|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_L_anim" 
		"rotate" " -type \"double3\" 0 0 19.09219263896679664"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 17.67335062369717491"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim" 
		"rotate" " -type \"double3\" 0 0 17.67335062369717491"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_L_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_L_anim" 
		"rotate" " -type \"double3\" 0 0 17.67335062369717491"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim" 
		"rotate" " -type \"double3\" 0 0 7.2902316718950555"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim" 
		"rotate" " -type \"double3\" 0 0 14.62618405207220285"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_finger_group|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_L_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_L_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_L_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_L_anim" 
		"rotate" " -type \"double3\" 0 0 26.25182592275459115"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_ik_elbow_anim" 
		"translate" " -type \"double3\" 8.30969980563897082 -26.15981760711308368 -98.43128789748165275"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translate" " -type \"double3\" 41.71592722155092048 -21.89334753923386501 -26.16821125529135372"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"translateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotate" " -type \"double3\" 20.68250268032607408 -193.14864652957615476 -73.86649199779442654"
		
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim" 
		"sticky" " -k 1"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotate" " -type \"double3\" -7.64051515345672616 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim" 
		"sticky" " -k 1"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim" 
		"rotate" " -type \"double3\" -7.64051515345672616 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim" 
		"rotate" " -type \"double3\" -7.64051515345672616 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateX" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim" 
		"sticky" " -k 1"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim" 
		"sticky" " -k 1"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim" 
		"rotateY" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotate" " -type \"double3\" 0 0 18.20049609093251419"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim" 
		"rotate" " -type \"double3\" 0 0 11.59138384761199525"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim" 
		"rotate" " -type \"double3\" 0 0 11.59138384761199525"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim" 
		"rotateZ" " -av"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_middle_pv_anim_grp|Rig_UE4ManController:SK_Mannequin:arm_R_middle_pv_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:Head_group|Rig_UE4ManController:SK_Mannequin:Head_ctrl_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_neck_01_anim|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:fk_head_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:fk_head_anim_grp|Rig_UE4ManController:SK_Mannequin:fk_head_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:torso_body_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:torso_body_anim_grp|Rig_UE4ManController:SK_Mannequin:torso_body_anim" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"translate" " -type \"double3\" 0 128.56230710800656425 -1.65479764133225227"
		2 "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:torso_group|Rig_UE4ManController:SK_Mannequin:torso_spine_ctrl_grp|Rig_UE4ManController:SK_Mannequin:torso_ik_grp|Rig_UE4ManController:SK_Mannequin:splineIK_spine_01_splineIK" 
		"rotate" " -type \"double3\" -89.99999999999998579 0 89.99999999999968736"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "displayType" " 2"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "visibility" " 1"
		2 "Rig_UE4ManController:SK_Mannequin:layer1" "displayOrder" " 2"
		3 "Rig_UE4ManController:groupParts117.outputGeometry" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.instObjGroups.objectGroups[20].objectGroupId" 
		"Rig_UE4ManControllerRN.placeHolderList[1]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:SK_Man|Rig_UE4ManController:SK_Mannequin:SK_Mannequin|Rig_UE4ManController:SK_Mannequin:SK_MannequinShape.inMesh" 
		"Rig_UE4ManControllerRN.placeHolderList[2]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[3]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[4]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[5]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[6]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[7]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[8]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[9]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[10]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[11]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[12]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[13]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[14]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.follow" 
		"Rig_UE4ManControllerRN.placeHolderList[15]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[16]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[17]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_L_group|Rig_UE4ManController:SK_Mannequin:arm_L_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_L_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[18]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_settings.mode" 
		"Rig_UE4ManControllerRN.placeHolderList[19]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[20]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[21]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[22]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.stretch" 
		"Rig_UE4ManControllerRN.placeHolderList[23]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.slide" 
		"Rig_UE4ManControllerRN.placeHolderList[24]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.elbowLock" 
		"Rig_UE4ManControllerRN.placeHolderList[25]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.side" 
		"Rig_UE4ManControllerRN.placeHolderList[26]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[27]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.tip_pivot" 
		"Rig_UE4ManControllerRN.placeHolderList[28]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_swivel" 
		"Rig_UE4ManControllerRN.placeHolderList[29]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.mid_bend" 
		"Rig_UE4ManControllerRN.placeHolderList[30]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[31]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[32]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_arm_ik_ctrls_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher_follow|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_space_switcher|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ik_hand_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[33]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[34]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[35]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[36]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[37]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[38]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[39]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[40]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[41]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[42]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[43]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[44]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[45]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[46]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[47]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[48]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[49]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[50]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[51]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[52]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[53]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[54]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[55]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[56]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[57]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[58]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[59]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[60]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:index_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_01_R_anim|Rig_UE4ManController:SK_Mannequin:index_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_02_R_anim|Rig_UE4ManController:SK_Mannequin:index_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:index_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[61]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[62]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[63]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[64]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[65]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[66]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[67]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[68]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[69]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[70]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[71]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[72]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[73]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[74]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[75]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[76]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[77]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[78]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[79]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[80]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[81]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[82]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[83]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[84]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[85]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[86]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[87]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[88]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_01_R_anim|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_02_R_anim|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:middle_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[89]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[90]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[91]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[92]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[93]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[94]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[95]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[96]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[97]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[98]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[99]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[100]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[101]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[102]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[103]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[104]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[105]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[106]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[107]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[108]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[109]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[110]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[111]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[112]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[113]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[114]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[115]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[116]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_01_R_anim|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_02_R_anim|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:ring_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[117]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[118]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[119]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[120]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[121]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[122]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[123]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[124]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[125]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[126]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim.sticky" 
		"Rig_UE4ManControllerRN.placeHolderList[127]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[128]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[129]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[130]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[131]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[132]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[133]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[134]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[135]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[136]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[137]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[138]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[139]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[140]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[141]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[142]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[143]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[144]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_01_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_02_R_anim|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:pinky_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[145]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[146]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[147]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[148]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[149]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[150]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[151]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[152]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[153]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[154]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[155]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[156]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[157]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[158]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[159]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[160]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[161]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[162]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[163]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateX" 
		"Rig_UE4ManControllerRN.placeHolderList[164]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateY" 
		"Rig_UE4ManControllerRN.placeHolderList[165]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.translateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[166]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateX" 
		"Rig_UE4ManControllerRN.placeHolderList[167]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateY" 
		"Rig_UE4ManControllerRN.placeHolderList[168]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.rotateZ" 
		"Rig_UE4ManControllerRN.placeHolderList[169]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleX" 
		"Rig_UE4ManControllerRN.placeHolderList[170]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleY" 
		"Rig_UE4ManControllerRN.placeHolderList[171]" ""
		5 4 "Rig_UE4ManControllerRN" "|Rig_UE4ManController:SK_Mannequin:master_anim|Rig_UE4ManController:SK_Mannequin:offset_anim|Rig_UE4ManController:SK_Mannequin:arm_R_group|Rig_UE4ManController:SK_Mannequin:arm_R_finger_group|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_master_grp|Rig_UE4ManController:SK_Mannequin:arm_R_hand_driven_grp|Rig_UE4ManController:SK_Mannequin:arm_R_fk_finger_ctrls|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_01_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_02_R_anim|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_driven_grp|Rig_UE4ManController:SK_Mannequin:thumb_03_R_anim.scaleZ" 
		"Rig_UE4ManControllerRN.placeHolderList[172]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode ilrOptionsNode -s -n "TurtleRenderOptions";
	rename -uid "F174DB98-42F4-C6EB-6B69-21B88D373DCE";
lockNode -l 1 ;
createNode ilrUIOptionsNode -s -n "TurtleUIOptions";
	rename -uid "A3756E06-4C22-85FA-A873-A883FBF7A71A";
lockNode -l 1 ;
createNode ilrBakeLayerManager -s -n "TurtleBakeLayerManager";
	rename -uid "72578236-4D72-4F99-CF17-64885EFB820C";
lockNode -l 1 ;
createNode ilrBakeLayer -s -n "TurtleDefaultBakeLayer";
	rename -uid "DE8000B5-4133-0645-B895-429447408B7E";
lockNode -l 1 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "1B9B6DC3-42D7-7AB3-E5DA-F5ACED2968F7";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n"
		+ "            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n"
		+ "            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1221\n            -height 651\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n"
		+ "            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n"
		+ "            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n"
		+ "            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n"
		+ "            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n"
		+ "                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\t}\n\t\t} else {\n"
		+ "\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n"
		+ "                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n"
		+ "                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n"
		+ "                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Model Panel5\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Model Panel5\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"left\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n"
		+ "            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n"
		+ "            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n"
		+ "            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Model Panel6\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Model Panel6\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"Rig_UE4ManController:FP_CamShape\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n"
		+ "            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n"
		+ "            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1119\n            -height 651\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1221\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1221\\n    -height 651\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 1000 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "1AFC679D-489C-E075-DD59-13B48A3EFC8B";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 120 ";
	setAttr ".st" 6;
createNode groupId -n "groupId5";
	rename -uid "30D16B17-4481-D4C0-5F15-7AB2420F31B8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "DBDFA0E2-4441-71F6-C9FB-76B05E6D0ED6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[25958:27625]" "f[31580:31595]";
createNode reference -n "sharedReferenceNode";
	rename -uid "320DD024-4F9C-ABDA-F405-B3814DAD3CCE";
	setAttr ".ed" -type "dataReferenceEdits" 
		"sharedReferenceNode";
createNode reference -n "TargetCubesRN";
	rename -uid "1685898A-4CEF-E0F6-8054-58845B632F5B";
	setAttr ".ed" -type "dataReferenceEdits" 
		"TargetCubesRN"
		"TargetCubesRN" 0;
lockNode -l 1 ;
createNode animCurveTA -n "ik_hand_L_anim_rotateX";
	rename -uid "296503FA-4C6F-D7BC-F0A7-4A8A16746645";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 -76.676054312998247 0 -76.676054312998303;
createNode animCurveTA -n "ik_hand_L_anim_rotateY";
	rename -uid "8CA8697F-4120-5595-C742-4393398BD1BB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 74.84239177988951 0 74.84239177988951;
createNode animCurveTA -n "ik_hand_L_anim_rotateZ";
	rename -uid "EEF56840-4AC1-9AA5-4789-7FBA0113084C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 -77.123878199661647 0 -77.123878199661732;
createNode animCurveTL -n "ik_hand_L_anim_translateX";
	rename -uid "8353E053-4E5D-5F9F-FDA7-9A8407BA1B1D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 -43.939344987049971 0 -43.939344987049957;
createNode animCurveTL -n "ik_hand_L_anim_translateY";
	rename -uid "5600D1C6-4335-2508-DE98-80AFEDDE4837";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 1.027468466842631e-14 0 7.9936057773011271e-15;
createNode animCurveTL -n "ik_hand_L_anim_translateZ";
	rename -uid "081F9ACE-400E-F903-C7FA-30B9A573BDB6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 -52.611452231107776 0 -52.611452231107776;
createNode animCurveTU -n "ik_hand_L_anim_stretch";
	rename -uid "CB2EB591-4054-319F-2E15-BE9F34AA9907";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_elbowLock";
	rename -uid "2B6473DB-4088-E39A-90DD-758C1312D933";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_slide";
	rename -uid "3F55E6D6-4A6C-A9BD-0230-0BB0E92C2870";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "arm_L_settings_mode";
	rename -uid "10F0C729-48B2-2BBC-44D0-B28E9F982099";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 1 0 1;
createNode animCurveTU -n "ik_hand_L_anim_side";
	rename -uid "CB918ABF-4C0E-909F-759C-D7B559168300";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_bend";
	rename -uid "A928DDFD-432C-875D-8CC8-38A6D004DCB2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_mid_swivel";
	rename -uid "B8374EC4-41B7-FC0C-835D-0EBF74EDB798";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_pivot";
	rename -uid "47648440-4151-40CA-AFB1-60AFA0F887A8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode animCurveTU -n "ik_hand_L_anim_tip_swivel";
	rename -uid "95F28EFC-4A9D-F222-2415-3B94EF4CF85A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
createNode hyperLayout -n "hyperLayout2";
	rename -uid "4662EB10-4736-E339-8D3E-F980D6B07F20";
	setAttr ".ihi" 0;
createNode hyperLayout -n "hyperLayout3";
	rename -uid "C8AC6A1E-4100-3E53-F072-5184F850AA7D";
	setAttr ".ihi" 0;
createNode animCurveTU -n "ik_hand_L_anim_follow";
	rename -uid "01C1250F-4BD4-4812-FBDC-D0AC0B64F19B";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  -1 0 0 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "ik_hand_R_anim_translateX";
	rename -uid "A0C46E26-4C3B-4DF4-59AD-30810219ADF3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 41.71592722155092 30 58.729591159731392
		 60 58.660475512080602 95 59.627380364670088 120 41.71592722155092;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTL -n "ik_hand_R_anim_translateY";
	rename -uid "BDF9DEC1-44FF-ADFD-5C59-A4A2DC0944A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -21.893347539233865 30 -47.244148434723847
		 60 -49.677905060076462 95 -46.592187050579213 120 -21.893347539233865;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTL -n "ik_hand_R_anim_translateZ";
	rename -uid "F7E287C0-4930-7D06-EA57-2195B79329CF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  0 -26.168211255291354 30 4.7190391883525971
		 60 3.035064740453195 95 4.5593039428721491 120 -26.168211255291354;
	setAttr -s 5 ".kit[4]"  1;
	setAttr -s 5 ".kot[4]"  1;
	setAttr -s 5 ".kix[4]"  1;
	setAttr -s 5 ".kiy[4]"  0;
	setAttr -s 5 ".kox[4]"  1;
	setAttr -s 5 ".koy[4]"  0;
createNode animCurveTA -n "ik_hand_R_anim_rotateX";
	rename -uid "BD5348F8-460F-9664-DE12-B3895691CD20";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 20.682502680326074 14 30.501421694990569
		 30 54.212144319719016 60 52.792083931306145 95 63.445077538460119 120 20.682502680326074;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  1;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  1;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTA -n "ik_hand_R_anim_rotateY";
	rename -uid "5F1EF4E0-4F84-63E9-080E-9B882285D8F5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -193.14864652957615 14 -184.79653297980926
		 30 -230.79822325837893 60 -219.56223135415158 95 -226.65261263914408 120 -193.14864652957615;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  1;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  1;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTA -n "ik_hand_R_anim_rotateZ";
	rename -uid "2CF1C77C-44A2-BAF1-F771-6E9EC877B6DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 6 ".ktv[0:5]"  0 -73.866491997794427 14 -76.22257786506708
		 30 -41.232674392293347 60 -51.533678475415918 95 -36.024972105316472 120 -73.866491997794427;
	setAttr -s 6 ".kit[5]"  1;
	setAttr -s 6 ".kot[5]"  1;
	setAttr -s 6 ".kix[5]"  1;
	setAttr -s 6 ".kiy[5]"  0;
	setAttr -s 6 ".kox[5]"  1;
	setAttr -s 6 ".koy[5]"  0;
createNode animCurveTU -n "ik_hand_R_anim_stretch";
	rename -uid "DB7EA27F-4624-DEFB-DDBF-218FB458FEFF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_elbowLock";
	rename -uid "6503C6CC-472C-C2B9-EBF3-36B262B90589";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_slide";
	rename -uid "260CEC40-4EF6-4D7E-7168-47B433B6EEF0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "arm_R_settings_mode";
	rename -uid "685715FD-4334-CC91-FF2B-E583D288A4F8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 1 30 1 95 1 120 1;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_side";
	rename -uid "B8D401BA-4F17-D42D-8957-658DE68613D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_mid_bend";
	rename -uid "A43AA28E-4F92-446C-742D-959850D2B913";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_mid_swivel";
	rename -uid "6B5DBD20-4052-4F4E-532A-7E893D17E682";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_tip_pivot";
	rename -uid "8260C9F9-4B3E-FE13-1643-10AF59EA5AB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTU -n "ik_hand_R_anim_tip_swivel";
	rename -uid "E286B7BC-457C-92D1-A7BC-BE9B2F29537F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 0 95 0 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "index_01_R_anim_rotateX";
	rename -uid "068CDEC9-465F-55B5-23A6-8F8D5CB63FEE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_01_R_anim_rotateY";
	rename -uid "D8D19EDA-4FC1-DD84-9685-8EA01C638E99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 -2.1978702818750055 95 -2.1978702818750055
		 120 -2.1978702818750055;
createNode animCurveTA -n "index_01_R_anim_rotateZ";
	rename -uid "97FA629B-4B38-7888-89A9-97B933E05ACB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_02_R_anim_rotateX";
	rename -uid "6C65EBCC-4A8C-9B32-3A50-E8B278042C01";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_02_R_anim_rotateY";
	rename -uid "A8FC5CE6-4216-B1F8-CE5F-319D94414EFB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_02_R_anim_rotateZ";
	rename -uid "94958199-4DFC-B6B3-66E4-999D13E29488";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_03_R_anim_rotateX";
	rename -uid "A67874E6-456D-525B-BFF1-6E991475D32F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_03_R_anim_rotateY";
	rename -uid "087DE133-40EC-1A15-E508-118345B299FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "index_03_R_anim_rotateZ";
	rename -uid "89D0640A-427D-3C4D-C91E-8DBDA8961A85";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "middle_01_R_anim_rotateX";
	rename -uid "2F1AEE8C-49EA-4188-982E-5482EE3795F2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -7.6405151534567262 30 3.4111736192297575
		 95 3.4111736192297575 120 -7.6405151534567262;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_01_R_anim_rotateY";
	rename -uid "C0602DBE-479A-8C0F-B445-47B7E14A4D4E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 7.4248558367174953 95 7.4248558367174953
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_01_R_anim_rotateZ";
	rename -uid "5F25B914-4ED8-B916-6895-E8B0A523FAA8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.276308237498228 95 76.276308237498228
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_02_R_anim_rotateX";
	rename -uid "7DEA06DF-4546-3D87-FD6A-8BA623C45415";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -7.6405151534567262 30 -1.8075670628056828
		 95 -1.8075670628056828 120 -7.6405151534567262;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_02_R_anim_rotateY";
	rename -uid "84ACE5B3-419F-F55B-F8F0-EE968DCBB544";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 7.4248558367174953 95 7.4248558367174953
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_02_R_anim_rotateZ";
	rename -uid "34297FBF-4178-BA07-3FD9-26B95138F1DE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.276308237498228 95 76.276308237498228
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_03_R_anim_rotateX";
	rename -uid "FF55639A-40C9-55F8-3600-2AA05496CFDB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 -7.6405151534567262 30 -1.8075670628056828
		 95 -1.8075670628056828 120 -7.6405151534567262;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_03_R_anim_rotateY";
	rename -uid "AAE01C53-4E64-9B35-1C09-ABBCFE0A2463";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 7.4248558367174953 95 7.4248558367174953
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "middle_03_R_anim_rotateZ";
	rename -uid "EEA90BE1-4EFE-1D72-327D-F9A96FE225B3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.276308237498228 95 76.276308237498228
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "pinky_01_R_anim_rotateX";
	rename -uid "514EA785-4A8F-5ED9-AA6A-C0AEA825DDEB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_01_R_anim_rotateY";
	rename -uid "300EEF41-4DF9-3DD0-30FC-D6BC2D8A8699";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_01_R_anim_rotateZ";
	rename -uid "B7AE27BB-4081-0FB7-593E-2CA27E1B6870";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateX";
	rename -uid "20CB7BFB-4CE6-C3CB-C765-90AFE2761888";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateY";
	rename -uid "8BAB0EBD-4C09-063B-CB73-E5B55C530D71";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_02_R_anim_rotateZ";
	rename -uid "80043B59-4E92-D3BC-EA3F-D98A27B4B1E5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateX";
	rename -uid "9313FE65-4F26-6E51-0788-42BE8DCA8D44";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateY";
	rename -uid "D936224C-433A-140B-899F-5BBD91FB75A4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "pinky_03_R_anim_rotateZ";
	rename -uid "DFAF68EB-4EC9-1E68-8912-49919988FC4C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_01_R_anim_rotateX";
	rename -uid "9884460B-480D-FEE4-54DF-CFA73920FCF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 -9.7905692812265013 95 -9.7905692812265013
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "ring_01_R_anim_rotateY";
	rename -uid "A00A42B3-4562-25E9-389C-478B282BADFF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_01_R_anim_rotateZ";
	rename -uid "5FA115E8-4B39-F7B4-6214-8BA01C8BD839";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.39360176455115 95 76.39360176455115
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "ring_02_R_anim_rotateX";
	rename -uid "D699C6D1-4A67-4787-A789-5D9243B573AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_02_R_anim_rotateY";
	rename -uid "23B046FD-4F86-10DC-DAD9-C1AE7BB04A31";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_02_R_anim_rotateZ";
	rename -uid "66F1D40A-4425-B2AB-F1AB-B98A04662117";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.39360176455115 95 76.39360176455115
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "ring_03_R_anim_rotateX";
	rename -uid "C1553C99-4607-6C06-DBA5-2CB12B325E1B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_03_R_anim_rotateY";
	rename -uid "CD18A7EB-426C-FCD4-333B-A689A8B6D6FB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "ring_03_R_anim_rotateZ";
	rename -uid "1BB1295B-4A48-7674-AAB1-25BD0C83458E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 0 30 76.39360176455115 95 76.39360176455115
		 120 0;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "thumb_01_R_anim_rotateX";
	rename -uid "D1AB3B99-47C1-E24A-70DE-1191C6F0EA79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_01_R_anim_rotateY";
	rename -uid "06DC9F46-41AB-D32A-BC17-C191A5F21417";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_01_R_anim_rotateZ";
	rename -uid "BDA987F7-44F8-EEAE-192D-A889947474E9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 18.200496090932514 30 -2.5872828717973362
		 95 -2.5872828717973362 120 18.200496090932514;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "thumb_02_R_anim_rotateX";
	rename -uid "89763F11-46C5-C355-5E53-0CA98D9362D4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateY";
	rename -uid "4E8A7655-48FE-24C4-670E-AC86119AB56E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_02_R_anim_rotateZ";
	rename -uid "B54DCEE7-4372-874F-4272-BD9A3AFC6ECB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 11.591383847611995 30 -4.8280359385002898
		 95 -4.8280359385002898 120 11.591383847611995;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTA -n "thumb_03_R_anim_rotateX";
	rename -uid "21EC20A6-4F50-7D03-09F0-F2AE6EBDD9AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateY";
	rename -uid "C79A8B8C-4C5B-F444-9370-0E88BE4FF4A7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTA -n "thumb_03_R_anim_rotateZ";
	rename -uid "308C1455-4633-5226-3496-D38CC0FA0734";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 4 ".ktv[0:3]"  0 11.591383847611995 30 -4.8280359385002898
		 95 -4.8280359385002898 120 11.591383847611995;
	setAttr -s 4 ".kit[3]"  1;
	setAttr -s 4 ".kot[3]"  1;
	setAttr -s 4 ".kix[3]"  1;
	setAttr -s 4 ".kiy[3]"  0;
	setAttr -s 4 ".kox[3]"  1;
	setAttr -s 4 ".koy[3]"  0;
createNode animCurveTL -n "index_02_R_anim_translateX";
	rename -uid "43EF8DAD-4534-C793-AD28-BBB83311935F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_02_R_anim_translateY";
	rename -uid "EE674F73-479D-9665-586D-6387A663BD3D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_02_R_anim_translateZ";
	rename -uid "B97B29AF-4614-CD58-E9AD-87904187C202";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "index_02_R_anim_scaleX";
	rename -uid "6A64BD3D-45EA-4BE5-C2D0-50ABED5F97DF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_02_R_anim_scaleY";
	rename -uid "94ABEE04-46EF-918E-C896-8C9DCA382321";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_02_R_anim_scaleZ";
	rename -uid "99B61976-4092-47B3-499D-65AAB9680EDB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "index_01_R_anim_translateX";
	rename -uid "9AA55D93-41C7-E475-C35B-55A36D1D0ADC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_01_R_anim_translateY";
	rename -uid "B9DE11DD-42D0-64A4-7429-E394318412A2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_01_R_anim_translateZ";
	rename -uid "A8578801-479B-2FC0-4FF6-3D888696B976";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "index_01_R_anim_scaleX";
	rename -uid "574B8D22-4020-1886-0229-63ACC4C0CA22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_01_R_anim_scaleY";
	rename -uid "31FB8945-41A8-085C-9775-19ACDB17CDCC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_01_R_anim_scaleZ";
	rename -uid "35225A1C-4F62-9CE1-A224-DE8C6085106D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_01_R_anim_sticky";
	rename -uid "607BA8C2-4F6F-B186-F026-368210673E6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_03_R_anim_translateX";
	rename -uid "C3FEB008-4C99-8A27-CA60-B3B9A5E525D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_03_R_anim_translateY";
	rename -uid "968544C5-49E4-FA3B-F1E2-D0B84553B992";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "index_03_R_anim_translateZ";
	rename -uid "F832AD2F-4C0F-36C3-EC03-D2A41A926C6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "index_03_R_anim_scaleX";
	rename -uid "9E405C27-40E5-0DF4-2483-56856D50F3C5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_03_R_anim_scaleY";
	rename -uid "345B14DE-48E7-21BB-4C67-5A8CD4338457";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "index_03_R_anim_scaleZ";
	rename -uid "99CB21F1-4CEF-5A4E-61B9-51B321AA576E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "middle_01_R_anim_translateX";
	rename -uid "E80CE4F5-483B-319C-18F9-2294B5F2B0A8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_01_R_anim_translateY";
	rename -uid "4F46FF73-4414-1DBE-C6D0-B5899C60EA35";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_01_R_anim_translateZ";
	rename -uid "DC1A52E3-4799-ADA6-1053-01B3621EC4A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "middle_01_R_anim_scaleX";
	rename -uid "F1E0601D-41DF-B5CA-FE51-B3B815ACC2EA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_01_R_anim_scaleY";
	rename -uid "AF733429-4529-22C7-3E6C-74BF1B101FB4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_01_R_anim_scaleZ";
	rename -uid "6A862F3C-4845-A0E9-14C9-50AE12A9175D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_01_R_anim_sticky";
	rename -uid "8BDA14F2-459D-5163-6CA7-08A9A3EF1AD2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_02_R_anim_translateX";
	rename -uid "96CBDBF4-4811-2E58-5DAF-48B12A27045F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_02_R_anim_translateY";
	rename -uid "73538A3F-4E66-A39C-05FA-A298638A3B97";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_02_R_anim_translateZ";
	rename -uid "D900D0BA-4EA5-B81C-47FA-FA87D9405BEE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "middle_02_R_anim_scaleX";
	rename -uid "0B7DE558-49D0-7756-4FF2-1083A47F1FEC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_02_R_anim_scaleY";
	rename -uid "6B0BB015-4D8C-8B5C-8864-D7A5E2D21403";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_02_R_anim_scaleZ";
	rename -uid "ACF1F9A9-4C16-023E-7FB7-19B54BA52086";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "middle_03_R_anim_translateX";
	rename -uid "316FB237-4220-555A-C3F8-43AC95CA9941";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_03_R_anim_translateY";
	rename -uid "C4D72F72-49C3-7CB3-32F8-8392401189AA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "middle_03_R_anim_translateZ";
	rename -uid "09963EDE-4BD0-65F5-B9D1-65ADB123FE7C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "middle_03_R_anim_scaleX";
	rename -uid "A87307AB-4975-C191-0089-8DAD98E90CD7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_03_R_anim_scaleY";
	rename -uid "86B323A1-492E-37F3-492B-D5B4E565DD99";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "middle_03_R_anim_scaleZ";
	rename -uid "9A838FF0-447A-5E2D-B794-109C36F7030E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "ring_01_R_anim_translateX";
	rename -uid "FD16D910-45C0-EC48-A5CB-0BA67BE4396C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_01_R_anim_translateY";
	rename -uid "7955B211-4128-6CDC-110B-B09DD51C735C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_01_R_anim_translateZ";
	rename -uid "E03AAEBE-4875-034B-8633-219F96A4A188";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "ring_01_R_anim_scaleX";
	rename -uid "4035D8B3-423F-E0A4-F8D2-BDB306E6115C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_01_R_anim_scaleY";
	rename -uid "4EF13B22-458F-4137-ED89-27A7FBB8CEA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_01_R_anim_scaleZ";
	rename -uid "B598855B-4746-A292-3CDA-0B9D62B8DEE8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_01_R_anim_sticky";
	rename -uid "0660D401-4B51-D0FF-2D34-88AD2F0FE2B9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_02_R_anim_translateX";
	rename -uid "F261F88F-400F-94BC-6C92-329AC7506E1B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_02_R_anim_translateY";
	rename -uid "1B5AF473-4F07-A96F-C323-108F3EED761C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_02_R_anim_translateZ";
	rename -uid "8E975CB2-4B8C-A427-527D-8BAA6B5471B7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "ring_02_R_anim_scaleX";
	rename -uid "90C99B52-4381-8DE3-810A-DDAA9258682F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_02_R_anim_scaleY";
	rename -uid "4E1A8DC2-408F-52B8-7817-4AA078715377";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_02_R_anim_scaleZ";
	rename -uid "E263E4B0-40BE-29C5-D71C-8EA4CF3C9C70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "ring_03_R_anim_translateX";
	rename -uid "F9AFEBF4-42BB-8C8E-0B59-2186983E19E1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_03_R_anim_translateY";
	rename -uid "C3D56D63-46E7-EE95-CE0A-FBA0464D1501";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "ring_03_R_anim_translateZ";
	rename -uid "0F049C6C-43F6-68A4-69D3-89807CEA364C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "ring_03_R_anim_scaleX";
	rename -uid "D947B4B8-421E-89AA-741D-82B40C6E6BE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_03_R_anim_scaleY";
	rename -uid "83953DE7-459B-346F-6210-438D9FA66551";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "ring_03_R_anim_scaleZ";
	rename -uid "A7EB150C-41E2-D440-E917-94AF743B1F42";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "pinky_01_R_anim_translateX";
	rename -uid "5DB2C0E2-40BB-374B-2DDB-E18E9B00243A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_01_R_anim_translateY";
	rename -uid "0D54754E-4081-0235-193D-9EAE61D796A4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_01_R_anim_translateZ";
	rename -uid "B386F989-4187-519B-C739-BBADCB113792";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "pinky_01_R_anim_scaleX";
	rename -uid "5A7C98FF-4028-0AC5-4B5D-DCB75C65FA1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleY";
	rename -uid "63CE1942-47F4-CB55-1187-3B9578688A55";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_01_R_anim_scaleZ";
	rename -uid "F595133E-4CFF-8F82-9E8E-F8A99046D069";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_01_R_anim_sticky";
	rename -uid "C44DF1C9-4053-B445-9C76-FCBC46538FD8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_02_R_anim_translateX";
	rename -uid "8EC3B7CB-478C-6909-F275-05B772B150E0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_02_R_anim_translateY";
	rename -uid "A818AF6F-4C5B-278D-FB62-3DA9A95C3DB9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_02_R_anim_translateZ";
	rename -uid "CFF143E5-4EB0-0E1F-7092-F2A37A83AB6E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "pinky_02_R_anim_scaleX";
	rename -uid "892EC69A-4BAF-DF10-100E-BD85C32A1C44";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleY";
	rename -uid "74E7388E-40FF-5F8A-9CAA-B7AEEA2D5C33";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_02_R_anim_scaleZ";
	rename -uid "3449C95B-4A96-697D-DB20-38951F0A05B8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "pinky_03_R_anim_translateX";
	rename -uid "840402DE-4A43-EFFF-83FD-F2B1565FEB78";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_03_R_anim_translateY";
	rename -uid "42734383-4690-8CB2-3444-03BB7030E539";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "pinky_03_R_anim_translateZ";
	rename -uid "04F71212-46C3-C914-6B77-A593AE9453B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "pinky_03_R_anim_scaleX";
	rename -uid "8B62FBB5-4CDB-4BEB-1482-D4BB98B008EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleY";
	rename -uid "4FFE954F-459D-858A-F47C-77B7F612EFF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "pinky_03_R_anim_scaleZ";
	rename -uid "23267C22-4F0E-A56C-8061-419842E80008";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "thumb_01_R_anim_translateX";
	rename -uid "8F286B13-44CD-8E7B-47FC-3582AF1BEAF7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_01_R_anim_translateY";
	rename -uid "861D5191-43FA-44C3-F951-D89E7D5BF7D3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_01_R_anim_translateZ";
	rename -uid "24FE5DF6-44A7-8F12-1E24-A583301E8E5F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "thumb_01_R_anim_scaleX";
	rename -uid "948B51D0-4F3A-4A69-E4D3-91B79119556D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleY";
	rename -uid "55A0DAD5-49EE-BACE-E47C-AC9147099813";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_01_R_anim_scaleZ";
	rename -uid "7661BC1C-4491-4197-1895-E68BD98A0E2C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "thumb_02_R_anim_translateX";
	rename -uid "F972FFC2-46DB-CCD9-5001-F0B167591125";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_02_R_anim_translateY";
	rename -uid "BF7869EE-4DF8-A853-90F8-80A7828DEF2D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_02_R_anim_translateZ";
	rename -uid "5948A8E2-491E-9A79-F8AA-D59266088A70";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "thumb_02_R_anim_scaleX";
	rename -uid "A2D350CA-4D85-F26B-2AA3-E8B433C739D9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleY";
	rename -uid "41214A9A-46A7-A775-3CC9-C9997A14E4F8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_02_R_anim_scaleZ";
	rename -uid "72C190E6-4FA2-AC01-29B1-899C57293CFB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTL -n "thumb_03_R_anim_translateX";
	rename -uid "F5B87AFC-4393-4A6A-1065-7D9235D89C87";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_03_R_anim_translateY";
	rename -uid "89346858-45AA-6754-C1C3-FEB4AAEFA398";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTL -n "thumb_03_R_anim_translateZ";
	rename -uid "D2E800E7-47BB-C847-1BEC-0788B80FB2FF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 0 95 0 120 0;
createNode animCurveTU -n "thumb_03_R_anim_scaleX";
	rename -uid "16EFF3F4-43C9-3FA6-63CD-4ABF1534B481";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleY";
	rename -uid "72480126-44A4-A8E5-E643-60898C9B1FE5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
createNode animCurveTU -n "thumb_03_R_anim_scaleZ";
	rename -uid "C727A8B6-4A28-41D4-55CD-628561C40F50";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  0 1 95 1 120 1;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 90 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 13 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 203 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 3 ".r";
select -ne :defaultTextureList1;
	setAttr -s 60 ".tx";
select -ne :initialShadingGroup;
	setAttr -s 150 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 123 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHideFaceDataSet;
select -ne :ikSystem;
	setAttr -s 6 ".sol";
connectAttr "Rig_UE4ManControllerRN.phl[173]" "groupParts5.ig";
connectAttr "groupId5.id" "Rig_UE4ManControllerRN.phl[1]";
connectAttr "groupParts5.og" "Rig_UE4ManControllerRN.phl[2]";
connectAttr "arm_L_settings_mode.o" "Rig_UE4ManControllerRN.phl[3]";
connectAttr "ik_hand_L_anim_translateX.o" "Rig_UE4ManControllerRN.phl[4]";
connectAttr "ik_hand_L_anim_translateY.o" "Rig_UE4ManControllerRN.phl[5]";
connectAttr "ik_hand_L_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[6]";
connectAttr "ik_hand_L_anim_stretch.o" "Rig_UE4ManControllerRN.phl[7]";
connectAttr "ik_hand_L_anim_slide.o" "Rig_UE4ManControllerRN.phl[8]";
connectAttr "ik_hand_L_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[9]";
connectAttr "ik_hand_L_anim_side.o" "Rig_UE4ManControllerRN.phl[10]";
connectAttr "ik_hand_L_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[11]";
connectAttr "ik_hand_L_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[12]";
connectAttr "ik_hand_L_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[13]";
connectAttr "ik_hand_L_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[14]";
connectAttr "ik_hand_L_anim_follow.o" "Rig_UE4ManControllerRN.phl[15]";
connectAttr "ik_hand_L_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[16]";
connectAttr "ik_hand_L_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[17]";
connectAttr "ik_hand_L_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[18]";
connectAttr "arm_R_settings_mode.o" "Rig_UE4ManControllerRN.phl[19]";
connectAttr "ik_hand_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[20]";
connectAttr "ik_hand_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[21]";
connectAttr "ik_hand_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[22]";
connectAttr "ik_hand_R_anim_stretch.o" "Rig_UE4ManControllerRN.phl[23]";
connectAttr "ik_hand_R_anim_slide.o" "Rig_UE4ManControllerRN.phl[24]";
connectAttr "ik_hand_R_anim_elbowLock.o" "Rig_UE4ManControllerRN.phl[25]";
connectAttr "ik_hand_R_anim_side.o" "Rig_UE4ManControllerRN.phl[26]";
connectAttr "ik_hand_R_anim_tip_swivel.o" "Rig_UE4ManControllerRN.phl[27]";
connectAttr "ik_hand_R_anim_tip_pivot.o" "Rig_UE4ManControllerRN.phl[28]";
connectAttr "ik_hand_R_anim_mid_swivel.o" "Rig_UE4ManControllerRN.phl[29]";
connectAttr "ik_hand_R_anim_mid_bend.o" "Rig_UE4ManControllerRN.phl[30]";
connectAttr "ik_hand_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[31]";
connectAttr "ik_hand_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[32]";
connectAttr "ik_hand_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[33]";
connectAttr "index_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[34]";
connectAttr "index_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[35]";
connectAttr "index_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[36]";
connectAttr "index_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[37]";
connectAttr "index_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[38]";
connectAttr "index_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[39]";
connectAttr "index_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[40]";
connectAttr "index_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[41]";
connectAttr "index_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[42]";
connectAttr "index_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[43]";
connectAttr "index_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[44]";
connectAttr "index_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[45]";
connectAttr "index_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[46]";
connectAttr "index_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[47]";
connectAttr "index_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[48]";
connectAttr "index_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[49]";
connectAttr "index_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[50]";
connectAttr "index_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[51]";
connectAttr "index_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[52]";
connectAttr "index_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[53]";
connectAttr "index_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[54]";
connectAttr "index_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[55]";
connectAttr "index_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[56]";
connectAttr "index_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[57]";
connectAttr "index_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[58]";
connectAttr "index_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[59]";
connectAttr "index_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[60]";
connectAttr "index_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[61]";
connectAttr "middle_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[62]";
connectAttr "middle_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[63]";
connectAttr "middle_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[64]";
connectAttr "middle_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[65]";
connectAttr "middle_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[66]";
connectAttr "middle_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[67]";
connectAttr "middle_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[68]";
connectAttr "middle_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[69]";
connectAttr "middle_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[70]";
connectAttr "middle_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[71]";
connectAttr "middle_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[72]";
connectAttr "middle_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[73]";
connectAttr "middle_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[74]";
connectAttr "middle_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[75]";
connectAttr "middle_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[76]";
connectAttr "middle_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[77]";
connectAttr "middle_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[78]";
connectAttr "middle_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[79]";
connectAttr "middle_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[80]";
connectAttr "middle_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[81]";
connectAttr "middle_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[82]";
connectAttr "middle_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[83]";
connectAttr "middle_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[84]";
connectAttr "middle_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[85]";
connectAttr "middle_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[86]";
connectAttr "middle_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[87]";
connectAttr "middle_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[88]";
connectAttr "middle_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[89]";
connectAttr "ring_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[90]";
connectAttr "ring_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[91]";
connectAttr "ring_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[92]";
connectAttr "ring_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[93]";
connectAttr "ring_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[94]";
connectAttr "ring_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[95]";
connectAttr "ring_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[96]";
connectAttr "ring_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[97]";
connectAttr "ring_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[98]";
connectAttr "ring_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[99]";
connectAttr "ring_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[100]";
connectAttr "ring_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[101]";
connectAttr "ring_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[102]";
connectAttr "ring_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[103]";
connectAttr "ring_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[104]";
connectAttr "ring_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[105]";
connectAttr "ring_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[106]";
connectAttr "ring_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[107]";
connectAttr "ring_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[108]";
connectAttr "ring_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[109]";
connectAttr "ring_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[110]";
connectAttr "ring_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[111]";
connectAttr "ring_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[112]";
connectAttr "ring_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[113]";
connectAttr "ring_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[114]";
connectAttr "ring_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[115]";
connectAttr "ring_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[116]";
connectAttr "ring_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[117]";
connectAttr "pinky_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[118]";
connectAttr "pinky_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[119]";
connectAttr "pinky_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[120]";
connectAttr "pinky_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[121]";
connectAttr "pinky_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[122]";
connectAttr "pinky_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[123]";
connectAttr "pinky_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[124]";
connectAttr "pinky_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[125]";
connectAttr "pinky_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[126]";
connectAttr "pinky_01_R_anim_sticky.o" "Rig_UE4ManControllerRN.phl[127]";
connectAttr "pinky_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[128]";
connectAttr "pinky_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[129]";
connectAttr "pinky_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[130]";
connectAttr "pinky_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[131]";
connectAttr "pinky_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[132]";
connectAttr "pinky_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[133]";
connectAttr "pinky_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[134]";
connectAttr "pinky_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[135]";
connectAttr "pinky_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[136]";
connectAttr "pinky_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[137]";
connectAttr "pinky_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[138]";
connectAttr "pinky_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[139]";
connectAttr "pinky_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[140]";
connectAttr "pinky_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[141]";
connectAttr "pinky_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[142]";
connectAttr "pinky_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[143]";
connectAttr "pinky_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[144]";
connectAttr "pinky_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[145]";
connectAttr "thumb_01_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[146]";
connectAttr "thumb_01_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[147]";
connectAttr "thumb_01_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[148]";
connectAttr "thumb_01_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[149]";
connectAttr "thumb_01_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[150]";
connectAttr "thumb_01_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[151]";
connectAttr "thumb_01_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[152]";
connectAttr "thumb_01_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[153]";
connectAttr "thumb_01_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[154]";
connectAttr "thumb_02_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[155]";
connectAttr "thumb_02_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[156]";
connectAttr "thumb_02_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[157]";
connectAttr "thumb_02_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[158]";
connectAttr "thumb_02_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[159]";
connectAttr "thumb_02_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[160]";
connectAttr "thumb_02_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[161]";
connectAttr "thumb_02_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[162]";
connectAttr "thumb_02_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[163]";
connectAttr "thumb_03_R_anim_translateX.o" "Rig_UE4ManControllerRN.phl[164]";
connectAttr "thumb_03_R_anim_translateY.o" "Rig_UE4ManControllerRN.phl[165]";
connectAttr "thumb_03_R_anim_translateZ.o" "Rig_UE4ManControllerRN.phl[166]";
connectAttr "thumb_03_R_anim_rotateX.o" "Rig_UE4ManControllerRN.phl[167]";
connectAttr "thumb_03_R_anim_rotateY.o" "Rig_UE4ManControllerRN.phl[168]";
connectAttr "thumb_03_R_anim_rotateZ.o" "Rig_UE4ManControllerRN.phl[169]";
connectAttr "thumb_03_R_anim_scaleX.o" "Rig_UE4ManControllerRN.phl[170]";
connectAttr "thumb_03_R_anim_scaleY.o" "Rig_UE4ManControllerRN.phl[171]";
connectAttr "thumb_03_R_anim_scaleZ.o" "Rig_UE4ManControllerRN.phl[172]";
connectAttr "hyperLayout1.msg" "animBot.hl";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "animBot.animBot_Anim_Recovery_Scene_ID"
		;
connectAttr "animBot_Select_Sets.msg" "animBot.animBot_Select_Sets";
connectAttr "__Red__.msg" "animBot.__Red__";
connectAttr "hyperLayout2.msg" "animBot_Select_Sets.hl";
connectAttr "hyperLayout3.msg" "__Red__.hl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "animBot_Anim_Recovery_Scene_ID.msg" "hyperLayout1.hyp[0].dn";
connectAttr "animBot_Select_Sets.msg" "hyperLayout1.hyp[1].dn";
connectAttr "sharedReferenceNode.sr" "Rig_UE4ManControllerRN.sr";
connectAttr "groupId5.id" "groupParts5.gi";
connectAttr "__Red__.msg" "hyperLayout2.hyp[0].dn";
connectAttr "All_BodyIK.msg" "hyperLayout3.hyp[0].dn";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "groupId5.msg" ":defaultLastHiddenSet.gn" -na;
// End of 1P_Mannequin_QuickAction_01.ma
